package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.ReviewAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.objects.ReviewDO;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;

public class ItemProfileActivity extends BaseNew implements DataListener {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_itemName,txt_desc,txt_cartCount,txt_add,txt_itemSize;
    public ImageView img_normal_actionbar,img_back,img_favourite;
    FrameLayout fl_cart,fl_Filter;
    PreferenceUtils preferenceUtils;
    String ItemId,ItemName,queryParams;
    private ArrayList<ItemsDO> arrItemsProfile;
    private ArrayList<ReviewDO> reviewList;
    LinearLayout ll_itemImage,ll_brandImage;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader= ImageLoader.getInstance();
    int CartCount;
    TextView txt_itemPrice,txt_ratingbtn;
    RatingBar rat_avg;
    ListView lst_reviews;
    ReviewAdapter reviewAdapter;
    ArrayList<ItemsCartsDO> SingleItemCartList;
    CartDatabase cartDatabase;
    FontType fonttype;
    TextView txt_qty;
    ImageView img_minus,img_plus,img_offer;
    int QTY;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_profile);
        mContext=ItemProfileActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();
        CartDatabase.init(this);
        imgLoaderConf = new ImageLoaderConfiguration.Builder(mContext).build();
        imgLoader.init(imgLoaderConf);
        dispImage=new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.noimage)
                .showImageOnLoading(R.mipmap.noimage)
                .showImageForEmptyUri(R.mipmap.noimage)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_itemprofile);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ItemId = getIntent().getExtras().getString("ItemId");
        ItemName= getIntent().getExtras().getString("ItemName");

        img_back.setImageResource(R.mipmap.back_arrow);

        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);

        txt_normal_actionbar.setText(ItemName);

        queryParams=ItemId;

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_ratingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,ReviewActivity.class);
                intent.putExtra("ItemId", ItemId);
                intent.putExtra("ItemName", ItemName);
                startActivity(intent);
            }
        });

        txt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                addItemToCartList(arrItemsProfile);
            }
        });

        fl_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(mContext, CartActivity.class);
                startActivity(intent);
            }
        });

        img_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);
                int newQty=SingleItemCartList.get(0).ItemQty;
                if (newQty >= 5)
                {
                    newQty = 5;
                }
                else
                {
                    newQty = newQty + 1;
                }
                CartDatabase.updateQuantityFromProfileScreen(SingleItemCartList.get(0).ItemId, newQty);
                SetAddButtonVisibility();
            }
        });

        img_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);
                QTY=SingleItemCartList.get(0).ItemQty;
                if (QTY <= 1)
                {
                    QTY = 1;
                }
                else
                {
                    QTY = QTY - 1;
                    CartDatabase.updateQuantityFromProfileScreen(SingleItemCartList.get(0).ItemId, QTY);
                    SetAddButtonVisibility();
                }
            }
        });
    }

    private void addItemToCartList(ArrayList<ItemsDO> arrItemsProfile)
    {
        Toast.makeText(mContext, "Item added to cart", Toast.LENGTH_SHORT).show();
        cartDatabase.addCartData(new ItemsCartsDO(arrItemsProfile.get(0).ItemId, arrItemsProfile.get(0).ItemName, 1, arrItemsProfile.get(0).ItemPrice, arrItemsProfile.get(0).ItemImageUrl_100, arrItemsProfile.get(0).ItemDesc, "true",arrItemsProfile.get(0).Size));
        SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);

        setCartCount();
        SetAddButtonVisibility();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Product Profile Screen");
        getItemsProfile(queryParams);

        setCartCount();
        SetAddButtonVisibility();
    }

    public void SetAddButtonVisibility()
    {
        SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);

        if(SingleItemCartList.size()>0)
        {
            txt_add.setVisibility(View.INVISIBLE);
            txt_qty.setText("" + SingleItemCartList.get(0).ItemQty);

        LogUtils.error(ItemProfileActivity.class.getSimpleName(),"SingleItemCartList="+SingleItemCartList.get(0).ItemName);
        LogUtils.error(ItemProfileActivity.class.getSimpleName(),"SingleItemCartList="+SingleItemCartList.get(0).ItemId);
        LogUtils.error(ItemProfileActivity.class.getSimpleName(),"SingleItemCartList="+SingleItemCartList.get(0).ItemQty);

        }
        else
        {
            txt_add.setVisibility(View.VISIBLE);
        }
    }

    public void setCartCount() {
        CartCount= CartDatabase.getCartlistCount();
        if(CartCount>0)
        {
            txt_cartCount.setVisibility(View.VISIBLE);
            txt_cartCount.setText(""+CartCount );
        }
        else
        {
            txt_cartCount.setVisibility(View.GONE);
        }
    }

    private void getItemsProfile(String queryParams)
    {
        showLoaderNew();
        if(new CommonBL(mContext, ItemProfileActivity.this).getItemsProfile(queryParams)) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        txt_cartCount=(TextView)findViewById(R.id.txt_cartCount);
        txt_itemSize=(TextView)findViewById(R.id.txt_itemSize);


        ll_itemImage=(LinearLayout)findViewById(R.id.ll_itemImage);
        ll_brandImage=(LinearLayout)findViewById(R.id.ll_brandImage);
        txt_itemName=(TextView)findViewById(R.id.txt_itemName);
        txt_desc=(TextView)findViewById(R.id.txt_desc);
        rat_avg=(RatingBar)findViewById(R.id.rat_avg);
        txt_itemPrice=(TextView)findViewById(R.id.txt_itemPrice);
        txt_ratingbtn=(TextView)findViewById(R.id.txt_ratingbtn);

        lst_reviews=(ListView)findViewById(R.id.lst_reviews);
        txt_add=(TextView)findViewById(R.id.txt_add);

        txt_qty= (TextView)findViewById(R.id.txt_qty);
        img_minus= (ImageView)findViewById(R.id.img_minus);
        img_plus= (ImageView)findViewById(R.id.img_plus);
        img_offer= (ImageView)findViewById(R.id.img_offer);

    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_ITEM_PROFILE)
        {
            if(!data.isError)
            {
                arrItemsProfile = (ArrayList<ItemsDO>) data.data;
                if(arrItemsProfile!=null&&arrItemsProfile.size()>0)
                {
                    txt_desc.setText(arrItemsProfile.get(0).ItemDesc);
                    txt_itemName.setText(arrItemsProfile.get(0).ItemName);
                    txt_itemPrice.setText("₹ " + arrItemsProfile.get(0).ItemPrice);
                    txt_itemSize.setText(arrItemsProfile.get(0).Size);
                    rat_avg.setRating(Float.valueOf(arrItemsProfile.get(0).Avgrating));

                    if(arrItemsProfile.get(0).OfferStatus)
                    {
                        img_offer.setImageResource(R.mipmap.offers_icon);
                    }
                    else
                    {
                        img_offer.setVisibility(View.INVISIBLE);
                    }

                    imgLoader.getInstance().displayImage(arrItemsProfile.get(0).ItemImageUrl_100, new BgViewAware(ll_itemImage), dispImage);

                    imgLoader.getInstance().displayImage(arrItemsProfile.get(0).ItemBrandImgUrl_72, new BgViewAware(ll_brandImage), dispImage);

                    reviewList=arrItemsProfile.get(0).reviewlist;

                    reviewAdapter=new ReviewAdapter(mContext,reviewList);
                    lst_reviews.setAdapter(reviewAdapter);
                }
            }
        }
    }
}
