package com.nm.cremeville.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.GoogleSearchPlaceAdapter;
import com.nm.cremeville.objects.Allpincode;
import com.nm.cremeville.objects.CurrentAddressDO;
import com.nm.cremeville.objects.SearchPlaceDomain;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


public class ManualLocation extends AppCompatActivity  {

    Context mContext;

    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    public ImageView img_normal_actionbar, img_back;
    PreferenceUtils preferenceUtils;
    ImageView img_favourite,img_resetFilter;
    FrameLayout fl_cart, fl_Filter;


    ListView lv_alllocation;
    EditText edt_placename;
    ImageView img_clearsrch;
    LinearLayout ll_noaddfound,ll_lvlayout;
    String usersearchplace="null";
    GoogleSearchPlaceAdapter googleSearchPlaceAdapter;
    private ArrayList<SearchPlaceDomain> searchPlacelist;
    private ArrayList<String> srchlocatypelist;
    SearchPlaceDomain searchPlaceDomain;
    ArrayList<Allpincode> pincodedatalist;
    Allpincode allpincode;
    AnimationSet anim_set=null;
    String googleplaceurl = "https://maps.googleapis.com/maps/api/geocode/json?components=country:in&";
    FontType fonttype;
    String Status;

    ArrayList<CurrentAddressDO>  arrCurrAddressDO;
    CurrentAddressDO	currentAddressDO;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);
        mContext=ManualLocation.this;
        preferenceUtils = new PreferenceUtils(mContext);
        srchlocatypelist=new ArrayList<>();
        initializeview();
        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
        ViewGroup root = (ViewGroup)findViewById(R.id.ll_manual);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txt_normal_actionbar.setText("Manual Location");

        fl_cart.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);

        final Animation fadein = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        final Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fade_out);

        anim_set = new AnimationSet(true);
        anim_set.addAnimation(fadein);
        anim_set.setStartOffset(500);
        anim_set.addAnimation(fadeout);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        img_clearsrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_placename.setText("");
                edt_placename.setHint("Enter delivery address");
            }
        });

        edt_placename.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence placename, int start, int before, int count) {

                usersearchplace = placename.toString();

                if (TextUtils.isEmpty(placename)) {

                    usersearchplace = "null";

                    shownoaddressfound();

                    LogUtils.error(ManualLocation.class.getSimpleName(), "if user typed letter is" + usersearchplace);


                } else {

                    hidenoaddressfound();

                    LogUtils.error(ManualLocation.class.getSimpleName(), "else user typed letter is" + usersearchplace);

                    goto_searchtask_details(usersearchplace);
                }
            }

            @Override
            public void afterTextChanged(Editable placename) {


            }
        });

        lv_alllocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SearchPlaceDomain searchPlaceDomain = (SearchPlaceDomain) parent.getItemAtPosition(position);

              /*  LogUtils.error(ManualLocation.class.getSimpleName(),"streetbldgname is "+ searchPlaceDomain.streetbldgname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"subareaname is "+ searchPlaceDomain.subareaname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"areaname is "+ searchPlaceDomain.areaname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"areadesc is "+ searchPlaceDomain.areadesc);
                LogUtils.error(ManualLocation.class.getSimpleName(),"routename is "+ searchPlaceDomain.routename);
                LogUtils.error(ManualLocation.class.getSimpleName(),"cityname is "+ searchPlaceDomain.cityname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"districtname is "+ searchPlaceDomain.districtname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"countryname is "+ searchPlaceDomain.countryname);
                LogUtils.error(ManualLocation.class.getSimpleName(),"formatted_address is "+ searchPlaceDomain.formatted_address);
                LogUtils.error(ManualLocation.class.getSimpleName(),"statename is "+ searchPlaceDomain.statename);
                LogUtils.error(ManualLocation.class.getSimpleName(),"Location Address is "+ searchPlaceDomain.location_address);
                LogUtils.error(ManualLocation.class.getSimpleName(),"Latitude is "+ searchPlaceDomain.location_latitude);
                LogUtils.error(ManualLocation.class.getSimpleName(),"Longitude  is "+ searchPlaceDomain.location_longitude);
                LogUtils.error(ManualLocation.class.getSimpleName(),"Pincode  is "+ searchPlaceDomain.pincode);*/



                    LogUtils.error(ManualLocation.class.getSimpleName(),"Item Clicked is ");
                    LogUtils.error(ManualLocation.class.getSimpleName(), "Latitude is " + searchPlaceDomain.location_latitude);
                    LogUtils.error(ManualLocation.class.getSimpleName(), "Longitude is " + searchPlaceDomain.location_longitude);

                    String SubArea=searchPlaceDomain.subareaname;
                    String RouteName=searchPlaceDomain.routename;
                    if(searchPlaceDomain.subareaname.equals("NA"))
                    {
                        if(searchPlaceDomain.areaname.equals("NA"))
                        {
                            if(searchPlaceDomain.routename.equals("NA"))
                            {
                                if(searchPlaceDomain.cityname.equals("NA"))
                                {
                                    if(searchPlaceDomain.districtname.equals("NA"))
                                    {

                                    }
                                    else
                                    {
                                        SubArea=searchPlaceDomain.districtname;
                                    }
                                }
                                else
                                {
                                    SubArea=searchPlaceDomain.cityname;
                                }
                            }
                            else
                            {
                                SubArea=searchPlaceDomain.routename;
                            }
                        }
                        else
                        {
                            SubArea=searchPlaceDomain.areaname;
                        }
                    }
                    else
                    {
                        SubArea=searchPlaceDomain.subareaname;
                    }


                    preferenceUtils.saveCurrentAddress(SubArea,searchPlaceDomain.cityname);
                    preferenceUtils.saveCurrentLocation(searchPlaceDomain.location_latitude, searchPlaceDomain.location_longitude);

                    Intent intent=new Intent(mContext,Dashboard.class);
                    startActivity(intent);



            }
        });



    }

    private void gotogooglemapscreen() {

        /*Intent googlemapintent=new Intent(mContext,DeliveryLocationActivity.class);
        startActivity(googlemapintent);
        finish();*/
    }

    private void goto_searchtask_details(String location) {


        String address="NA",sensor="sensor=false",placesearchurl="NA";

        String region="&region=MH";

        if(location.equals("null"))
        {
            shownoaddressfound();
        }
        else
        {
            try {
                // encoding special characters like space in the user input place
                location= URLEncoder.encode(location, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            address="address=" + location;

            // url , from where the geocoding data is fetched
            placesearchurl = googleplaceurl + address +region+ "&" + sensor;

           LogUtils.error(ManualLocation.class.getSimpleName(), "placesearchurl is " + placesearchurl);
            new GeocodePlaceTask().execute(placesearchurl);
        }
    }


    @SuppressLint("SetTextI18n")
    private void initializeview() {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        lv_alllocation= (ListView) findViewById(R.id.lv_alllocation);
        edt_placename= (EditText) findViewById(R.id.edt_placename);
        ll_noaddfound= (LinearLayout) findViewById(R.id.ll_noaddfound);
        ll_lvlayout= (LinearLayout) findViewById(R.id.ll_lvlayout);
        img_clearsrch= (ImageView) findViewById(R.id.img_clearsrch);

    }


    /** A class, to download Places from Geocoding webservice */
    class GeocodePlaceTask extends AsyncTask<String,Void,String>
    {
        String data = null;

        public GeocodePlaceTask() {


        }

        @Override
        protected void onPreExecute() {

            //showLoaderNew();
        }


        @Override
        protected String doInBackground(String... params) {

            try
            {
                data=downloadUrl(params[0]);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

            return data;
        }



        @Override
        protected void onPostExecute(String result) {

            //hideloader();

            searchPlacelist=new ArrayList<>();


            // Instantiating ParserTask which parses the json data from Geocoding webservice
            // in a non-ui thread

            // Start parsing the places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask

            // new ParserTask().execute(result);

            searchplacedataparser(result);
        }
    }

    /*private void searchplacedataparsernew(String result) {

        String resultstatus="NA";

        pincodedatalist=new ArrayList<>();

        try {
            JSONObject curraddrsjobj = new JSONObject(result);

            if (curraddrsjobj.has("status"))
            {
                Status=curraddrsjobj.getString("status");

                if(Status.equalsIgnoreCase("OK"))
                {
                    if(curraddrsjobj.has("results"))
                    {
                        currentAddressDO=new CurrentAddressDO();

                        JSONArray addrsresultjsarry=curraddrsjobj.getJSONArray("results");

                        for(int i=0;i<addrsresultjsarry.length();i++)
                        {
                            JSONObject addrsresultjobj=addrsresultjsarry.getJSONObject(i);

                            if(addrsresultjobj.has("address_components"))
                            {
                                JSONArray addrscmpjsnArry=addrsresultjobj.getJSONArray("address_components");

                                for(int j=0;j<addrscmpjsnArry.length();j++)
                                {
                                    JSONObject addrscompjobj=addrscmpjsnArry.getJSONObject(j);

                                    if(addrscompjobj.has("types"))
                                    {
                                        JSONArray typesarry=addrscompjobj.getJSONArray("types");

                                        for(int count=0;count<typesarry.length();count++)
                                        {
                                            srchlocatypelist=new ArrayList<>();

                                            String types= (String) typesarry.get(count);

                                            srchlocatypelist.add(types);

                                            if(srchlocatypelist.size()>0)
                                            {
                                                for(String typevalue:srchlocatypelist)
                                                {
                                                    if(typevalue.equals("street_number"))
                                                    {

                                                        currentAddressDO.streetbldgname= addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("route"))
                                                    {

                                                        currentAddressDO.routename= addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("sublocality_level_2"))
                                                    {
                                                        currentAddressDO.subareaname=addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("sublocality_level_1"))
                                                    {
                                                        currentAddressDO.areaname=addrscompjobj.getString("long_name");

                                                    }

                                                    if(typevalue.equals("administrative_area_level_1"))
                                                    {
                                                        currentAddressDO.statename=addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("administrative_area_level_2"))
                                                    {
                                                        currentAddressDO.districtname=addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("locality"))
                                                    {
                                                        currentAddressDO.cityname=addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("country"))
                                                    {
                                                        currentAddressDO.countryname=addrscompjobj.getString("long_name");
                                                    }

                                                    if(typevalue.equals("postal_code"))
                                                    {
                                                        currentAddressDO.pincode=addrscompjobj.getString("long_name");
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    if(addrsresultjobj.has("formatted_address"))
                                    {
                                        currentAddressDO.formatted_address=addrsresultjobj.getString("formatted_address");

                                    }

                                    arrCurrAddressDO.add(currentAddressDO);

                                }
                            }
                        }

                    }

                }
                else
                {
                   // errorMessage="error";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(arrCurrAddressDO.size()>0)
        {

            set_placelist_adapter(arrCurrAddressDO);
        }
        else
        {
            shownoaddressfound();
        }
    }*/

    private void searchplacedataparser(String result) {

        String resultstatus="NA";

        pincodedatalist=new ArrayList<>();

        try {
            JSONObject jsonObject=new JSONObject(result);

            if(jsonObject.has("status"))
            {
                resultstatus=jsonObject.getString("status");
                if(resultstatus.equals("OK"))
                {
                    JSONArray resultjsnarry=jsonObject.getJSONArray("results");
                    searchPlaceDomain=new SearchPlaceDomain();
                    allpincode=new Allpincode();
                    for(int i=0;i<resultjsnarry.length();i++)
                    {
                        JSONObject resultjobj=resultjsnarry.getJSONObject(i);
                        if(resultjobj.has("formatted_address"))
                        {
                            searchPlaceDomain.location_address=resultjobj.getString("formatted_address");
                        }
                        if(resultjobj.has("geometry"))
                        {
                            JSONObject geometryjobj=resultjobj.getJSONObject("geometry");
                            if(geometryjobj.has("location"))
                            {
                                JSONObject locationjobj=geometryjobj.getJSONObject("location");
                                if(locationjobj.has("lat"))
                                {
                                    searchPlaceDomain.location_latitude=String.valueOf(locationjobj.getDouble("lat"));
                                }
                                if(locationjobj.has("lng"))
                                {
                                    searchPlaceDomain.location_longitude=String.valueOf(locationjobj.getDouble("lng"));
                                }
                            }
                        }

                        if(resultjobj.has("address_components"))
                        {
                            JSONArray addrscompnetjsnarry=resultjobj.getJSONArray("address_components");
                            for(int j=0;j<addrscompnetjsnarry.length();j++)
                            {
                                JSONObject addrscompntjobj=addrscompnetjsnarry.getJSONObject(j);
                                if(addrscompntjobj.has("types"))
                                {
                                    JSONArray typesarry=addrscompntjobj.getJSONArray("types");
                                    for(int k=0;k<typesarry.length();k++)
                                    {
                                        srchlocatypelist=new ArrayList<>();
                                        String types=(String)typesarry.get(k);
                                        srchlocatypelist.add(types);
                                        if(srchlocatypelist.size()>0)
                                        {
                                            for(String typevalue:srchlocatypelist)
                                            {
                                                if(typevalue.equals("postal_code"))
                                                {
                                                    String postalcode=addrscompntjobj.getString("long_name");
                                                    String pincode=addrscompntjobj.getString("short_name");
                                                    searchPlaceDomain.pincode=postalcode;
                                                    searchPlaceDomain.pincode=pincode;
                                                    allpincode.pincode= Long.parseLong(addrscompntjobj.getString("long_name"));
                                                    allpincode.pincode_value=addrscompntjobj.getString("long_name");
                                                    pincodedatalist.add(allpincode);
                                                    // LoggerUtils.debug(SearchDelvLocationActivity.class.getSimpleName(),"srch location pincode is.."+postalcode);
                                                }
                                                if(typevalue.equals("street_number"))
                                                {

                                                    searchPlaceDomain.streetbldgname= addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("route"))
                                                {

                                                    searchPlaceDomain.routename= addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("sublocality_level_2"))
                                                {
                                                    searchPlaceDomain.subareaname=addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("sublocality_level_1"))
                                                {
                                                    searchPlaceDomain.areaname=addrscompntjobj.getString("long_name");

                                                }

                                                if(typevalue.equals("administrative_area_level_1"))
                                                {
                                                    searchPlaceDomain.statename=addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("administrative_area_level_2"))
                                                {
                                                    searchPlaceDomain.districtname=addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("locality"))
                                                {
                                                    searchPlaceDomain.cityname=addrscompntjobj.getString("long_name");
                                                }

                                                if(typevalue.equals("country"))
                                                {
                                                    searchPlaceDomain.countryname=addrscompntjobj.getString("long_name");
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    searchPlacelist.add(searchPlaceDomain);
                    ll_lvlayout.setVisibility(View.GONE);
                }
                else
                {
                    searchPlacelist=new ArrayList<>();
                }
            }
            else
            {
                searchPlacelist=new ArrayList<>();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(searchPlacelist.size()>0)
        {
            set_placelist_adapter(searchPlacelist);
        }
        else
        {
            shownoaddressfound();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Manual Address Screen");
    }

    private void shownoaddressfound(){

        ll_noaddfound.setVisibility(View.VISIBLE);

        ll_lvlayout.setVisibility(View.GONE);

    }

    private void hidenoaddressfound()
    {
        ll_noaddfound.setVisibility(View.GONE);

        ll_lvlayout.setVisibility(View.VISIBLE);


    }

    private void set_placelist_adapter(ArrayList<SearchPlaceDomain> arrCurrAddressDO) {

        hidenoaddressfound();

        ll_lvlayout.setVisibility(View.VISIBLE);


        for(Allpincode allpincode:pincodedatalist)
        {
            LogUtils.error(ManualLocation.class.getSimpleName(), "pincode for search area is" + allpincode.pincode);
        }

        googleSearchPlaceAdapter=new GoogleSearchPlaceAdapter(mContext,arrCurrAddressDO);

        lv_alllocation.setAdapter(googleSearchPlaceAdapter);

        googleSearchPlaceAdapter.notifyDataSetChanged();

    }

    private String downloadUrl(String strUrl) {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        int responsestatus=0;

        try {
            URL url = new URL(strUrl);

            // Creating an http connection to COMMUNICATE with url
            try {
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url
                urlConnection.connect();

                responsestatus=urlConnection.getResponseCode();

                // Reading data from url


                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb = new StringBuffer();

                String line = "";
                while( ( line = br.readLine()) != null){
                    sb.append(line);
                }

                data = sb.toString();

                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (iStream != null) {
                    iStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

}