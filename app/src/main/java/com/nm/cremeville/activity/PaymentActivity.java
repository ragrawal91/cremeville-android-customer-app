package com.nm.cremeville.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.PostOrderDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;
import com.payUMoney.sdk.SdkConstants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PaymentActivity extends BaseNew implements DataListener {


    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    public ImageView img_normal_actionbar,img_back;
    PreferenceUtils preferenceUtils;
    ImageView img_favourite;
    FrameLayout fl_cart,fl_Filter;
    double subtotal,GrandTotal,vatTax;
    String  deliveryValue;
    String OrderId;
    DecimalFormat df=new DecimalFormat("00.00######");
    Button btn_payumoney,btn_card,btn_netBanking,btn_cashOnDelivery;
    TextView txt_amount;

    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;

   String STOREID;

    ArrayList<PostOrderDO>  arrPostOrderDO;
    ArrayList<ItemsCartsDO> CartList;

    String CouponCode,CouponStatus,CouponAmount,LoyaltyStatus,LoyaltyAmount,LoyaltyPoints,OrderStatus="NEW";
    String OrderOriginId="ANDROID",transactionId;
    String DeliveryAddress,AddressId;
    boolean PaymentStatus;
    HashMap<String, String> params = new HashMap<>();
    String hashSequence,txnid,rndm;


    //TEST
   // String MERCHANT_KEY="dRQuiA", MERCHANT_SALT="teEkuVg2", MERCHANT_ID="4928174";
   // boolean DEBUG_KEY=false;

    //LIVE Cremeville
    String MERCHANT_KEY="UpQLaBov", MERCHANT_SALT="vRIt1raKut", MERCHANT_ID="5457301";
    boolean DEBUG_KEY=false;

    public static final String TAG = "PayUMoneySDK";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_screen);
        mContext=PaymentActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        CartDatabase.init(this);
        initializeControls();

        Map<String, Object> paymentScreen = new HashMap<String, Object>();
        paymentScreen.put(AFInAppEventParameterName.LEVEL,7);
        paymentScreen.put(AFInAppEventParameterName.SCORE,70);
        AppsFlyerLib.getInstance().trackEvent(mContext,"Checkout Screen",paymentScreen);

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
        CartDatabase.init(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txt_normal_actionbar.setText("Payment");

        fl_cart.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);

        Random rand = new Random();
        rndm = Integer.toString(rand.nextInt())+(System.currentTimeMillis() / 1000L);
        txnid=hashCal(rndm).substring(0, 20);

        AddressId = getIntent().getExtras().getString("AddressId");
        STOREID= getIntent().getExtras().getString("STOREID");
        DeliveryAddress = getIntent().getExtras().getString("DeliveryAddress");
        subtotal = getIntent().getExtras().getDouble("subtotal");
        vatTax = getIntent().getExtras().getDouble("vatTax");
        deliveryValue = getIntent().getExtras().getString("deliveryCharges");
        GrandTotal = getIntent().getExtras().getDouble("GrandTotal");
        CouponCode = getIntent().getExtras().getString("CouponCode");
        CouponStatus = getIntent().getExtras().getString("CouponStatus");
        CouponAmount = getIntent().getExtras().getString("CouponAmount");
        LoyaltyStatus = getIntent().getExtras().getString("LoyaltyStatus");
        LoyaltyAmount = getIntent().getExtras().getString("LoyaltyAmount");
        LoyaltyPoints = getIntent().getExtras().getString("LoyaltyPoints");

       LogUtils.error(CheckoutActivity.class.getSimpleName(), "StoreID=" + STOREID);

        if(CouponStatus.equals("1"))
        {
            CartList=CartDatabase.getAllCouponCartList();
        }
        else
        {
            CartList=CartDatabase.getAllCartList();
        }

        txt_amount.setText("₹ " + String.format("%.2f", GrandTotal));


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_payumoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                MyApplication.getInstance().trackEvent("PayUmoney Wallet", "PayUMoney Wallet button", "Proceed to payment method");

                Map<String, Object> payuMoneybutton = new HashMap<String, Object>();
                payuMoneybutton.put(AFInAppEventParameterName.LEVEL,7);
                payuMoneybutton.put(AFInAppEventParameterName.SCORE,70);
                AppsFlyerLib.getInstance().trackEvent(mContext,"Payu Money Clicked",payuMoneybutton);

                makePayment(CustomerID, OrderId, GrandTotal);
            }
        });

        btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                MyApplication.getInstance().trackEvent("Card Payment", "Card Payment button", "Proceed to payment method");

                Map<String, Object> cardButton = new HashMap<String, Object>();
                cardButton.put(AFInAppEventParameterName.LEVEL,7);
                cardButton.put(AFInAppEventParameterName.SCORE,70);
                AppsFlyerLib.getInstance().trackEvent(mContext,"Card Button Clicked",cardButton);

                makePayment(CustomerID, OrderId, GrandTotal);
            }
        });

        btn_netBanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                MyApplication.getInstance().trackEvent("NetBanking", "NetBanking button", "Proceed to payment method");

                Map<String, Object> netBankingButton = new HashMap<String, Object>();
                netBankingButton.put(AFInAppEventParameterName.LEVEL,7);
                netBankingButton.put(AFInAppEventParameterName.SCORE,70);
                AppsFlyerLib.getInstance().trackEvent(mContext,"NetBanking Button Clicked",netBankingButton);

                makePayment(CustomerID, OrderId, GrandTotal);
            }
        });

        btn_cashOnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().trackEvent("Cash On Delivery", "Cash button", "Placed Order");

                Map<String, Object> cashbutton = new HashMap<String, Object>();
                cashbutton.put(AFInAppEventParameterName.LEVEL,7);
                cashbutton.put(AFInAppEventParameterName.SCORE,70);
                AppsFlyerLib.getInstance().trackEvent(mContext,"Cash button clicked",cashbutton);

                postOrderDetails(CustomerID, STOREID,AddressId,DeliveryAddress,CartList,subtotal,
                        vatTax,deliveryValue,GrandTotal,CouponCode,CouponStatus,CouponAmount,LoyaltyStatus,LoyaltyAmount,LoyaltyPoints,"CASH"
                        ,OrderOriginId,"COD","unpaid",OrderStatus);
            }
        });

    }

    private void postOrderDetails(String customerID, String storeId, String addressId, String deliveryAddress,
                                  ArrayList<ItemsCartsDO> cartList, double subtotal, double vatTax, String deliveryValue,
                                  double grandTotal, String couponCode, String couponStatus, String couponAmount,
                                  String loyaltyStatus, String loyaltyAmount, String loyaltyPoints, String paymentMode,
                                  String orderOriginId, String transactionId, String paymentStatus,String OrderStatus)
    {

        String queryParam=AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, PaymentActivity.this).postOrderDetails(customerID, storeId,addressId,deliveryAddress,cartList,subtotal,
                vatTax,deliveryValue,grandTotal,couponCode,couponStatus,couponAmount,loyaltyStatus,loyaltyAmount,loyaltyPoints,paymentMode
                ,orderOriginId,transactionId,paymentStatus,queryParam,OrderStatus))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void makePayment(String customerID, String orderId, double grandTotal)
    {
        PayUmoneySdkInitilizer.PaymentParam.Builder builder = new PayUmoneySdkInitilizer.PaymentParam.Builder();

        builder.setKey(MERCHANT_KEY); //Put your live KEY here
        builder.setSalt(MERCHANT_SALT); //Put your live SALT here
        builder.setMerchantId(MERCHANT_ID); //Put your live MerchantId here


        builder.setIsDebug(DEBUG_KEY);
        builder.setDebugKey(MERCHANT_KEY);// Debug Key
        builder.setDebugMerchantId(MERCHANT_ID);// Debug Merchant ID
        builder.setDebugSalt(MERCHANT_SALT);// Debug Salt

     //   builder.setAmount(1);
        builder.setAmount(grandTotal);
        builder.setTnxId(txnid);
        builder.setPhone(CustomerContact);
        builder.setProductName("Cremeville");
        builder.setFirstName(CustomerName);
        builder.setEmail("email");
        builder.setsUrl("http://139.162.42.96:4848/mobile-urls/success.html");
        builder.setfUrl("http://139.162.42.96:4848/mobile-urls/failure.html");
        builder.setUdf1(customerID);
        builder.setUdf2(txnid);
        builder.setUdf3("M"); //For Mobile Transaction
        builder.setUdf4("");
        builder.setUdf5("");
        PayUmoneySdkInitilizer.PaymentParam paymentParam = builder.build();
        PayUmoneySdkInitilizer.startPaymentActivityForResult(this,paymentParam);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
            //    Log.i(TAG, "Success - Payment ID : " + data.getStringExtra(SdkConstants.PAYMENT_ID));
                String paymentId = data.getStringExtra(SdkConstants.PAYMENT_ID);
               // showDialogMessage( "Payment Success Id : " + paymentId);
                postOrderDetails(CustomerID, STOREID,AddressId,DeliveryAddress,CartList,this.subtotal,
                        this.vatTax, this.deliveryValue,GrandTotal,CouponCode,CouponStatus,CouponAmount,LoyaltyStatus,LoyaltyAmount,LoyaltyPoints,"PAYUMONEY"
                        ,OrderOriginId, paymentId,"true",OrderStatus);
            }
            else if (resultCode == RESULT_CANCELED)
            {
           //     Log.i(TAG, "failure");
                showDialogMessage("cancelled");
            }
            else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED)
            {
         //       Log.i("app_activity", "failure");

                if (data != null)
                {
                    if (data.getStringExtra(SdkConstants.RESULT).equals("cancel"))
                    {

                    }
                    else
                    {
                        showDialogMessage("failure");
                    }
                }
                //Write your code if there's no result
            }

            else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK)
            {
          //      Log.i(TAG, "User returned without login");
                showDialogMessage( "User returned without login");
            }
        }

    }

    private void showDialogMessage(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Payment Screen");
        Userdata=preferenceUtils.getUserDetails();
        // CustomerID,CustomerName,CustomerContact,AccessToken
        CustomerID=Userdata.get(AppConstants.KEY_CUSTOMER_ID);
        CustomerName=Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
        CustomerContact=Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
        AccessToken=Userdata.get(AppConstants.KEY_ACCESS_TOKEN);


        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerID=" + CustomerID);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerName="+CustomerName);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerContact="+CustomerContact);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "AccessToken="+AccessToken);
    }

    private void initializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        txt_amount=(TextView)findViewById(R.id.txt_amount);

        btn_payumoney=(Button)findViewById(R.id.btn_payumoney);
        btn_card=(Button)findViewById(R.id.btn_card);
        btn_netBanking=(Button)findViewById(R.id.btn_netBanking);
        btn_cashOnDelivery=(Button)findViewById(R.id.btn_cashOnDelivery);
    }

    @Override
    public void dataRetreived(Response data) {
        hideloader();
        if (data != null && data.method == ServiceMethods.WS_POST_ORDER_DETAILS) {
            if (!data.isError) {
                arrPostOrderDO = (ArrayList<PostOrderDO>) data.data;
                if(arrPostOrderDO!=null&&arrPostOrderDO.size()>0)
                {
                    String OrderId= arrPostOrderDO.get(0).OrderId;
                    LogUtils.error(CheckoutActivity.class.getSimpleName(), "OrderId="+OrderId);
                    Intent intent=new Intent(mContext,ReceiptActivity.class);
                    intent.putExtra("OrderId",OrderId);
                    startActivity(intent);

                }
            }
        }
    }
}
