package com.nm.cremeville.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.utilities.FontType;

public class TermsCondition extends AppCompatActivity
{

    WebView wbv_terms;
    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount,tv_faq,tv_privacy_policy,tv_termscondition;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    FontType fonttype;
    FrameLayout fl_cart,fl_Filter;

    String URL,header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termscondition);
        mContext=TermsCondition.this;
        initview();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_aboutus);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        URL=getIntent().getExtras().getString("URL");
        header=getIntent().getExtras().getString("header");


        txt_normal_actionbar.setText(header);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });



    }

    private void showapptermsandprivacypolicymethod() {

        WebSettings webSettings = wbv_terms.getSettings();
        wbv_terms.getSettings().setJavaScriptEnabled(true);
        wbv_terms.getSettings().setLoadWithOverviewMode(true);
        wbv_terms.getSettings().setUseWideViewPort(true);
        wbv_terms.getSettings().setTextZoom(300);
        wbv_terms.setWebViewClient(new MyWebViewClient());

        wbv_terms.loadUrl(URL);

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        showapptermsandprivacypolicymethod();
    }


    private void initview() {

        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        wbv_terms= (WebView) findViewById(R.id.wbv_terms);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);


    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            super.onPageFinished(view, url);

        }
    }
}
