package com.nm.cremeville.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.ShowNotificationAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.ShowNotificationDO;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;

public class NotificationActivity extends BaseNew implements DataListener {

    Context mContext;
    private ListView lst_notification;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;
    private ArrayList<ShowNotificationDO> arrShowNotificationDO;
    ShowNotificationAdapter showNotificationAdapter;
    FrameLayout fl_cart,fl_Filter;
    FontType fonttype;
    LinearLayout ll_NoNotification;
    TextView txt_addfavourite;
    String Message;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        mContext=NotificationActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_notification);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("Notifications");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Notification Screen");
        getNotifications();

    }

    private void getNotifications()
    {
        showLoaderNew();
        if(new CommonBL(mContext, NotificationActivity.this).getNotifications()) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }


    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        lst_notification      = (ListView) findViewById(R.id.lst_notification);
        ll_NoNotification  =(LinearLayout) findViewById(R.id.ll_NoNotification);

        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        txt_addfavourite=(TextView) findViewById(R.id.txt_addfavourite);
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_SHOW_NOTIFICATIONS)
        {
            if(!data.isError)
            {
                arrShowNotificationDO = (ArrayList<ShowNotificationDO>) data.data;
                if(arrShowNotificationDO!=null&&arrShowNotificationDO.size()>0)
                {
                    for (int i=0;i<arrShowNotificationDO.size();i++)
                    {
                       Message=arrShowNotificationDO.get(i).Status;
                    }
                    if(Message.equalsIgnoreCase("success"))
                    {
                        ll_NoNotification.setVisibility(View.GONE);
                        lst_notification.setVisibility(View.VISIBLE);

                        showNotificationAdapter =new ShowNotificationAdapter(mContext, arrShowNotificationDO);
                        lst_notification.setAdapter(showNotificationAdapter);
                    }
                    else
                    {
                        ll_NoNotification.setVisibility(View.VISIBLE);
                        lst_notification.setVisibility(View.GONE);
                    }
                }
                else
                {
                    ll_NoNotification.setVisibility(View.VISIBLE);
                    lst_notification.setVisibility(View.GONE);
                }
            }
        }
    }


}
