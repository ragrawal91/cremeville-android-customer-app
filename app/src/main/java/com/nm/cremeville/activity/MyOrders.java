package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.MyNewOrderAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MyOrders extends BaseNew implements DataListener {

    Context mContext;
    private ListView lst_orders;
    LinearLayout ll_NoItemCart,ll_orders;
    TextView txt_orderNow;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;
    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;
    FrameLayout fl_cart,fl_Filter;
    ArrayList<GetOrderDO>  arrgetOrderDO;
    ArrayList<ProductDO> productList;
    MyNewOrderAdapter myNewOrderAdapter;
    String Message;
    FontType fonttype;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);
        mContext=MyOrders.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_myorders);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("MyOrders");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_orderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(mContext,Dashboard.class);
                startActivity(intent);
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("My Orders Screen");
        if(preferenceUtils.isLoggedIn())
        {
            Userdata = preferenceUtils.getUserDetails();
            // CustomerID,CustomerName,CustomerContact,AccessToken
            CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
            CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
            CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
            AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);
            LogUtils.error(MyOrders.class.getSimpleName(), "CustomerID=" + CustomerID);
            LogUtils.error(MyOrders.class.getSimpleName(), "CustomerName=" + CustomerName);
            LogUtils.error(MyOrders.class.getSimpleName(), "CustomerContact=" + CustomerContact);
            LogUtils.error(MyOrders.class.getSimpleName(), "AccessToken=" + AccessToken);
        }

        getExistingOrders();

    }

    private void getExistingOrders()
    {
        JSONObject jsonObject=new JSONObject();
        //{"where":{"categoryId":"56cee6e8c4b8f7197ced15cf"}}
        JSONObject jObject=new JSONObject();
        try {
            jObject.put("customerId",CustomerID);
            jsonObject.put("where",jObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String queryParam=jsonObject.toString();
        showLoaderNew();
        if(new CommonBL(mContext, MyOrders.this).getExistingOrders(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        ll_NoItemCart=(LinearLayout)findViewById(R.id.ll_NoItemCart);
        ll_orders=(LinearLayout)findViewById(R.id.ll_orders);
        txt_orderNow=(TextView)findViewById(R.id.txt_orderNow);
        lst_orders=(ListView)findViewById(R.id.lst_orders);

    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_EXISTING_ORDER) {
            if (!data.isError) {
                arrgetOrderDO = (ArrayList<GetOrderDO>) data.data;
                if (arrgetOrderDO != null && arrgetOrderDO.size() > 0)
                {
                    Collections.reverse(arrgetOrderDO);
                    for(int i=0;i<arrgetOrderDO.size();i++)
                    {
                        Message=arrgetOrderDO.get(i).Status;
                    }
                    if(Message.equalsIgnoreCase("success"))
                    {
                        LogUtils.error(MyOrders.class.getSimpleName(), "no of items" + arrgetOrderDO.size());
                        ll_orders.setVisibility(View.VISIBLE);
                        ll_NoItemCart.setVisibility(View.INVISIBLE);
                        myNewOrderAdapter=new MyNewOrderAdapter(mContext,arrgetOrderDO);
                        lst_orders.setAdapter(myNewOrderAdapter);


                    }
                    else
                    {
                        ll_orders.setVisibility(View.INVISIBLE);
                        ll_NoItemCart.setVisibility(View.VISIBLE);
                    }

                }
                else
                {
                    ll_orders.setVisibility(View.INVISIBLE);
                    ll_NoItemCart.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
