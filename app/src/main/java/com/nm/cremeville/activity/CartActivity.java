package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.CartAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.objects.TaxesDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class CartActivity extends BaseNew implements CartListListener,DataListener {

    LinearLayout ll_cartActivity,ll_NoItemCart;
    Context mContext;
    Button btn_proceed;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount,txt_orderNow;
    public ImageView img_normal_actionbar,img_back,img_favourite;
    TextView txt_grandtotal,txt_deliveryCharge,txt_subtotal,txt_checkout;
    ListView lst_cartItems;
    String Message;
    PreferenceUtils preferenceUtils;
    public HashMap<String,String> Userdata;
    ArrayList<ItemsCartsDO> NewCartList;
    CartAdapter cartAdapter;
    DecimalFormat df=new DecimalFormat("00.00######");
    double subtotal,GrandTotal,vatTax,deliveryCharges;
    String deliveryValue;
    private ArrayList<TaxesDO> arrtaxes;
    double vatTaxValue,TotalTaxValue,MinAmount;
    String StartTime, EndTime;
    boolean OrderingStatus;
    int CartCount;
    FrameLayout fl_cart,fl_Filter;
    FontType fonttype;
    TextView tv_addmoreitem;
    public HashMap<String,String> StoreData;
    String StoreID;
    Crouton mCrouton = null;
    Style style;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_screen);
        mContext=CartActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        CartDatabase.init(this);
        initializeControls();
        NewCartList=new ArrayList<>();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_cartScreen);
        fonttype = new FontType(mContext, root);


        fl_cart.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);

        style = new Style.Builder()
                .setBackgroundColor(R.color.yellow)
                .setGravity(Gravity.CENTER)
                .setTextColor(android.R.color.black)
                .setHeight(60)
                .build();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        txt_normal_actionbar.setText("My Cart");

        img_favourite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (preferenceUtils.isLoggedIn()) {
                    Intent intent = new Intent(mContext, MyFavourites.class);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("key", "cartActivity");
                    startActivity(intent);
                }
            }
        });

        txt_checkout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CartDatabase.clearCouponDatabase();
                if(!StoreID.equals("NA"))
                {
                    if(OrderingStatus)
                    {
                        if(preferenceUtils.isLoggedIn())
                        {
                            Intent intent=new Intent(mContext,CheckoutActivity.class);
                            intent.putExtra("subtotal",subtotal);
                            intent.putExtra("vatTax",vatTax);
                            intent.putExtra("deliveryCharges",deliveryValue);
                            intent.putExtra("GrandTotal",GrandTotal);
                            startActivity(intent);
                        }
                        else
                        {
                            Intent intent=new Intent(mContext,LoginActivity.class);
                            intent.putExtra("key","Cart");
                            intent.putExtra("subtotal",subtotal);
                            intent.putExtra("vatTax",vatTax);
                            intent.putExtra("deliveryCharges",deliveryValue);
                            intent.putExtra("GrandTotal",GrandTotal);
                            startActivity(intent);
                        }
                    }
                    else
                    {
                        showAlertDialog("Sorry, you can place your orders in between"+StartTime+" to "+EndTime,"OK");
                    }
                }
                else
                {
                    showAlertDialog("Sorry, we do not deliver at your location","OK");
                }
            }
        });

        txt_orderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(mContext,Dashboard.class);
                startActivity(intent);
            }
        });

        tv_addmoreitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(mContext,Dashboard.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Cart Screen");
        StoreData=preferenceUtils.getStoreId();
        StoreID=StoreData.get(AppConstants.KEY_STORE_ID);

        if(CartDatabase.getCartlistCount()>0)
        {
            Map<String, Object> cartScreen = new HashMap<String, Object>();
            cartScreen.put(AFInAppEventParameterName.LEVEL,5);
            cartScreen.put(AFInAppEventParameterName.SCORE,50);
            AppsFlyerLib.getInstance().trackEvent(mContext,"Cart Screen",cartScreen);

            getTaxes();
            ll_NoItemCart.setVisibility(View.GONE);
            ll_cartActivity.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_NoItemCart.setVisibility(View.VISIBLE);
            ll_cartActivity.setVisibility(View.GONE);
        }


    }

    private void getTaxes()
    {
            showLoaderNew();
            if(new CommonBL(mContext, CartActivity.this).getTaxes()) {
            }
            else {
                hideloader();
                showAlertDialog("NO INTERNET", "OK");
            }
    }

    private void callLocaldatabase()
    {
        mCrouton = Crouton.makeText(CartActivity.this, "Free delivery on order above "+"₹ " +String.format("%.2f", MinAmount), style,ll_cartActivity).
                setConfiguration(AppConstants.CONFIGURATION_LONG);
        mCrouton.show();

        NewCartList=CartDatabase.getAllCartList();
        CartCount=CartDatabase.getCartlistCount();
        if(CartCount>0)
        {
           txt_cartCount.setVisibility(View.VISIBLE);
            txt_cartCount.setText(""+CartCount );
        }
        else
        {
            txt_cartCount.setVisibility(View.GONE);
        }
        calculateSubtotalPrice(NewCartList);
        cartAdapter=new CartAdapter(mContext,NewCartList);
        cartAdapter.registerLocaldatabaseListener(this);
        lst_cartItems.setAdapter(cartAdapter);
        LogUtils.error(CartActivity.class.getSimpleName(), "Size=" + NewCartList.size());
    }

    private void calculateSubtotalPrice(ArrayList<ItemsCartsDO> newCartList) {

        double tempsubtotal=0;
        for(int i=0;i<newCartList.size();i++)
        {
            ItemsCartsDO model=newCartList.get(i);
            tempsubtotal=model.ItemQty*Double.parseDouble(model.ItemPrice)+tempsubtotal;

        }
        subtotal=tempsubtotal;

        txt_subtotal.setText("₹ "+ String.format("%.2f", subtotal));

        calculateTax(subtotal);

        if(subtotal<MinAmount)
        {
            deliveryValue=String.format("%.0f", deliveryCharges);
            calculateGrandTotal(subtotal, vatTax, deliveryCharges);
            txt_deliveryCharge.setText("₹ " + deliveryValue);
        }

        else
        {
            deliveryValue="free";
            txt_deliveryCharge.setText(deliveryValue);
            calculateGrandTotal(subtotal,vatTax,0);
        }
    }

    private void calculateGrandTotal(double subtotal, double vatTax, double deliveryCharges)
    {
        GrandTotal=subtotal+vatTax+deliveryCharges;
        TotalTaxValue=vatTax+deliveryCharges;
        txt_grandtotal.setText("₹ " + String.format("%.2f", GrandTotal));
    }

    private void calculateTax(double subtotal)
    {
        vatTax=subtotal*(vatTaxValue/100);
        //txt_vattax.setText("₹ " + String.format("%.2f", vatTax));
    }


    private void initializeControls()
    {
        lst_cartItems=(ListView)findViewById(R.id.lst_cartItems);
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);

        txt_grandtotal=(TextView) findViewById(R.id.txt_grandtotal);
       // txt_vattax=(TextView) findViewById(R.id.txt_vattax);
        txt_deliveryCharge=(TextView) findViewById(R.id.txt_deliveryCharge);
        txt_subtotal=(TextView) findViewById(R.id.txt_subtotal);
        txt_checkout=(TextView) findViewById(R.id.txt_checkout);
        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        ll_cartActivity=(LinearLayout) findViewById(R.id.ll_cartActivity);
        ll_NoItemCart=(LinearLayout) findViewById(R.id.ll_NoItemCart);

        txt_orderNow=(TextView) findViewById(R.id.txt_orderNow);
        tv_addmoreitem=(TextView) findViewById(R.id.tv_addmoreitem);

    }

    @Override
    public void getCartList(ItemsDO model) {

    }



    @Override
    public void getLocalDatabaseCartList() {

        callLocaldatabase();

        if(NewCartList.size()>0)
        {
            cartAdapter = new CartAdapter(mContext, NewCartList);
            calculateSubtotalPrice(NewCartList);
            cartAdapter.registerLocaldatabaseListener(this);
            lst_cartItems.setAdapter(cartAdapter);
        }
        else
        {
            finish();
        }

    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice) {
        double tempPrice=itemFinalPrice+subtotal;
        subtotal=tempPrice;
        txt_subtotal.setText("₹ "+String.format("%.2f", tempPrice));
        calculateTax(subtotal);
        if(subtotal<MinAmount)
        {
            deliveryValue=String.format("%.0f", deliveryCharges);
            calculateGrandTotal(subtotal, vatTax, Double.parseDouble(deliveryValue));
            txt_deliveryCharge.setText("₹ "+deliveryValue);
        }

        else
        {
            deliveryValue="free";
            txt_deliveryCharge.setText(deliveryValue);
            calculateGrandTotal(subtotal, vatTax, 0);
        }

    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice) {

        double tempPrice=subtotal-itemFinalPrice;
        subtotal=tempPrice;
        txt_subtotal.setText("₹ "+String.format("%.2f", tempPrice));
        calculateTax(subtotal);
        if(subtotal<MinAmount)
        {
            deliveryValue=String.format("%.0f", deliveryCharges);
            calculateGrandTotal(subtotal, vatTax, Double.parseDouble(deliveryValue));
            txt_deliveryCharge.setText("₹ "+deliveryValue);
        }

        else
        {
            deliveryValue="free";
            txt_deliveryCharge.setText(deliveryValue);
            calculateGrandTotal(subtotal,vatTax,0);
        }
    }

    @Override
    public void addFavouriteItem(ItemsDO model) {

    }

    @Override
    public void removeFavouriteItem(ItemsDO model) {

    }


    @Override
    public void getSizeFavouriteItemList(int Size) {

    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_TAXES)
        {
            if(!data.isError)
            {
                arrtaxes = (ArrayList<TaxesDO>) data.data;
                if(arrtaxes!=null&&arrtaxes.size()>0)
                {
                   for(int i=0;i<arrtaxes.size();i++)
                   {
                       vatTaxValue=arrtaxes.get(i).vatTax;
                       deliveryCharges=arrtaxes.get(i).deliveryCharge;
                       MinAmount=arrtaxes.get(i).minAmount;
                       StartTime=arrtaxes.get(i).StartTime;
                       EndTime=arrtaxes.get(i).EndTime;
                       OrderingStatus=arrtaxes.get(i).OrderingStatus;
                   }

                    LogUtils.error(CartActivity.class.getSimpleName(), "StartTime=" + StartTime);
                    LogUtils.error(CartActivity.class.getSimpleName(), "EndTime=" + EndTime);

                    callLocaldatabase();
                }
            }
        }
    }
}
