package com.nm.cremeville.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.CouponCartAdapter;
import com.nm.cremeville.adapter.StockCheckAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.AddressDO;
import com.nm.cremeville.objects.CouponDO;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.objects.SaveAddressDO;
import com.nm.cremeville.objects.StockCheckDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class CheckoutActivity extends BaseNew implements DataListener {

    LinearLayout ll_Cart,ll_checkoutroot,ll_homelayout,ll_officelayout,ll_otherslayout;
    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    TextView txt_no,txt_yes,txt_text,txt_text1,txt_text2,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_backfilter,img_cart;
    PreferenceUtils preferenceUtils;

    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;

    public HashMap<String,String> loyaltyData;
    String TotalLoyaltyPoints,totalLoyaltyAmount;

    public HashMap<String,String> UserAddress;
    String Areaname,CityName;

    public HashMap<String,String> StoreData;

    String StoreID,OfficeStoreID,HomeStoreID,OthersStoreID;

    ImageView img_favourite;
    FrameLayout fl_cart,fl_Filter;
    double subtotal,GrandTotal,GrandTotalTemp,vatTax;
    String  deliveryValue;
    TextView txt_grandtotal,txt_addresstype,txt_cremeeAmount,txt_couponamount,txt_newgrandtotal;
    DecimalFormat df=new DecimalFormat("00.00######");
    Button btn_proceed,btn_applyCoupon;
    LinearLayout ll_coupon,ll_cremee,ll_redeemcremee,ll_PayableAmount;
    EditText edt_couponCode;
    TextView txt_cremeePoints,txt_homeAddress,txt_officeAddress,txt_othersAddress,txt_PayableAmount,txt_deliveryAddress;
    CheckBox chk_addHomeAddress,chk_addofficeAddress,chk_addothersAddress;
    LinearLayout ll_addresslayout,ll_checkoutlayout;

    private Animation animUp;
    private Animation animDown;

    EditText edt_housenumber,edt_address,edt_landmark,edt_city,edt_pincode,edt_areaname;
    Button btn_SaveAddress;
    String addressKey,DeliveryAddress="null",HomeAddressId,OfficeAddressId,OtherAddressId,AddressId;
    String HouseNumber,Address,Landmark,City,Pincode,AreaName;
    ArrayList<SaveAddressDO>  arrSaveAddressDO;
    ArrayList<AddressDO>  arrAddressDO;
    String AddressFlag;
    AddressDO addressDO;
    StringBuilder strHomeAddress,strOfficeAddress,strOthersAddress;
    ArrayList<ItemsCartsDO> CartList;
    String CouponCode="null",CouponStatus="0",CouponAmount="0",LoyaltyStatus="0",LoyaltyAmount="0",LoyaltyPoints="0";
    ArrayList<CouponDO>  arrCouponDO;
    ArrayList<StockCheckDO>  arrStockCheckDO;
    ArrayList<ProductDO>  outOfStockList;
    String CouponApplied;
    TextView txt_couponAmount,txt_home,txt_others,txt_office,txt_coupondesc;
    Crouton mCrouton = null;
    Style style;
    FontType fonttype;

    ListView lst_cartItems,lst_cartstockcheck;
    ImageView img_remove;
    ArrayList<ProductDO> NewCartList;
    CouponCartAdapter couponCartAdapter;
    StockCheckAdapter stockCheckAdapter;
    public View footer_cartitems;
    boolean AddressTab;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_screen);
        mContext=CheckoutActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);

        CartDatabase.init(this);
        initializeControls();

        Map<String, Object> checkoutScreen = new HashMap<String, Object>();
        checkoutScreen.put(AFInAppEventParameterName.LEVEL,6);
        checkoutScreen.put(AFInAppEventParameterName.SCORE,60);
        AppsFlyerLib.getInstance().trackEvent(mContext,"Checkout Screen",checkoutScreen);

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
        CartDatabase.init(this);
        ViewGroup root = (ViewGroup)findViewById(R.id.ll_checkoutroot);
        fonttype = new FontType(mContext, root);


        animUp = AnimationUtils.loadAnimation(this, R.anim.anim_up);
        animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txt_normal_actionbar.setText("Checkout");

        fl_cart.setVisibility(View.VISIBLE);
        img_cart.setImageResource(R.mipmap.home);
        txt_cartCount.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);

        subtotal = getIntent().getExtras().getDouble("subtotal");
        vatTax = getIntent().getExtras().getDouble("vatTax");
        deliveryValue = getIntent().getExtras().getString("deliveryCharges");
        GrandTotal = getIntent().getExtras().getDouble("GrandTotal");
        GrandTotalTemp=GrandTotal;
       LogUtils.error(CheckoutActivity.class.getSimpleName(),"deliveryValue="+deliveryValue);

         style = new Style.Builder()
                .setBackgroundColor(R.color.yellow)
                .setGravity(Gravity.CENTER)
                .setTextColor(android.R.color.black)
                .setHeight(60)
                .build();

        txt_grandtotal.setText("₹ " + String.format("%.2f", GrandTotal));

        fl_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(CouponStatus.equals("1"))
                {
                    //  showAlertDialog("Coupon applied will be discarded","OK");
                    showDialogHome(mContext);
                }
                else
                {
                    Intent intent=new Intent(mContext,Dashboard.class);
                    startActivity(intent);
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(CouponStatus.equals("1"))
                {
                  //  showAlertDialog("Coupon applied will be discarded","OK");
                    showDialogRemove(mContext);
                }
                else
                {
                    finish();
                }
            }
        });




        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CouponStatus.equals("1"))
                {
                    //  showAlertDialog("Coupon applied will be discarded","OK");
                    showDialogRemove(mContext);
                }
                else
                {
                    finish();
                }
            }
        });

        btn_applyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CouponCode=edt_couponCode.getText().toString();
                if(CouponCode.isEmpty() && CouponCode.equalsIgnoreCase(""))
                {
                    showAlertDialog("Enter coupon code","OK");
                }
                else
                {
                    //btn_applyCoupon.setEnabled(false);
                    if(CouponStatus.equals("1"))
                    {
                        mCrouton = Crouton.makeText(CheckoutActivity.this, "Coupon already applied", style,ll_checkoutlayout).
                                setConfiguration(AppConstants.CONFIGURATION_SHORT);
                        mCrouton.show();
                    }
                    else
                    {
                        applyCoupon(CustomerID,GrandTotalTemp,CouponCode,CartList,deliveryValue,LoyaltyAmount);
                    }
                }

            }
        });

        ll_redeemcremee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(!TotalLoyaltyPoints.equals("0"))
                {
                    LoyaltyStatus="1";
                    LoyaltyPoints=TotalLoyaltyPoints;
                    LoyaltyAmount=totalLoyaltyAmount;

                    txt_cremeePoints.setText("0");
                    ll_redeemcremee.setEnabled(false);
                    ll_cremee.setVisibility(View.VISIBLE);
                    txt_cremeeAmount.setText("-₹ " + totalLoyaltyAmount);
                    GrandTotalTemp=GrandTotalTemp-Double.valueOf(totalLoyaltyAmount);
                    ll_PayableAmount.setVisibility(View.VISIBLE);
                    txt_PayableAmount.setText("₹ "+String.format("%.2f",GrandTotalTemp));
                }
                else
                {
                    showAlertDialog("You have 0 cremee","OK");
                }


            }
        });


        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyApplication.getInstance().trackEvent("Proceed Button", "proceeding to payment screen", "Checkout screen");
                if(!DeliveryAddress.equalsIgnoreCase("null"))
                {
                    if(CouponStatus.equals("1"))
                    {
                        CartList=CartDatabase.getAllCouponCartList();
                    }
                    else
                    {
                        CartList=CartDatabase.getAllCartList();
                    }
                    outOfStockList.clear();
                    applystockCheck(CartList,StoreID);
                }
                else
                {
                    showAlertDialog("Please choose a delivery address","OK");
                }
            }
        });

        chk_addHomeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AddressTab=true;
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_checkoutlayout.setVisibility(View.GONE);
                fl_cart.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Home Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey="Home";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);

                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Home Address");
            }
        });

        chk_addofficeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AddressTab=true;
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_checkoutlayout.setVisibility(View.GONE);
                fl_cart.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Office Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey = "Office";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);

                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Office Address");
            }
        });

        chk_addothersAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AddressTab=true;
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_checkoutlayout.setVisibility(View.GONE);
                fl_cart.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Others Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey = "Others";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);


                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Others Address");
            }
        });

        btn_SaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                HouseNumber=edt_housenumber.getText().toString();
                Address=edt_address.getText().toString();
                Landmark=edt_landmark.getText().toString();
                City=edt_city.getText().toString();
                Pincode=edt_pincode.getText().toString();
                AreaName=edt_areaname.getText().toString();

                StoreData=preferenceUtils.getStoreId();
                String SavedStoreID;
                SavedStoreID=StoreData.get(AppConstants.KEY_STORE_ID);
                LogUtils.error(CheckoutActivity.class.getSimpleName(), "SavedStoreID=" + SavedStoreID);

                checkAddresskeyvalidations(HouseNumber, Address, Landmark, City, Pincode,AreaName,SavedStoreID);
            }
        });

        txt_homeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                DeliveryAddress=strHomeAddress.toString();
                AddressId=HomeAddressId;
                StoreID=HomeStoreID;
                txt_deliveryAddress.setText(DeliveryAddress);

                ll_homelayout.setBackgroundResource(R.drawable.edt_pinkcurve);
                ll_officelayout.setBackgroundResource(R.drawable.edt_graycurve);
                ll_otherslayout.setBackgroundResource(R.drawable.edt_graycurve);

                txt_home.setTextColor(Color.parseColor("#FFFFFF"));
                txt_office.setTextColor(Color.parseColor("#000000"));
                txt_others.setTextColor(Color.parseColor("#000000"));

                txt_home.setBackgroundResource(R.drawable.edt_darkpinkcurve);
                txt_office.setBackgroundResource(R.drawable.edt_darkgraycurve);
                txt_others.setBackgroundResource(R.drawable.edt_darkgraycurve);


                txt_homeAddress.setTextColor(Color.parseColor("#FFFFFF"));
                txt_officeAddress.setTextColor(Color.parseColor("#000000"));
                txt_othersAddress.setTextColor(Color.parseColor("#000000"));

            }
        });

        txt_officeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                DeliveryAddress=strOfficeAddress.toString();
                AddressId=OfficeAddressId;
                StoreID=OfficeStoreID;
                txt_deliveryAddress.setText(DeliveryAddress);

                ll_homelayout.setBackgroundResource(R.drawable.edt_graycurve);
                ll_officelayout.setBackgroundResource(R.drawable.edt_pinkcurve);
                ll_otherslayout.setBackgroundResource(R.drawable.edt_graycurve);

                txt_home.setTextColor(Color.parseColor("#000000"));
                txt_office.setTextColor(Color.parseColor("#FFFFFF"));
                txt_others.setTextColor(Color.parseColor("#000000"));

                txt_home.setBackgroundResource(R.drawable.edt_darkgraycurve);
                txt_office.setBackgroundResource(R.drawable.edt_darkpinkcurve);
                txt_others.setBackgroundResource(R.drawable.edt_darkgraycurve);

                txt_homeAddress.setTextColor(Color.parseColor("#000000"));
                txt_officeAddress.setTextColor(Color.parseColor("#FFFFFF"));
                txt_othersAddress.setTextColor(Color.parseColor("#000000"));
            }
        });

        txt_othersAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                DeliveryAddress=strOthersAddress.toString();
                AddressId=OtherAddressId;
                StoreID=OthersStoreID;
                txt_deliveryAddress.setText(DeliveryAddress);

                ll_homelayout.setBackgroundResource(R.drawable.edt_graycurve);
                ll_officelayout.setBackgroundResource(R.drawable.edt_graycurve);
                ll_otherslayout.setBackgroundResource(R.drawable.edt_pinkcurve);

                txt_home.setTextColor(Color.parseColor("#000000"));
                txt_office.setTextColor(Color.parseColor("#000000"));
                txt_others.setTextColor(Color.parseColor("#FFFFFF"));

                txt_home.setBackgroundResource(R.drawable.edt_darkgraycurve);
                txt_office.setBackgroundResource(R.drawable.edt_darkgraycurve);
                txt_others.setBackgroundResource(R.drawable.edt_darkpinkcurve);

                txt_homeAddress.setTextColor(Color.parseColor("#000000"));
                txt_officeAddress.setTextColor(Color.parseColor("#000000"));
                txt_othersAddress.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });

        img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                closeAddressLayout();
            }
        });

        img_backfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAddressLayout();
            }
        });

        edt_areaname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("You can change your locality only from dashboard", "OK");
            }
        });

        edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("You can change your locality only from dashboard","OK");
            }
        });


    }

    private void applystockCheck(ArrayList<ItemsCartsDO> cartList, String storeID)
    {

        showLoaderNew();
        if (new CommonBL(mContext, CheckoutActivity.this).applystockCheck(cartList,storeID))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    @Override
    public void onBackPressed()
    {
        if(AddressTab)
        {
            closeAddressLayout();
        }
        else
        {
            if(CouponStatus.equals("1"))
            {
                //  showAlertDialog("Coupon applied will be discarded","OK");
                showDialogRemove(mContext);
            }
            else
            {
                finish();
            }
        }
    }

    public void showDialogRemove(Context context)
    {
        dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogremove);
        dialog.setCanceledOnTouchOutside(false);

        txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_no= (TextView) dialog.findViewById(R.id.txt_no);
        txt_text1= (TextView) dialog.findViewById(R.id.txt_text1);
        txt_text2= (TextView) dialog.findViewById(R.id.txt_text2);
        txt_text= (TextView) dialog.findViewById(R.id.txt_text);


        txt_text1.setVisibility(View.GONE);
        txt_text2.setVisibility(View.GONE);

        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(context, root);

        txt_text.setText("Applied coupon would be discarded");

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDatabase.clearCouponDatabase();
                dialog.dismiss();
                finish();

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();
    }

    public void showDialogHome(Context context)
    {
        dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogremove);
        dialog.setCanceledOnTouchOutside(false);

        txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_no= (TextView) dialog.findViewById(R.id.txt_no);
        txt_text1= (TextView) dialog.findViewById(R.id.txt_text1);
        txt_text2= (TextView) dialog.findViewById(R.id.txt_text2);
        txt_text= (TextView) dialog.findViewById(R.id.txt_text);


        txt_text1.setVisibility(View.GONE);
        txt_text2.setVisibility(View.GONE);

        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(context, root);

        txt_text.setText("Applied coupon would be discarded");

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDatabase.clearCouponDatabase();
                dialog.dismiss();
                Intent intent=new Intent(mContext,Dashboard.class);
                startActivity(intent);

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void closeAddressLayout() {

        AddressTab=false;
        img_favourite.setVisibility(View.INVISIBLE);
        img_normal_actionbar.setVisibility(View.VISIBLE);
        txt_normal_actionbar.setText("Checkout");
        ll_addresslayout.startAnimation(animDown);
        ll_addresslayout.setVisibility(View.GONE);
        ll_checkoutlayout.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);
        img_backfilter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.VISIBLE);
    }

    private void checkAddresskeyvalidations(String houseNumber, String address, String landmark, String city, String pincode,String areaName,String storeId)
    {
        if(!houseNumber.equalsIgnoreCase("") && !houseNumber.isEmpty())
        {
            if(!address.equalsIgnoreCase("") && !address.isEmpty())
            {
                if(!landmark.equalsIgnoreCase("") && !landmark.isEmpty())
                {
                    if(!city.equalsIgnoreCase("") && !city.isEmpty())
                        {
                            SaveAddressDetailsToServer(CustomerID,houseNumber,address,landmark,city,pincode,areaName,storeId);
                        }
                    else
                        {
                            showAlertDialog("Please enter city name","OK");
                        }
                }
                else
                {
                    showAlertDialog("Please enter landmark","OK");
                }
            }
            else
            {
                showAlertDialog("Please enter complete address","OK");
            }

        }
        else
        {
            showAlertDialog("Please enter house number","OK");
        }

    }

    private void SaveAddressDetailsToServer(String customerID, String houseNumber, String address, String landmark,
                                            String city, String pincode,String AreaName,String storeId)
    {
        String queryParam=CustomerID+"/addresses?access_token="+AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, CheckoutActivity.this).saveAddressDetailsToServer(customerID, houseNumber, address, landmark,
                city, pincode,AreaName,storeId, addressKey, queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void getAddressDetails()
    {
        JSONObject jsonObject=new JSONObject();
        //{"where":{"categoryId":"56cee6e8c4b8f7197ced15cf"}}
        JSONObject jObject=new JSONObject();
        try {
            jObject.put("customerId",CustomerID);
            jsonObject.put("where",jObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String queryParam=jsonObject.toString();
        showLoaderNew();
        if(new CommonBL(mContext, CheckoutActivity.this).getAddressDetails(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void applyCoupon(String customerID, double grandTotal, String couponCode, ArrayList<ItemsCartsDO> cartList,
                             String deliveryValue, String loyaltyAmount)
    {
        showLoaderNew();
        if(new CommonBL(mContext, CheckoutActivity.this).applyCoupon(customerID, grandTotal, couponCode, cartList,deliveryValue,loyaltyAmount))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Checkout Screen");
        AddressTab=false;
        CartList=CartDatabase.getAllCartList();

        Userdata = preferenceUtils.getUserDetails();
        // CustomerID,CustomerName,CustomerContact,AccessToken
        CustomerID=Userdata.get(AppConstants.KEY_CUSTOMER_ID);
        CustomerName=Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
        CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
        AccessToken=Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

        loyaltyData=preferenceUtils.getLoyaltyPoints();
        TotalLoyaltyPoints=loyaltyData.get(AppConstants.KEY_TOTAL_POINTS);
        totalLoyaltyAmount=loyaltyData.get(AppConstants.KEY_LOYALTY_AMOUNT);

        txt_cremeePoints.setText(TotalLoyaltyPoints);

        UserAddress=preferenceUtils.getUserAddress();
        Areaname=UserAddress.get(AppConstants.KEY_SUB_AREA_NAME);
        CityName=UserAddress.get(AppConstants.KEY_CITY_NAME);

        outOfStockList=new ArrayList<>();



        getAddressDetails();

        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerID=" + CustomerID);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerName="+CustomerName);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "CustomerContact="+CustomerContact);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "AccessToken="+AccessToken);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "TotalLoyaltyPoints="+TotalLoyaltyPoints);
        LogUtils.error(CheckoutActivity.class.getSimpleName(), "totalLoyaltyAmount=" + totalLoyaltyAmount);
    }

    private void initializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_backfilter=(ImageView)findViewById(R.id.img_backfilter);
        img_cart=(ImageView)findViewById(R.id.img_cart);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);


        txt_grandtotal=(TextView) findViewById(R.id.txt_grandtotal);
        txt_PayableAmount=(TextView) findViewById(R.id.txt_PayableAmount);
        txt_addresstype=(TextView) findViewById(R.id.txt_addresstype);
        txt_deliveryAddress=(TextView) findViewById(R.id.txt_deliveryAddress);

        txt_cremeePoints=(TextView) findViewById(R.id.txt_cremeePoints);
        txt_cremeeAmount=(TextView) findViewById(R.id.txt_cremeeAmount);
        txt_homeAddress=(TextView) findViewById(R.id.txt_homeAddress);
        txt_officeAddress=(TextView) findViewById(R.id.txt_officeAddress);
        txt_othersAddress=(TextView) findViewById(R.id.txt_othersAddress);
        txt_couponAmount=(TextView) findViewById(R.id.txt_couponAmount);
        txt_coupondesc=(TextView) findViewById(R.id.txt_coupondesc);

        txt_home=(TextView) findViewById(R.id.txt_home);
        txt_others=(TextView) findViewById(R.id.txt_others);
        txt_office=(TextView) findViewById(R.id.txt_office);

        btn_proceed=(Button) findViewById(R.id.btn_proceed);
        btn_applyCoupon=(Button) findViewById(R.id.btn_applyCoupon);
        ll_coupon=(LinearLayout) findViewById(R.id.ll_coupon);
        ll_cremee=(LinearLayout) findViewById(R.id.ll_cremee);
        ll_redeemcremee=(LinearLayout) findViewById(R.id.ll_redeemcremee);
        edt_couponCode=(EditText) findViewById(R.id.edt_couponCode);
        chk_addHomeAddress=(CheckBox) findViewById(R.id.chk_addHomeAddress);
        chk_addofficeAddress=(CheckBox) findViewById(R.id.chk_addofficeAddress);
        chk_addothersAddress=(CheckBox) findViewById(R.id.chk_addothersAddress);

        ll_addresslayout=(LinearLayout)findViewById(R.id.ll_addresslayout);
        ll_checkoutlayout=(LinearLayout)findViewById(R.id.ll_checkoutlayout);
        ll_checkoutroot=(LinearLayout)findViewById(R.id.ll_checkoutroot);
        ll_PayableAmount=(LinearLayout)findViewById(R.id.ll_PayableAmount);

        ll_homelayout =(LinearLayout)findViewById(R.id.ll_homelayout);
        ll_officelayout=(LinearLayout)findViewById(R.id.ll_officelayout);
        ll_otherslayout=(LinearLayout)findViewById(R.id.ll_otherslayout);

        ll_addresslayout.bringToFront();

        edt_housenumber=(EditText) findViewById(R.id.edt_housenumber);
        edt_address=(EditText) findViewById(R.id.edt_address);
        edt_landmark=(EditText) findViewById(R.id.edt_landmark);
        edt_city=(EditText) findViewById(R.id.edt_city);
        edt_pincode=(EditText) findViewById(R.id.edt_pincode);
        edt_areaname=(EditText) findViewById(R.id.edt_areaname);
        btn_SaveAddress=(Button) findViewById(R.id.btn_SaveAddress);

    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_SAVE_ADDRESS_DETAILS)
        {
            if(!data.isError)
            {
                arrSaveAddressDO = (ArrayList<SaveAddressDO>) data.data;
                if(arrSaveAddressDO!=null&&arrSaveAddressDO.size()>0)
                {
                    getAddressDetails();
                    closeAddressLayout();
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_POST_STOCK_CHECK)
        {
            if(!data.isError)
            {
                arrStockCheckDO = (ArrayList<StockCheckDO>) data.data;
                if(arrStockCheckDO!=null&&arrStockCheckDO.size()>0)
                {
                    for(int k=0;k<arrStockCheckDO.get(0).stockList.size();k++)
                    {
                        ProductDO productDO=arrStockCheckDO.get(0).stockList.get(k);
                        if(!productDO.StockStatus)
                        {
                            outOfStockList.add(productDO);
                        }
                    }

                    LogUtils.error(CheckoutActivity.class.getSimpleName(), "outOfStockList size=" + outOfStockList.size());
                    if(outOfStockList.size()>0)
                    {
                        showDialogStockCheck(mContext,outOfStockList);
                    }
                    else
                    {
                        Intent intent=new Intent(mContext,PaymentActivity.class);
                        intent.putExtra("AddressId", AddressId);
                        intent.putExtra("STOREID",StoreID);
                        intent.putExtra("DeliveryAddress", DeliveryAddress);
                        intent.putExtra("subtotal", subtotal);
                        intent.putExtra("vatTax", vatTax);
                        intent.putExtra("deliveryCharges", deliveryValue);
                        intent.putExtra("GrandTotal", GrandTotalTemp);
                        intent.putExtra("CouponCode", CouponCode);
                        intent.putExtra("CouponStatus", CouponStatus);
                        intent.putExtra("CouponAmount", CouponAmount);
                        intent.putExtra("LoyaltyStatus", LoyaltyStatus);
                        intent.putExtra("LoyaltyAmount", LoyaltyAmount);
                        intent.putExtra("LoyaltyPoints", LoyaltyPoints);
                        startActivity(intent);
                    }
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_APPLY_COUPON)
        {
            if(!data.isError)
            {
                arrCouponDO = (ArrayList<CouponDO>) data.data;
                if(arrCouponDO!=null&&arrCouponDO.size()>0)
                {
                    for(int i=0;i<arrCouponDO.size();i++)
                    {
                        CouponApplied=arrCouponDO.get(i).Status;
                    }

                    if(CouponApplied.equalsIgnoreCase("success"))
                    {
                        ll_coupon.setVisibility(View.VISIBLE);
                        ll_PayableAmount.setVisibility(View.VISIBLE);

                        CouponCode=CouponCode;
                        edt_couponCode.setEnabled(false);
                        CouponStatus="1";
                        CouponAmount=arrCouponDO.get(0).DiscountAmount;
                        GrandTotalTemp=Double.valueOf(arrCouponDO.get(0).FinalValue);

                       /* if(deliveryValue.equals("free"))
                        {
                            GrandTotalTemp=GrandTotalTemp;
                        }
                        else
                        {
                            GrandTotalTemp=GrandTotalTemp+Double.valueOf(deliveryValue);
                        }*/

                        if(LoyaltyStatus.equals("1"))
                        {
                            LoyaltyPoints=TotalLoyaltyPoints;
                            LoyaltyAmount=totalLoyaltyAmount;
                            GrandTotalTemp=GrandTotalTemp-Double.valueOf(totalLoyaltyAmount);
                        }

                        txt_couponAmount.setText("-₹ " + CouponAmount);
                        txt_PayableAmount.setText("₹ " + String.format("%.2f", GrandTotalTemp));
                        txt_coupondesc.setText(arrCouponDO.get(0).CouponDesc);
                        LogUtils.error(Dashboard.class.getSimpleName(), "Product List Size=" + arrCouponDO.get(0).productList.size());

                        for(int k=0;k<arrCouponDO.get(0).productList.size();k++)
                        {
                            CartDatabase.addCouponCartData(new ItemsCartsDO(arrCouponDO.get(0).productList.get(k).ItemId, arrCouponDO.get(0).productList.get(k).ItemName, Integer.valueOf(arrCouponDO.get(0).productList.get(k).Quantity), arrCouponDO.get(0).productList.get(k).ItemPrice, arrCouponDO.get(0).productList.get(k).ItemImage,"DESC", "true", arrCouponDO.get(0).productList.get(k).ItemSize));
                        }

                        showCartListDialog(mContext, arrCouponDO);

                        LogUtils.error(Dashboard.class.getSimpleName(), "Coupon CartList Size=" + CartDatabase.getCouponCartlistCount());

                    }
                    else if (CouponApplied.equalsIgnoreCase("Invalid"))
                    {
                        mCrouton = Crouton.makeText(CheckoutActivity.this, "Invalid Coupon Code", style,ll_checkoutlayout).
                                setConfiguration(AppConstants.CONFIGURATION_SHORT);
                        mCrouton.show();
                        //showAlertDialog("Invalid Coupon Code","OK");
                    }
                }
            }
        }

       else if (data != null && data.method == ServiceMethods.WS_GET_ADDRESS_DETAILS)
        {
            if(!data.isError)
            {
                arrAddressDO = (ArrayList<AddressDO>) data.data;
                if(arrAddressDO!=null&&arrAddressDO.size()>0)
                {
                    for(int i=0;i<arrAddressDO.size();i++)
                    {
                        AddressFlag=arrAddressDO.get(i).Status;
                    }

                    if(AddressFlag.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrAddressDO.size();i++)
                        {
                            addressDO=arrAddressDO.get(i);
                            if(addressDO.AddressType.equalsIgnoreCase("Home"))
                            {
                                HomeAddressId=addressDO.Addressid;
                                HomeStoreID=addressDO.StoreId;
                                LogUtils.error(CheckoutActivity.class.getSimpleName(), "HomeAddressId="+HomeAddressId);
                                strHomeAddress=new StringBuilder();
                                strHomeAddress.append(addressDO.HouseNumber);
                                strHomeAddress.append(",");
                                strHomeAddress.append(addressDO.Areaname);
                                strHomeAddress.append(",");
                                strHomeAddress.append(addressDO.Address);
                                strHomeAddress.append(",");
                                strHomeAddress.append(addressDO.City);
                                strHomeAddress.append(",");
                                strHomeAddress.append(addressDO.Pincode);
                                strHomeAddress.append(",");
                                strHomeAddress.append(addressDO.Landmark);

                                chk_addHomeAddress.setVisibility(View.INVISIBLE);
                                txt_homeAddress.setVisibility(View.VISIBLE);
                                txt_homeAddress.setText(strHomeAddress.toString());
                            }

                            else if(addressDO.AddressType.equalsIgnoreCase("Office"))
                            {
                                OfficeAddressId=addressDO.Addressid;
                                OfficeStoreID=addressDO.StoreId;
                                LogUtils.error(CheckoutActivity.class.getSimpleName(), "OfficeAddressId="+OfficeAddressId);
                                strOfficeAddress=new StringBuilder();
                                strOfficeAddress.append(addressDO.HouseNumber);
                                strOfficeAddress.append(",");
                                strOfficeAddress.append(addressDO.Areaname);
                                strOfficeAddress.append(",");
                                strOfficeAddress.append(addressDO.Address);
                                strOfficeAddress.append(",");
                                strOfficeAddress.append(addressDO.City);
                                strOfficeAddress.append(",");
                                strOfficeAddress.append(addressDO.Pincode);
                                strOfficeAddress.append(",");
                                strOfficeAddress.append(addressDO.Landmark);

                                chk_addofficeAddress.setVisibility(View.INVISIBLE);
                                txt_officeAddress.setVisibility(View.VISIBLE);
                                txt_officeAddress.setText(strOfficeAddress.toString());
                            }

                            else if(addressDO.AddressType.equalsIgnoreCase("Others"))
                            {
                                OtherAddressId=addressDO.Addressid;
                                OthersStoreID=addressDO.StoreId;
                               LogUtils.error(CheckoutActivity.class.getSimpleName(), "OtherAddressId="+OtherAddressId);
                                strOthersAddress=new StringBuilder();
                                strOthersAddress.append(addressDO.HouseNumber);
                                strOthersAddress.append(",");
                                strOthersAddress.append(addressDO.Areaname);
                                strOthersAddress.append(",");
                                strOthersAddress.append(addressDO.Address);
                                strOthersAddress.append(",");
                                strOthersAddress.append(addressDO.City);
                                strOthersAddress.append(",");
                                strOthersAddress.append(addressDO.Pincode);
                                strOthersAddress.append(",");
                                strOthersAddress.append(addressDO.Landmark);

                                chk_addothersAddress.setVisibility(View.INVISIBLE);
                                txt_othersAddress.setVisibility(View.VISIBLE);
                                txt_othersAddress.setText(strOthersAddress.toString());
                            }
                        }
                    }
                }
            }
        }
    }

    private void showCartListDialog(Context mContext, ArrayList<CouponDO> arrCouponDO)
    {
        dialog  = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogcartitem);
        dialog.setCanceledOnTouchOutside(false);

        lst_cartItems = (ListView) dialog.findViewById(R.id.lst_cartItems);
        img_remove= (ImageView) dialog.findViewById(R.id.img_remove);
        footer_cartitems=getLayoutInflater().inflate(R.layout.footer_cartitems,null);

        txt_couponamount= (TextView) footer_cartitems.findViewById(R.id.txt_couponamount);
        txt_newgrandtotal= (TextView) footer_cartitems.findViewById(R.id.txt_newgrandtotal);

        lst_cartItems.addFooterView(footer_cartitems, null, false);

        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(mContext, root);

        NewCartList=arrCouponDO.get(0).productList;
        couponCartAdapter=new CouponCartAdapter(mContext,NewCartList);
        lst_cartItems.setAdapter(couponCartAdapter);
        txt_couponamount.setText("-₹ " + CouponAmount);
        txt_newgrandtotal.setText("₹ " + String.format("%.0f", GrandTotalTemp));



        img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void showDialogStockCheck(Context mContext, ArrayList<ProductDO> outOfStockList)
    {
        dialog  = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogstockcheck);
        dialog.setCanceledOnTouchOutside(false);

        lst_cartstockcheck = (ListView) dialog.findViewById(R.id.lst_cartstockcheck);
        txt_yes= (TextView) dialog.findViewById(R.id.txt_yes);


        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(mContext, root);

        stockCheckAdapter=new StockCheckAdapter(mContext,outOfStockList);
        lst_cartstockcheck.setAdapter(stockCheckAdapter);

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDatabase.clearCouponDatabase();
                dialog.dismiss();
                finish();

            }
        });

        dialog.show();
    }

}
