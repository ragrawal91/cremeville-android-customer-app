package com.nm.cremeville.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.OrderedItemsAdapter;
import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.PreferenceUtils;

import java.util.ArrayList;

public class OrderDetails extends BaseNew {

    Context mContext;
    private ListView lst_ordersitems;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;
    FrameLayout fl_cart,fl_Filter;
    ArrayList<GetOrderDO>  arrgetOrderDO;
    ArrayList<ProductDO> productList;
    FontType fonttype;
    OrderedItemsAdapter orderedItemsAdapter;
    public View footer_view;

    String OrderID,TotalAmount,OrderTime,Address,Subtotal,DeliveryCharge,CouponCode,CouponAmount,CouponStatus,PaymentMode;

    TextView tv_dateTime, tv_deliveryAddress, tv_amount,tv_print,tv_orderid;
    TextView tv_subtotal,tv_dlCharges,tv_grandtotal,tv_couponCode,tv_paidvia,tv_couponAmount;
    LinearLayout ll_coupon;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        mContext=OrderDetails.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();
        lst_ordersitems.addFooterView(footer_view, null, false);

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        try
        {
            Bundle bundle=getIntent().getExtras();

            getdatafromotheractivity(bundle);

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_myorderdetail);
        fonttype = new FontType(mContext, root);


        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("Order Detail");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void getdatafromotheractivity(Bundle bundle)
    {
        OrderID = bundle.getString("OrderID");
        TotalAmount = bundle.getString("TotalAmount");
        OrderTime = bundle.getString("OrderTime");
        Address = bundle.getString("Address");

        Subtotal = bundle.getString("Subtotal");
        DeliveryCharge = bundle.getString("DeliveryCharge");
        CouponCode = bundle.getString("CouponCode");
        CouponAmount = bundle.getString("CouponAmount");
        CouponStatus = bundle.getString("CouponStatus");
        PaymentMode = bundle.getString("PaymentMode");
        productList=bundle.getParcelableArrayList("ProductList");

        tv_amount.setText("₹ " + TotalAmount);
        tv_orderid.setText("Order id: # " + OrderID);
        tv_dateTime.setText(OrderTime);
        tv_deliveryAddress.setText("Delivery Address: " + Address);
        tv_subtotal.setText("₹ " + Subtotal);

        if(!DeliveryCharge.equals("free"))
        {
            tv_dlCharges.setText("₹ " + DeliveryCharge);
        }
        else
        {
            tv_dlCharges.setText(DeliveryCharge);
        }

        tv_grandtotal.setText("₹ " + TotalAmount);
        tv_paidvia.setText("Paid via: " + PaymentMode);

        if(CouponStatus.equals("1"))
        {
            ll_coupon.setVisibility(View.VISIBLE);
            tv_couponAmount.setText("-₹ " + CouponAmount);
        }

        orderedItemsAdapter=new OrderedItemsAdapter(mContext, productList);
        lst_ordersitems.setAdapter(orderedItemsAdapter);

    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Order Detail Screen");
    }



    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);


        lst_ordersitems=(ListView)findViewById(R.id.lst_ordersitems);

        tv_amount = (TextView) findViewById(R.id.tv_amount);
        tv_dateTime = (TextView) findViewById(R.id.tv_dateTime);
        tv_deliveryAddress = (TextView) findViewById(R.id.tv_deliveryAddress);
        tv_print= (TextView) findViewById(R.id.tv_print);
        tv_orderid= (TextView) findViewById(R.id.tv_orderid);
        footer_view=getLayoutInflater().inflate(R.layout.footer_myorder,null);

        tv_subtotal=(TextView)footer_view.findViewById(R.id.tv_subtotal);
        tv_dlCharges=(TextView)footer_view.findViewById(R.id.tv_dlCharges);
        tv_grandtotal=(TextView)footer_view.findViewById(R.id.tv_grandtotal);
        tv_couponCode=(TextView)footer_view.findViewById(R.id.tv_couponCode);
        tv_paidvia=(TextView)footer_view.findViewById(R.id.tv_paidvia);
        tv_couponAmount=(TextView)footer_view.findViewById(R.id.tv_couponAmount);
        ll_coupon=(LinearLayout)footer_view.findViewById(R.id.ll_coupon);


    }
}
