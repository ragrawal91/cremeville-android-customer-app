package com.nm.cremeville.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import com.nm.cremeville.R;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.NetworkUtils;
import com.nm.cremeville.utilities.PreferenceUtils;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity{

    Timer timer;
    private LayoutInflater minflater;
    Context mContext;
    AlertDialog alertDialog;
    PreferenceUtils preferenceUtils;
    public HashMap<String,String> UserAddress;
    String AreaName,CityName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = SplashActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        minflater = this.getLayoutInflater();
        timer = new Timer();

    }

    protected void onPause() {
        super.onPause();

        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        System.gc();
    }


    protected void onResume() {
        super.onResume();

        UserAddress=preferenceUtils.getUserAddress();
        AreaName=UserAddress.get(AppConstants.KEY_SUB_AREA_NAME);
        CityName=UserAddress.get(AppConstants.KEY_CITY_NAME);

        if (NetworkUtils.isNetworkConnectionAvailable(mContext))
        {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    if(AreaName.equals("NA"))
                    {
                        Intent registeractivity = new Intent(mContext, LocationActivity.class);
                        startActivity(registeractivity);
                        finish();
                    }
                    else
                    {
                        preferenceUtils.saveLoginState(true);
                        Intent registeractivity = new Intent(mContext, Dashboard.class);
                        startActivity(registeractivity);
                        finish();
                    }
                }
            }, 3000);
        } else {
            // getCustomToast(AppConstant.CHECK_NETWORK_CONN);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showAlertDialog(mContext, "INTERNET CONNECTION", AppConstants.CHECK_NETWORK_CONN, false);
                }
            });

        }

    }

    private void showAlertDialog(Context context, String title, String message,
                                 Boolean status) {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCanceledOnTouchOutside(false);
        if (status != null)
            alertDialog.setIcon((status) ? R.mipmap.error : R.mipmap.error);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Setting",new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                open_device_setting_screen();

            }
        });
        alertDialog.show();
    }

    private void open_device_setting_screen() {

        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
