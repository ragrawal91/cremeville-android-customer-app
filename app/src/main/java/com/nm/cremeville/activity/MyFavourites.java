package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.FavouritesAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.objects.AddFavouriteDO;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyFavourites extends BaseNew implements DataListener, CartListListener {

    Context mContext;
    private ListView lst_favitems;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;
    private ArrayList<ItemsDO> arrfavourites;
    FavouritesAdapter favouritesAdapter;
    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;
    String Message;
    FrameLayout fl_cart,fl_Filter;
    int CartCount;
    private ArrayList<AddFavouriteDO> arrAddFavouriteDO;
    FontType fonttype;
    LinearLayout ll_NoFavourite;
    TextView txt_addfavourite;
    ArrayList<ItemsCartsDO> LocalCartList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        mContext=MyFavourites.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_favourites);
        fonttype = new FontType(mContext, root);
        LocalCartList=new ArrayList<>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);

        txt_normal_actionbar.setText("Favourites");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        fl_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(mContext, CartActivity.class);
                startActivity(intent);
            }
        });

        txt_addfavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("My Favourite Screen");
        LocalCartList=CartDatabase.getAllCartList();
        if(preferenceUtils.isLoggedIn())
        {
            Userdata = preferenceUtils.getUserDetails();
            // CustomerID,CustomerName,CustomerContact,AccessToken
            CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
            CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
            CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
            AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

            LogUtils.error(MyFavourites.class.getSimpleName(), "CustomerID=" + CustomerID);
            LogUtils.error(MyFavourites.class.getSimpleName(), "CustomerName=" + CustomerName);
            LogUtils.error(MyFavourites.class.getSimpleName(), "CustomerContact=" + CustomerContact);
            LogUtils.error(MyFavourites.class.getSimpleName(), "AccessToken=" + AccessToken);

        }

        CartCount= CartDatabase.getCartlistCount();
        if(CartCount>0)
        {
            txt_cartCount.setVisibility(View.VISIBLE);
            txt_cartCount.setText(""+CartCount );
        }
        else
        {
            txt_cartCount.setVisibility(View.GONE);
        }

        getFavouriteItems(CustomerID);

    }

    private void removeFavouriteItemFromServer(ItemsDO model) {

        String queryParam=model.favouriteId+"?access_token="+AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, MyFavourites.this).removeFavouriteItemFromServer(queryParam))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }

    private void getFavouriteItems(String CustomerID)
    {
        String queryParam;
        JSONObject jsonObject=new JSONObject();
        JSONObject jObject=new JSONObject();
        try {
            jObject.put("createdBy",CustomerID);
            jsonObject.put("where",jObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        queryParam=jsonObject.toString();
        showLoaderNew();
        if(new CommonBL(mContext, MyFavourites.this).getFavouriteItems(queryParam))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }


    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        lst_favitems      = (ListView) findViewById(R.id.lst_favitems);
        ll_NoFavourite  =(LinearLayout) findViewById(R.id.ll_NoFavourite);

        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        txt_addfavourite=(TextView) findViewById(R.id.txt_addfavourite);
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_FAVOURITE_ITEMS)
        {
            if(!data.isError)
            {
                arrfavourites = (ArrayList<ItemsDO>) data.data;
                if(arrfavourites!=null&&arrfavourites.size()>0)
                {
                    for (int i=0;i<arrfavourites.size();i++)
                    {
                       Message=arrfavourites.get(i).Status;
                    }
                    if(Message.equalsIgnoreCase("success"))
                    {
                        ll_NoFavourite.setVisibility(View.GONE);
                        lst_favitems.setVisibility(View.VISIBLE);

                        if(LocalCartList.size()>0) {
                            for (int i = 0; i < arrfavourites.size(); i++)
                            {
                                for (int j = 0; j < LocalCartList.size(); j++)
                                {
                                    if (LocalCartList.get(j).ItemId.equalsIgnoreCase(arrfavourites.get(i).ItemId))
                                    {
                                        arrfavourites.get(i).cartIconStatus = true;
                                        arrfavourites.get(i).ItemQty = LocalCartList.get(j).ItemQty;
                                    }
                                }
                            }
                        }


                        favouritesAdapter =new FavouritesAdapter(mContext, arrfavourites);
                        favouritesAdapter.registerFavouriteItemListener(this);
                        lst_favitems.setAdapter(favouritesAdapter);
                    }
                    else
                    {
                        ll_NoFavourite.setVisibility(View.VISIBLE);
                        lst_favitems.setVisibility(View.GONE);
                    }


                }
                else
                {
                    ll_NoFavourite.setVisibility(View.VISIBLE);
                    lst_favitems.setVisibility(View.GONE);
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_DELETE_FAVOURITE_ITEMS)
        {
            if(!data.isError)
            {
                arrAddFavouriteDO = (ArrayList<AddFavouriteDO>) data.data;
                if(arrAddFavouriteDO!=null&&arrAddFavouriteDO.size()>0)
                {
                    //getFavouriteItems(CustomerID);
                }
            }
        }
    }

    @Override
    public void getCartList(ItemsDO model)
    {

        txt_cartCount.setVisibility(View.VISIBLE);
        txt_cartCount.setText("" + CartDatabase.getCartlistCount());
    }

    @Override
    public void getLocalDatabaseCartList() {

    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice) {

    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice) {

    }

    @Override
    public void addFavouriteItem(ItemsDO model) {

    }



    @Override
    public void removeFavouriteItem(ItemsDO model)
    {
        removeFavouriteItemFromServer(model);
    }

    @Override
    public void getSizeFavouriteItemList(int Size)
    {
        if(Size==0)
        {
                ll_NoFavourite.setVisibility(View.VISIBLE);
                lst_favitems.setVisibility(View.GONE);
        }

    }
}
