package com.nm.cremeville.activity;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.CategoryAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.CategoryDO;
import com.nm.cremeville.objects.LoginDO;
import com.nm.cremeville.objects.NearestStoreDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.CurrentDate;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.NetworkUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class Dashboard extends BaseActivity implements DataListener {

	LinearLayout ll_Dashboard;
	Context mContext;
	boolean doubleBackToExitPressedOnce = false;
	private TextView tvDashBoardNoData;
	PreferenceUtils preferenceUtils;
	GridView grd_category;
	private ArrayList<CategoryDO> arrCategories;
	CategoryAdapter categoryAdapter;
	NetworkUtils networkUtils;
	Date appsavedate,appcurrentdate;
	CurrentDate currentDate;
	int CartCount;
	ArrayList<NearestStoreDO>  arrNearestStoreDO;

	String Message;

	public HashMap<String,String> Userdata;
	public HashMap<String,String> loyaltyData;
	public HashMap<String,String> UserLocation;
	public HashMap<String,String> UserAddress;

	String CustomerID,CustomerName,Password,CustomerContact,AccessToken;
	String TotalLoyaltyPoints,totalLoyaltyAmount;
	String Latitude,Longitude,AreaName,CityName;


	private ArrayList<LoginDO> arrLogin;

	String accesstoken,customerid,password,customerphone,customername,customer_email,totalloyaltypoint,loyaltyamount;
    FontType fonttype;

	@Override
	public void initialize() {
		ll_Dashboard = (LinearLayout) inflater.inflate(R.layout.dashboard, null);
		llContent.addView(ll_Dashboard, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		mContext = Dashboard.this;
		preferenceUtils = new PreferenceUtils(mContext);
		networkUtils = new NetworkUtils();
		currentDate=new CurrentDate();
		CartDatabase.init(this);
		initializeControls();
		ViewGroup root = (ViewGroup)findViewById(R.id.ll_dashboard);
		fonttype = new FontType(mContext, root);

		AppsFlyerLib.getInstance().startTracking(this.getApplication(),"[n6Tb2ZQsXeTMtqMe9w3Cq9]");

		loadCategoryData();

		CartDatabase.clearCouponDatabase();

		//	CityId=preferenceUtils.getIntFromPreference("cityid", 1);
		fl_cart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
					Intent intent = new Intent(mContext, CartActivity.class);
					startActivity(intent);
			}
		});

		fl_baseHeader.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext,ManualLocation.class);
				startActivity(intent);

			}
		});

		img_actionbar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(preferenceUtils.isLoggedIn())
				{
					Intent intent = new Intent(mContext, MyFavourites.class);
					startActivity(intent);

				}
				else
				{
					Intent intent = new Intent(mContext, LoginActivity.class);
					intent.putExtra("key", "Dashboard");
					startActivity(intent);
				}
			}
		});


	}

	@Override
	public void goto_myProfileMethod() {

		if(preferenceUtils.isLoggedIn())
		{
			Intent intent = new Intent(mContext, MyProfile.class);
			startActivity(intent);

		}
		else
		{
			Intent intent = new Intent(mContext, LoginActivity.class);
			intent.putExtra("key", "MyProfile");
			startActivity(intent);
		}

	}

	@Override
	public void goto_myOrdersMethod() {

		if(preferenceUtils.isLoggedIn())
		{
			Intent intent = new Intent(mContext, MyOrders.class);
			startActivity(intent);

		}
		else
		{
			Intent intent = new Intent(mContext, LoginActivity.class);
			intent.putExtra("key", "MyOrders");
			startActivity(intent);
		}

	}

	@Override
	public void goto_myFavouritesMethod() {

		if(preferenceUtils.isLoggedIn())
		{
			Intent intent = new Intent(mContext, MyFavourites.class);
			startActivity(intent);

		}
		else
		{
			Intent intent = new Intent(mContext, LoginActivity.class);
			intent.putExtra("key", "MyFavourites");
			startActivity(intent);
		}
	}

	@Override
	public void goto_CouponsMethod() {

		Intent intent = new Intent(mContext, CouponsActivity.class);
		startActivity(intent);
	}

	@Override
	public void goto_MyCremeeMethod()
	{
		Intent intent = new Intent(mContext, MyCremeeActivity.class);
		startActivity(intent);
	}

	@Override
	public void goto_CallUsMethod()
	{
		try
		{
			String ContactNum = "9552378888";
			String uri = "tel:" +"+91"+ ContactNum;
			Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
			startActivity(dialIntent);
		}
		catch (Exception e)
		{
			Toast.makeText(mContext, "Your call has failed...",Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	@Override
	public void goto_NotificationsMethod() {

		Intent intent = new Intent(mContext, NotificationActivity.class);
		startActivity(intent);

	}

	@Override
	public void goto_ShareMethod() {

		/*Intent intent= new Intent(mContext,ShareActivity.class);
		startActivity(intent);*/

		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("text/plain");
		share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

		// Add data to the intent, the receiving app will decide
		// what to do with it.
		share.putExtra(Intent.EXTRA_SUBJECT, "Cremeville");
		share.putExtra(Intent.EXTRA_TEXT, "Click on the link to download Cremeville. http://cremeville.com/mobile-urls/share-app.html");

		Intent intent=Intent.createChooser(share, "Share link!");

		//startActivity(Intent.createChooser(share, "Share link!"));

		startActivityForResult(intent, 1001);
	}

	@Override
	public void goto_AboutUsMethod() {

		Intent intent = new Intent(mContext, AboutUsActivity.class);
		startActivity(intent);
	}

	@Override
	public void goto_loginMethod()
	{
		Intent intent= new Intent(mContext,LoginActivity.class);
		intent.putExtra("key","SidebarLogin");
		startActivity(intent);
	}

	@Override
	public void goto_logoutMethod() {

		preferenceUtils.logoutUser();
		Intent intent= new Intent(mContext,Dashboard.class);
		startActivity(intent);

	}


	@Override
	protected void onResume() {
		MyApplication.getInstance().trackScreenView("Dashboard");
		LogUtils.error(Dashboard.class.getSimpleName(), "onResume");
		CartCount=CartDatabase.getCartlistCount();
		if(CartCount>0)
		{
			txt_cartCount.setVisibility(View.VISIBLE);
			txt_cartCount.setText("" + CartCount);
		}
		else
		{
			txt_cartCount.setVisibility(View.GONE);
		}

		UserAddress=preferenceUtils.getUserAddress();
		AreaName=UserAddress.get(AppConstants.KEY_SUB_AREA_NAME);
		CityName=UserAddress.get(AppConstants.KEY_CITY_NAME);

		UserLocation=preferenceUtils.getUserLocation();
		Latitude=UserLocation.get(AppConstants.KEY_LATITUDE);
		Longitude=UserLocation.get(AppConstants.KEY_LONGITUDE);


		txt_actionbar.setText(AreaName+","+CityName);

		if(preferenceUtils.isLoggedIn())
		{
			Userdata=preferenceUtils.getUserDetails();
			// CustomerID,CustomerName,CustomerContact,AccessToken
			CustomerID=Userdata.get(AppConstants.KEY_CUSTOMER_ID);
			Password=Userdata.get(AppConstants.KEY_CUSTOMER_PASSWORD);
			CustomerName=Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
			CustomerContact=Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
			AccessToken=Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

			loyaltyData=preferenceUtils.getLoyaltyPoints();
			TotalLoyaltyPoints=loyaltyData.get(AppConstants.KEY_TOTAL_POINTS);
			totalLoyaltyAmount=loyaltyData.get(AppConstants.KEY_LOYALTY_AMOUNT);

			if(preferenceUtils.getLoginState())
			{
				getLogin(Password, CustomerContact);
				LogUtils.error(Dashboard.class.getSimpleName(), "Login Executed");
			}
		}

		getNearestStoreInfo(Latitude,Longitude);

		super.onResume();

	}

	private void getLogin(String UserOTP, String UserFirstMobile)
	{
		showLoaderNew();

		if(new CommonBL(mContext, Dashboard.this).getLogin(UserOTP, UserFirstMobile))
		{

		}
		else
		{
			hideloader();
			showAlertDialog("NO INTERNET","OK");
		}
	}

	private void loadCategoryData()
	{
		// URL : http://104.196.99.177:4848/api/Categories
		//preferenceUtils.saveInt("cityid", CityId);
		showLoaderNew();
		if(new CommonBL(mContext, Dashboard.this).getAllCategories())
		{
//			showLoader("Loading all brands...");

		}
		else
		{
			hideloader();
			showAlertDialog("NO INTERNET","OK");
		}
	}

	private void getNearestStoreInfo(String latitude, String longitude)
	{
		// URL : http://104.196.99.177:4848/api/Stores/getNearestStoreInformation?lat=17.4936856&lng=78.3401293
		//lat=17.494731299999998&lng=78.3330046
		String queryParam="lat="+latitude+"&lng="+longitude;
		showLoaderNew();
		if(new CommonBL(mContext, Dashboard.this).getNearestStoreInfo(queryParam))
		{
//			showLoader("Loading all brands...");

		}
		else
		{
			hideloader();
			showAlertDialog("NO INTERNET","OK");
		}
	}

	private void initializeControls() {
		tvDashBoardNoData=(TextView) ll_Dashboard.findViewById(R.id.tvDashBoardNoData);
		grd_category=(GridView) ll_Dashboard.findViewById(R.id.grd_category);


	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);
	}


	@Override
	public void dataRetreived(Response data) {

		hideloader();
		if (data != null && data.method == ServiceMethods.WS_CATEGORIES)
		{
			if(!data.isError)
			{
				arrCategories = (ArrayList<CategoryDO>) data.data;
				if(arrCategories!=null&&arrCategories.size()>0)
				{
					tvDashBoardNoData.setVisibility(View.GONE);
					grd_category.setVisibility(View.VISIBLE);

					categoryAdapter=new CategoryAdapter(mContext, arrCategories);
					grd_category.setAdapter(categoryAdapter);
				}
				else
				{
					tvDashBoardNoData.setVisibility(View.VISIBLE);
					grd_category.setVisibility(View.GONE);
				}
			}
		}
		else if (data != null && data.method == ServiceMethods.WS_GET_LOGGED_IN)
		{
			if(!data.isError)
			{
				arrLogin = (ArrayList<LoginDO>) data.data;
				if(arrLogin!=null&&arrLogin.size()>0)
				{
					for(int i=0;i<arrLogin.size();i++)
					{
						Message=arrLogin.get(i).Status;
					}

					if (Message.equalsIgnoreCase("success"))
					{
						for(int i=0;i<arrLogin.size();i++)
						{
							//accesstoken,customerid,password,customerphone,customername,totalloyaltypoint,loyaltyamount
							Message=arrLogin.get(i).Status;
							customerid=arrLogin.get(i).CustomerId;
							password=arrLogin.get(i).Password;
							customerphone=arrLogin.get(i).CustomerPhone;
							customername=arrLogin.get(i).CustomerName;
							customer_email=arrLogin.get(i).CustomerEmail;
							accesstoken=arrLogin.get(i).AccessToken;
							totalloyaltypoint=arrLogin.get(i).TotalLoyaltyPoint;
							loyaltyamount=arrLogin.get(i).LoyaltyAmount;

							LogUtils.error(Dashboard.class.getSimpleName(),"customer_email"+customer_email);

						}
						preferenceUtils.createLoginSession(customerid, password, customerphone, customername,customer_email, accesstoken);
						preferenceUtils.saveLoyaltyPoints(totalloyaltypoint, loyaltyamount);
						preferenceUtils.saveLoginState(false);
					}
					else if (Message.equalsIgnoreCase("Login Failed"))
					{
						Intent intent= new Intent(mContext,LoginActivity.class);
						intent.putExtra("key","DashboardLogin");
						startActivity(intent);
					}
				}
			}
		}

		else if (data != null && data.method == ServiceMethods.WS_GET_NEAREST_STORE)
		{
			if(!data.isError)
			{
				arrNearestStoreDO = (ArrayList<NearestStoreDO>) data.data;
				if(arrNearestStoreDO!=null&&arrNearestStoreDO.size()>0)
				{
					Message=arrNearestStoreDO.get(0).Status;
					if(Message.equalsIgnoreCase("success"))
					{
						String StoreId=arrNearestStoreDO.get(0).StoreId;
						preferenceUtils.saveStoreId(StoreId);
						LogUtils.error(Dashboard.class.getSimpleName(), "StoreId=" + StoreId);
					}
					else
					{
						preferenceUtils.saveStoreId("NA");
						showAlertDialog("Sorry we do not deliver in your area, You can still browse our menu","OK");
					}
				}
				else
				{

				}
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LogUtils.error(Dashboard.class.getSimpleName(), " Dashboard  :  OnActivity Result called");
		if(requestCode==1000)
		{
			super.onActivityResult(requestCode, resultCode, data);
		}
		else
		{
			Intent intent=new Intent(mContext,Dashboard.class);
			startActivity(intent);
		}
	}
}
