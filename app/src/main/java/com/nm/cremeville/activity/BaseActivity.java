package com.nm.cremeville.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.NavDrawerListAdapter;
import com.nm.cremeville.objects.NavDrawerItem;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class BaseActivity extends AppCompatActivity {
	Context mContext;
	public LinearLayout llContent;
    public LayoutInflater inflater;
    public Toolbar mtoolbar;
	public Dialog dialog;
	ListView left_drawer;
	private AnimationDrawable animationDrawable;
	public AlertDialog alertDialog;
	public AlertDialog.Builder alertBuilder;
	public RelativeLayout rl_baseHeader,rl_logo;
	public TextView txt_actionbar,txt_usrname,txt_usrmobile,txt_cartCount;
	public ImageView img_actionbar,img_actionbar2,img_header;
    public String[] nav_item_titlenames;
    public ListView mdrawerlistview;
    public DrawerLayout mdrawerlayout;
    public ActionBarDrawerToggle mdrawertoogle;
    public View header_view,footer_view;
    public ArrayList<NavDrawerItem> navDrawerItems;
    public NavDrawerListAdapter adapter;
	PreferenceUtils preferenceUtils;
	String UserName,UserContact;
	FrameLayout fl_cart,fl_baseHeader;
	public HashMap<String, String> Userdata;
	String CustomerID, CustomerName, CustomerContact, AccessToken;

	public HashMap<String,String> loyaltyData;
	String TotalLoyaltyPoints,totalLoyaltyAmount,TempLoyaltyPoints;

	FontType fonttype;
	CleverTapAPI cleverTap;

    @Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.base_activity);
		mContext=BaseActivity.this;

		inflater = this.getLayoutInflater();

		preferenceUtils=new PreferenceUtils(this);
		baseInitializeControls();
		initialize();

		try {
			cleverTap = CleverTapAPI.getInstance(getApplicationContext());
		} catch (CleverTapMetaDataNotFoundException e) {
			// handle appropriately
		} catch (CleverTapPermissionsNotSatisfied e) {
			// handle appropriately
		}

		generate_iconNstring_drawer();
		int width = (int) (getResources().getDisplayMetrics().widthPixels/1.4);
		DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) left_drawer.getLayoutParams();
		params.width = width;
		left_drawer.setLayoutParams(params);

		adapter = new NavDrawerListAdapter(mContext,navDrawerItems);

		mdrawerlistview.addHeaderView(header_view, null, false);

		mdrawerlistview.setAdapter(adapter);

		mdrawerlistview.addFooterView(footer_view, null, false);

	        if(mtoolbar!=null)
	        {
	            setSupportActionBar(mtoolbar);
	        }

	        try
	        {
	            getSupportActionBar().setHomeButtonEnabled(true);
	        }
	        catch (Exception ex)
	        {
	            ex.printStackTrace();
	        }


	        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	        getSupportActionBar().setDisplayShowTitleEnabled(false);

	        initdrawer();

	        mdrawertoogle.syncState();

	        mdrawerlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

	                NavDrawerItem drawerobj= (NavDrawerItem) parent.getItemAtPosition(position);

	                slidelist_item_click(drawerobj.title);

	            }
	        });

	    }


	@Override
	protected void onResume() {
		super.onResume();
		if(preferenceUtils.isLoggedIn())
		{
			Userdata = preferenceUtils.getUserDetails();
			// CustomerID,CustomerName,CustomerContact,AccessToken
			CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
			CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
			CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
			AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

			loyaltyData=preferenceUtils.getLoyaltyPoints();
			TotalLoyaltyPoints=loyaltyData.get(AppConstants.KEY_TOTAL_POINTS);
			totalLoyaltyAmount=loyaltyData.get(AppConstants.KEY_LOYALTY_AMOUNT);

			TempLoyaltyPoints=TotalLoyaltyPoints;


			LogUtils.error(BaseActivity.class.getSimpleName(), "CustomerID=" + CustomerID);
			LogUtils.error(BaseActivity.class.getSimpleName(), "CustomerName=" + CustomerName);
			LogUtils.error(BaseActivity.class.getSimpleName(), "CustomerContact=" + CustomerContact);
			LogUtils.error(BaseActivity.class.getSimpleName(), "AccessToken=" + AccessToken);

			LogUtils.error(BaseActivity.class.getSimpleName(), "TotalLoyaltyPoints in On Resume=" + TotalLoyaltyPoints);
			LogUtils.error(BaseActivity.class.getSimpleName(), "totalLoyaltyAmount in On Resume=" + totalLoyaltyAmount);

		}

		generate_iconNstring_drawer();
	}

	public  void slidelist_item_click(String title)
	    {
	        switch (title)
	        {
	            case "My Profile":
					goto_myProfileMethod();
	                mdrawerlayout.closeDrawers();
	                break;
	            case "My Orders":
					goto_myOrdersMethod();
	                mdrawerlayout.closeDrawers();
	                break;
				case "Favourites":
					goto_myFavouritesMethod();
					mdrawerlayout.closeDrawers();
					break;
	            case "Coupons":
					goto_CouponsMethod();
	                mdrawerlayout.closeDrawers();
	                break;

				case "My Cremee":
					goto_MyCremeeMethod();
					mdrawerlayout.closeDrawers();
					break;
				case "Notifications":
					goto_NotificationsMethod();
					mdrawerlayout.closeDrawers();
					break;
	            case "Call Us":
					goto_CallUsMethod();
	                mdrawerlayout.closeDrawers();
	                break;
	            case "Share":
					goto_ShareMethod();
	                mdrawerlayout.closeDrawers();
	                break;
				case "About us":
					goto_AboutUsMethod();
					mdrawerlayout.closeDrawers();
					break;
	            case "Login":
					goto_loginMethod();
					adapter.notifyDataSetChanged();
	                mdrawerlayout.closeDrawers();
	                break;
	            case "Logout":
					goto_logoutMethod();
					adapter.notifyDataSetChanged();
	                mdrawerlayout.closeDrawers();
	        }
	    }
	    
	    public abstract void initialize();

		public abstract void goto_myProfileMethod();

	    public abstract void goto_myOrdersMethod();

	    public abstract void goto_myFavouritesMethod();

	    public abstract void goto_CouponsMethod();

	    public abstract void goto_MyCremeeMethod();

	    public abstract void goto_CallUsMethod();

	    public abstract void goto_NotificationsMethod();

	    public abstract void goto_ShareMethod();

		public abstract void goto_AboutUsMethod();

	    public abstract void goto_loginMethod();

	    public abstract void goto_logoutMethod();

	    private void initdrawer() {
	        mdrawertoogle=new ActionBarDrawerToggle(this,mdrawerlayout,mtoolbar,R.string.app_name,R.string.app_name)
	        {
	            @Override
	            public void onDrawerOpened(View drawerView)
				{
	                super.onDrawerOpened(drawerView);
	                try
	                {
						rl_logo.setVisibility(View.VISIBLE);
						rl_baseHeader.setVisibility(View.INVISIBLE);
	                }
	                catch (Exception ex)
	                {
	                    ex.printStackTrace();
	                }
	            }

	            @Override
	            public void onDrawerClosed(View drawerView) {
	                super.onDrawerClosed(drawerView);

	                try
	                {
						rl_baseHeader.setVisibility(View.VISIBLE);
						rl_logo.setVisibility(View.INVISIBLE);
	                }
	                catch (Exception ex)
	                {
	                    ex.printStackTrace();
	                }
	            }

	            @Override
	            public void onDrawerStateChanged(int newState) {
	                super.onDrawerStateChanged(newState);
	            }

	            @Override
	            public void onDrawerSlide(View drawerView, float slideOffset) {
	                super.onDrawerSlide(drawerView, slideOffset);
	            }
	        };
	        mdrawerlayout.setDrawerListener(mdrawertoogle);
	    }

	    private void baseInitializeControls() {
			left_drawer= (ListView) findViewById(R.id.left_drawer);
	        llContent= (LinearLayout) findViewById(R.id.llContent);
	        header_view=getLayoutInflater().inflate(R.layout.header_navi, null);
	        footer_view=getLayoutInflater().inflate(R.layout.footer_navi,null);
			txt_actionbar=(TextView)findViewById(R.id.txt_actionbar);
			img_actionbar=(ImageView)findViewById(R.id.img_actionbar);
			img_actionbar2=(ImageView)findViewById(R.id.img_actionbar2);
			txt_cartCount=(TextView)findViewById(R.id.txt_cartCount);
			img_header   =(ImageView)findViewById(R.id.img_header);
			txt_usrname=(TextView)header_view.findViewById(R.id.txt_usrname);
			txt_usrmobile =(TextView)header_view.findViewById(R.id.txt_usrmobile);
	        mdrawerlistview= (ListView) findViewById(R.id.left_drawer);
	        mtoolbar= (Toolbar) findViewById(R.id.toolbar);
	        mdrawerlayout= (DrawerLayout) findViewById(R.id.drawerLayout);
			rl_baseHeader= (RelativeLayout) findViewById(R.id.rl_baseHeader);
			rl_logo= (RelativeLayout) findViewById(R.id.rl_logo);
		    fl_cart= (FrameLayout) findViewById(R.id.fl_cart);
			fl_baseHeader= (FrameLayout) findViewById(R.id.fl_baseHeader);
		}

	    public void generate_iconNstring_drawer()
		{
			changeLoginList();
	    }

	private void changeLoginList() {

		if(preferenceUtils.isLoggedIn())
		{
			nav_item_titlenames = getResources().getStringArray(R.array.nav_drawer_items_logout);

			Userdata=preferenceUtils.getUserDetails();
			UserName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
			UserContact=Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);

			loyaltyData=preferenceUtils.getLoyaltyPoints();
			TotalLoyaltyPoints = loyaltyData.get(AppConstants.KEY_TOTAL_POINTS);
			totalLoyaltyAmount=loyaltyData.get(AppConstants.KEY_LOYALTY_AMOUNT);

			TempLoyaltyPoints=TotalLoyaltyPoints;

		/*	Log.e(BaseActivity.class.getSimpleName(), "TotalLoyaltyPoints in change=" + TotalLoyaltyPoints);
			Log.e(BaseActivity.class.getSimpleName(), "totalLoyaltyAmount in change=" + totalLoyaltyAmount);*/

			txt_usrname.setText(UserName);
			txt_usrmobile.setText(UserContact);
		}
		else
		{
			nav_item_titlenames = getResources().getStringArray(R.array.nav_drawer_items);

			txt_usrname.setText("");
			txt_usrmobile.setText("");
		}

		TypedArray navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// My Profile
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[0], navMenuIcons.getResourceId(0, -1)));
		// My Orders
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[1], navMenuIcons.getResourceId(1, -1)));
		// Favourites
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[2], navMenuIcons.getResourceId(2, -1),false,"9"));
		// Coupons
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[3], navMenuIcons.getResourceId(3, -1)));

		if(preferenceUtils.isLoggedIn())
		{
			// My Cremee
			navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[4], navMenuIcons.getResourceId(4, -1),true,TempLoyaltyPoints));
		}
		else
		{
			// My Cremee
			navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[4], navMenuIcons.getResourceId(4, -1),false,TempLoyaltyPoints));
		}
		// Notifications
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[5], navMenuIcons.getResourceId(5, -1)));
		// Call Us
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[6], navMenuIcons.getResourceId(6, -1)));
		// Share
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[7], navMenuIcons.getResourceId(7, -1)));
		// About us
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[8], navMenuIcons.getResourceId(8, -1)));
		// Login
		navDrawerItems.add(new NavDrawerItem(nav_item_titlenames[9], navMenuIcons.getResourceId(9, -1)));

		navMenuIcons.recycle();
	}


	@SuppressLint("NewApi") @Override
	    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState){
			super.onPostCreate(savedInstanceState, persistentState);

	        mdrawertoogle.syncState();
	    }

	    @Override
	    public void onConfigurationChanged(Configuration newConfig) {
	        super.onConfigurationChanged(newConfig);

	        mdrawertoogle.syncState();
	    }

	    @Override
	    public void onBackPressed() {
	        super.onBackPressed();



	    }

	public void hideKeyBoard(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public void showAlertDialog(String strMessage, String firstBtnName)
	{
		runOnUiThread(new RunshowCustomDialogs(strMessage, firstBtnName));
	}


	class RunshowCustomDialogs implements Runnable
	{
		private String strMessage;// Message to be shown in dialog
		private String firstBtnName;
		private int titleGravity;
		private boolean isShowNestedDialog;
		private String dialogFrom;

		public RunshowCustomDialogs( String strMessage, String firstBtnName)
		{
			this.strMessage 	= strMessage;
			this.firstBtnName 	= firstBtnName;
		}

		@Override
		public void run()
		{
			closeAlertDialog();
			alertBuilder = new AlertDialog.Builder(mContext);
			alertBuilder.setCancelable(true);

			final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.notification_dailog, null);

			ViewGroup root = (ViewGroup)linearLayout.findViewById(R.id.ll_notifydialog);
			fonttype = new FontType(mContext, root);

			TextView dialogtvTitle = (TextView)linearLayout.findViewById(R.id.tvTitle);
			TextView btnYes = (TextView)linearLayout.findViewById(R.id.btnYes);




			if(titleGravity!=0)
			{
				// Only in the case of Crash Report Dialog, i am customizing it with custom padding.
				dialogtvTitle.setGravity(titleGravity);
				dialogtvTitle.setPadding(35, 35, 0, 35);

			}
			dialogtvTitle.setText(strMessage);
			btnYes.setText(firstBtnName);
			btnYes.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					alertDialog.cancel();

				}
			});

			try
			{
				alertDialog = alertBuilder.create();
				alertDialog.setView(linearLayout,0,0,0,0);
				alertDialog.setInverseBackgroundForced(true);
				alertDialog.show();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	public void closeAlertDialog()
	{
		if (alertDialog != null && alertDialog.isShowing())
			alertDialog.dismiss();
	}
	public void onButtonNoClick(String from)
	{}
	public void onButtonYesClick(String dialogFrom)
	{
	}

	public void hideloader() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try
				{
					if (dialog != null && dialog.isShowing())
						dialog.dismiss();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void showLoaderNew() {
		runOnUiThread(new Runloader(getResources().getString(R.string.loading)));
	}

	class Runloader implements Runnable {
		private String strrMsg;

		public Runloader(String strMsg) {
			this.strrMsg = strMsg;
		}

		@Override
		public void run() {
			try {
				if (dialog == null)
				{
					dialog = new Dialog(mContext,R.style.Theme_Dialog_Translucent);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawable(
							new ColorDrawable(android.graphics.Color.TRANSPARENT));
				}
				dialog.setContentView(R.layout.loading);
				dialog.setCancelable(false);

				if (dialog != null && dialog.isShowing())
				{
					dialog.dismiss();
					dialog=null;
				}
				dialog.show();

				ImageView imgeView = (ImageView) dialog
						.findViewById(R.id.imgeView);
				TextView tvLoading = (TextView) dialog
						.findViewById(R.id.tvLoading);
				if (!strrMsg.equalsIgnoreCase(""))
					tvLoading.setText(strrMsg);

				imgeView.setBackgroundResource(R.anim.frame);

				animationDrawable = (AnimationDrawable) imgeView
						.getBackground();
				imgeView.post(new Runnable() {
					@Override
					public void run() {
						if (animationDrawable != null)
							animationDrawable.start();
					}
				});
			} catch (Exception e)
			{

			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Log.e(BaseActivity.class.getSimpleName(),"OnActivity Rseult called"+"Request Code=="+requestCode+"==result code=="+resultCode);
	}


}
