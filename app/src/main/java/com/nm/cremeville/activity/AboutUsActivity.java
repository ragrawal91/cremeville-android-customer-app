package com.nm.cremeville.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.utilities.FontType;



public class AboutUsActivity extends AppCompatActivity
{

    WebView wbv_appdata;
    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount,tv_faq,tv_privacy_policy,tv_termscondition;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    FontType fonttype;
    FrameLayout fl_cart,fl_Filter;

    private String ABOUTUSURL="http://139.162.61.196:4848/mobile-urls/about-us-m.html";
    private String PRIVACYPOLICYURL="http://139.162.61.196:4848/mobile-urls/privacy-policy-m.html";
    private String FAQURL="http://139.162.61.196:4848/mobile-urls/faq-m.html";
    private String TERMSCONDITIONURL="http://139.162.61.196:4848/mobile-urls/terms-of-use-m.html";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        mContext=AboutUsActivity.this;
        initview();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_aboutus);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);


        txt_normal_actionbar.setText("About us");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,TermsCondition.class);
                intent.putExtra("URL",FAQURL);
                intent.putExtra("header","FAQ");
                startActivity(intent);
            }
        });
        tv_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,TermsCondition.class);
                intent.putExtra("URL",PRIVACYPOLICYURL);
                intent.putExtra("header","Privacy Policy");
                startActivity(intent);
            }
        });
        tv_termscondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,TermsCondition.class);
                intent.putExtra("URL",TERMSCONDITIONURL);
                intent.putExtra("header","Terms & Condition");
                startActivity(intent);
            }
        });
    }

    private void showapptermsandprivacypolicymethod() {

        WebSettings webSettings = wbv_appdata.getSettings();
        wbv_appdata.getSettings().setJavaScriptEnabled(true);
        wbv_appdata.getSettings().setLoadWithOverviewMode(true);
        wbv_appdata.getSettings().setUseWideViewPort(true);
        wbv_appdata.getSettings().setTextZoom(300);
        wbv_appdata.setWebViewClient(new MyWebViewClient());

        wbv_appdata.loadUrl(ABOUTUSURL);

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("About Us Screen");
        showapptermsandprivacypolicymethod();
    }


    private void initview() {

        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        wbv_appdata= (WebView) findViewById(R.id.wbv_appdata);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        tv_faq=(TextView)findViewById(R.id.tv_faq);
        tv_privacy_policy=(TextView)findViewById(R.id.tv_privacy_policy);
        tv_termscondition=(TextView)findViewById(R.id.tv_termscondition);

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            super.onPageFinished(view, url);

        }
    }
}
