package com.nm.cremeville.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ShareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "FITROS");
        share.putExtra(Intent.EXTRA_SUBJECT, "FITROS");
        share.putExtra(Intent.EXTRA_TEXT, "http://www.Fitros.com");

        Intent intent=Intent.createChooser(share, "Share link!");

        //startActivity(Intent.createChooser(share, "Share link!"));

        startActivityForResult(intent,1000);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent=new Intent(ShareActivity.this,Dashboard.class);
        startActivity(intent);
        finish();
    }
}
