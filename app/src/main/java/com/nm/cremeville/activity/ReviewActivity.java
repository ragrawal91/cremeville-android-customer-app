package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nm.cremeville.R;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.objects.PostReviewDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;
import java.util.HashMap;

public class ReviewActivity extends BaseNew implements DataListener {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_itemName,txt_desc,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_favourite;
    FrameLayout fl_cart,fl_Filter;
    PreferenceUtils preferenceUtils;
    String ItemId,ItemName,queryParams;
    private ArrayList<ItemsDO> arrItemsProfile;
    LinearLayout ll_itemImage,ll_brandImage;
    ImageLoaderConfiguration imgLoaderConf;
    DisplayImageOptions dispImage;
    ImageLoader imgLoader= ImageLoader.getInstance();
    RatingBar rat_itemrating;
    EditText edt_comment;
    TextView txt_submit;
    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;
    ArrayList<PostReviewDO>  arrPostReviewDO;
    String Message;
    FontType fonttype;




    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review);
        mContext=ReviewActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();
        imgLoaderConf = new ImageLoaderConfiguration.Builder(mContext).build();
        imgLoader.init(imgLoaderConf);
        dispImage=new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.noimage)
                .showImageOnLoading(R.mipmap.noimage)
                .showImageForEmptyUri(R.mipmap.noimage)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .delayBeforeLoading(100)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
        ViewGroup root = (ViewGroup)findViewById(R.id.ll_review);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ItemId = getIntent().getExtras().getString("ItemId");
        ItemName= getIntent().getExtras().getString("ItemName");

        img_back.setImageResource(R.mipmap.back_arrow);

        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("Rate This");
        queryParams=ItemId;

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });

        txt_submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String Comment=edt_comment.getText().toString();
                float Rating=rat_itemrating.getRating();

                if(preferenceUtils.isLoggedIn())
                {
                    if(Rating==0)
                    {
                        Toast.makeText(mContext,"Please provide a proper rating",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        postReview(Rating,ItemId,CustomerID,Comment,"Item",CustomerName);
                    }

                }
                else
                {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("key", "ReviewActivity");
                    startActivity(intent);
                }


            }
        });
    }

    private void postReview(float rating, String itemId, String customerID, String comment, String item,String CustomerName)
    {
        showLoaderNew();
        if(new CommonBL(mContext, ReviewActivity.this).postReview(rating,itemId,customerID,comment,item,CustomerName))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Product Review Screen");
        getItemsProfile(queryParams);
        Userdata=preferenceUtils.getUserDetails();
        // CustomerID,CustomerName,CustomerContact,AccessToken
        CustomerID=Userdata.get(AppConstants.KEY_CUSTOMER_ID);
        CustomerName=Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
        CustomerContact=Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
        AccessToken=Userdata.get(AppConstants.KEY_ACCESS_TOKEN);
    }

    private void getItemsProfile(String queryParams)
    {
        showLoaderNew();
        if(new CommonBL(mContext, ReviewActivity.this).getItemsProfile(queryParams)) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        ll_itemImage=(LinearLayout)findViewById(R.id.ll_itemImage);
        ll_brandImage=(LinearLayout)findViewById(R.id.ll_brandImage);
        txt_itemName=(TextView)findViewById(R.id.txt_itemName);
        txt_desc=(TextView)findViewById(R.id.txt_desc);

        rat_itemrating=(RatingBar)findViewById(R.id.rat_itemrating);
        edt_comment=(EditText)findViewById(R.id.edt_comment);
        txt_submit=(TextView)findViewById(R.id.txt_submit);
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_ITEM_PROFILE)
        {
            if(!data.isError)
            {
                arrItemsProfile = (ArrayList<ItemsDO>) data.data;
                if(arrItemsProfile!=null&&arrItemsProfile.size()>0)
                {
                    txt_desc.setText(arrItemsProfile.get(0).ItemDesc);
                    txt_itemName.setText(arrItemsProfile.get(0).ItemName);

                    imgLoader.getInstance().displayImage(arrItemsProfile.get(0).ItemImageUrl_100, new BgViewAware(ll_itemImage), dispImage);

                    imgLoader.getInstance().displayImage(arrItemsProfile.get(0).ItemBrandImgUrl_72, new BgViewAware(ll_brandImage), dispImage);


                }

            }
        }

        else if (data != null && data.method == ServiceMethods.WS_POST_ITEM_REVIEW)
        {
            if(!data.isError)
            {
                arrPostReviewDO = (ArrayList<PostReviewDO>) data.data;
                if(arrPostReviewDO!=null&&arrPostReviewDO.size()>0)
                {
                    for(int i=0;i<arrPostReviewDO.size();i++)
                    {
                        Message=arrPostReviewDO.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {
                        finish();
                        Toast.makeText(mContext,"Thank you for your feedback",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

    }
}
