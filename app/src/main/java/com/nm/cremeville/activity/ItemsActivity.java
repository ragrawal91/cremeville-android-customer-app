package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.ExpandableAdapter;
import com.nm.cremeville.adapter.ItemsGridAdapter;
import com.nm.cremeville.adapter.ShowCouponAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.listener.FilterCheckboxListener;
import com.nm.cremeville.objects.AddFavouriteDO;
import com.nm.cremeville.objects.FilterMetadataDO;
import com.nm.cremeville.objects.FlavourDO;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.objects.ShowCouponDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemsActivity extends BaseNew implements DataListener, CartListListener, FilterCheckboxListener {

    Context mContext;
    private GridView grd_items;
    private TextView tvItemsNoData;
    public Toolbar mtoolbar;
    ExpandableListView exp_flavour;
    public TextView txt_normal_actionbar,txt_filter_actionbar,txt_cartCount,txt_resetFilter;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite,img_backfilter;
    PreferenceUtils preferenceUtils;
    String CatgId,CatgName,queryParams,queryParamsFilterApply;
    private ArrayList<ItemsDO> arrItems;
    private ArrayList<ItemsDO> arrItemsFilter;
    private ArrayList<FilterMetadataDO> arrFilterMetadDO;
    private ArrayList<AddFavouriteDO> arrAddFavouriteDO;
    FrameLayout fl_cart,fl_Filter;
    LinearLayout ll_filter;
    ItemsGridAdapter itemsGridAdapter;
    ArrayList<ItemsCartsDO> LocalCartList;
    public ArrayList<FlavourDO> brandslist;
    public ArrayList<FlavourDO> flavourlist;
    private Animation animUp;
    private Animation animDown;
    int CartCount;
    FrameLayout ll_items;
    LinearLayout ll_NoItems;
    ExpandableAdapter expandableAdapter;
    ArrayList<FilterMetadataDO> expandableDomains;
    RangeBar rangebar;
    TextView leftIndexValue,rightIndexValue;
    FilterMetadataDO domain1, domain2;
    int priceStart=0,priceEnd=1000;
    ArrayList<FlavourDO> Filterlist;
    ArrayList<FlavourDO> FlavourFilterlist;
    ArrayList<FlavourDO> BrandFilterlist;
    ScrollView scrollView1;
    Button btn_apply,btn_reset;
    String Message;

    private ArrayList<ShowCouponDO> arrShowCouponDO;
    ShowCouponAdapter showCouponAdapter;
    private ListView lst_coupons;
    LinearLayout ll_NoCoupons;
    TextView txt_addfavourite;



    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,AccessToken;
    FontType fonttype;
    FlavourDO flavourDO;
    boolean filterTab;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        CatgId = getIntent().getExtras().getString("CatgId");
        CatgName= getIntent().getExtras().getString("CatgName");


        if(CatgName.equalsIgnoreCase("DEALS & OFFERS"))
        {
            setContentView(R.layout.activity_coupons);
            mContext=ItemsActivity.this;
            preferenceUtils=new PreferenceUtils(mContext);
            baseInitializeControls();

            if(mtoolbar!=null)
            {
                setSupportActionBar(mtoolbar);

            }
            CartDatabase.init(this);

            ViewGroup root = (ViewGroup)findViewById(R.id.ll_coupons);
            fonttype = new FontType(mContext, root);

            animUp = AnimationUtils.loadAnimation(this, R.anim.anim_up);
            animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        else
        {
            setContentView(R.layout.activity_items);
            mContext=ItemsActivity.this;
            preferenceUtils=new PreferenceUtils(mContext);
            baseInitializeControls();

            if(mtoolbar!=null)
            {
                setSupportActionBar(mtoolbar);

            }
            CartDatabase.init(this);

            ViewGroup root = (ViewGroup)findViewById(R.id.ll_itemss);
            fonttype = new FontType(mContext, root);

            animUp = AnimationUtils.loadAnimation(this, R.anim.anim_up);
            animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }




        if(CatgName.equalsIgnoreCase("DEALS & OFFERS"))
        {
            filterTab=false;
            img_normal_actionbar.setVisibility(View.VISIBLE);
            img_normal_actionbar.setImageResource(R.mipmap.filter);
            img_back.setImageResource(R.mipmap.back_arrow);
            img_favourite.setVisibility(View.GONE);
            fl_Filter.setVisibility(View.GONE);
            fl_cart.setVisibility(View.GONE);

            txt_normal_actionbar.setText("Coupons");

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

        }
        else
        {
            LocalCartList=new ArrayList<>();
            Filterlist=new ArrayList<>();
            FlavourFilterlist=new ArrayList<>();
            BrandFilterlist=new ArrayList<>();

            img_normal_actionbar.setVisibility(View.VISIBLE);
            img_normal_actionbar.setImageResource(R.mipmap.filter);
            img_back.setImageResource(R.mipmap.back_arrow);

            txt_normal_actionbar.setText(CatgName);

           // rangebar.setTickCount(15);
            rangebar.setBarColor(Color.parseColor("#f05b72"));
            rangebar.setBarWeight(5);
            rangebar.setConnectingLineColor(Color.parseColor("#f05b72"));
            rangebar.setConnectingLineWeight(10);
            rangebar.setTickColor(Color.parseColor("#f05b72"));
            rangebar.setTickColor(Color.parseColor("#f05b72"));
            rangebar.setSelectorColor(Color.parseColor("#c03045"));
            rangebar.setTickStart(0 / 10.0f);
            rangebar.setTickEnd(200 / 10.0f);
            rangebar.setPinRadius(0.0f);

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            txt_filter_actionbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    filterTab=false;
                    img_resetFilter.setVisibility(View.INVISIBLE);
                    img_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_filter_actionbar.setVisibility(View.GONE);
                    txt_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_normal_actionbar.setText(CatgName);
                    ll_filter.startAnimation(animDown);
                    ll_filter.setVisibility(View.GONE);
                    ll_items.setVisibility(View.VISIBLE);
                    fl_cart.setVisibility(View.VISIBLE);
                    img_favourite.setVisibility(View.VISIBLE);
                    img_back.setVisibility(View.VISIBLE);
                    img_backfilter.setVisibility(View.GONE);

                }
            });

            img_normal_actionbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    filterTab=true;
                    img_resetFilter.setVisibility(View.VISIBLE);
                    img_normal_actionbar.setVisibility(View.INVISIBLE);
                    ll_filter.startAnimation(animUp);
                    ll_filter.setVisibility(View.VISIBLE);
                    ll_items.setVisibility(View.GONE);
                    txt_normal_actionbar.setVisibility(View.GONE);
                    txt_filter_actionbar.setVisibility(View.VISIBLE);
                    txt_filter_actionbar.setText("Filter");
                    fl_cart.setVisibility(View.GONE);
                    img_favourite.setVisibility(View.GONE);
                    img_back.setVisibility(View.GONE);
                    img_backfilter.setVisibility(View.VISIBLE);

                }
            });

            img_resetFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    filterTab=false;
                    img_resetFilter.setVisibility(View.INVISIBLE);
                    img_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_filter_actionbar.setVisibility(View.GONE);
                    txt_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_normal_actionbar.setText(CatgName);
                    ll_filter.startAnimation(animDown);
                    ll_filter.setVisibility(View.GONE);
                    ll_items.setVisibility(View.VISIBLE);
                    fl_cart.setVisibility(View.VISIBLE);
                    img_favourite.setVisibility(View.VISIBLE);
                    img_back.setVisibility(View.VISIBLE);
                    img_backfilter.setVisibility(View.GONE);
                }
            });

            img_backfilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    filterTab=false;
                    img_resetFilter.setVisibility(View.INVISIBLE);
                    img_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_filter_actionbar.setVisibility(View.GONE);
                    txt_normal_actionbar.setVisibility(View.VISIBLE);
                    txt_normal_actionbar.setText(CatgName);
                    ll_filter.startAnimation(animDown);
                    ll_filter.setVisibility(View.GONE);
                    ll_items.setVisibility(View.VISIBLE);
                    fl_cart.setVisibility(View.VISIBLE);
                    img_favourite.setVisibility(View.VISIBLE);
                    img_back.setVisibility(View.VISIBLE);
                    img_backfilter.setVisibility(View.GONE);
                }
            });


            fl_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, CartActivity.class);
                    startActivity(intent);
                }
            });


            rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex,
                                                  String leftPinValue, String rightPinValue) {

                    leftIndexValue.setText("From : " + leftPinIndex * 50);
                    rightIndexValue.setText("To : " + rightPinIndex * 50);

                    priceStart = leftPinIndex * 50;
                    priceEnd = rightPinIndex * 50;
                }


            });

            btn_apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (FlavourFilterlist.size() > 0 || BrandFilterlist.size() > 0 || priceStart > 1 || priceEnd < 999)
                    {
                        applyFilter();
                        filterTab=false;
                        img_resetFilter.setVisibility(View.INVISIBLE);
                        img_normal_actionbar.setVisibility(View.VISIBLE);
                        txt_filter_actionbar.setVisibility(View.GONE);
                        txt_normal_actionbar.setVisibility(View.VISIBLE);
                        txt_normal_actionbar.setText(CatgName);
                        ll_filter.startAnimation(animDown);
                        ll_filter.setVisibility(View.GONE);
                        ll_items.setVisibility(View.VISIBLE);
                        fl_cart.setVisibility(View.VISIBLE);
                        img_favourite.setVisibility(View.VISIBLE);
                        img_back.setVisibility(View.VISIBLE);
                        img_backfilter.setVisibility(View.GONE);
                    }
                    else
                    {
                        Toast.makeText(mContext, "No Filter selected", Toast.LENGTH_SHORT).show();
                    }


                }
            });

            btn_reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (FlavourFilterlist.size() > 0 || BrandFilterlist.size() > 0 || priceStart > 1 || priceEnd < 999)
                    {
                        for(int i=0;i<expandableDomains.size();i++)
                        {
                            FilterMetadataDO filterMetadataDO=expandableDomains.get(i);
                            for(int j=0;j<filterMetadataDO.flavourlist.size();j++)
                            {
                                FlavourDO flavourDO=filterMetadataDO.flavourlist.get(j);
                                flavourDO.isChecked=false;
                            }

                        }

                        setFilterMetadata();

                        FlavourFilterlist.clear();
                        BrandFilterlist.clear();

                        filterTab=false;
                        img_resetFilter.setVisibility(View.INVISIBLE);
                        img_normal_actionbar.setVisibility(View.VISIBLE);
                        txt_filter_actionbar.setVisibility(View.GONE);
                        txt_normal_actionbar.setVisibility(View.VISIBLE);
                        txt_normal_actionbar.setText(CatgName);
                        ll_filter.startAnimation(animDown);
                        ll_filter.setVisibility(View.GONE);
                        ll_items.setVisibility(View.VISIBLE);
                        fl_cart.setVisibility(View.VISIBLE);
                        img_favourite.setVisibility(View.VISIBLE);
                        img_back.setVisibility(View.VISIBLE);
                        img_backfilter.setVisibility(View.GONE);
                        getItems(CatgId);
                    }
                    else
                    {
                        Toast.makeText(mContext, "No Filter selected", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            txt_resetFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    for(int i=0;i<expandableDomains.size();i++)
                    {
                        FilterMetadataDO filterMetadataDO=expandableDomains.get(i);
                        for(int j=0;j<filterMetadataDO.flavourlist.size();j++)
                        {
                            FlavourDO flavourDO=filterMetadataDO.flavourlist.get(j);
                            flavourDO.isChecked=false;
                        }

                    }

                    setFilterMetadata();

                    FlavourFilterlist.clear();
                    BrandFilterlist.clear();
                    getItems(CatgId);
                }
            });

            img_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(preferenceUtils.isLoggedIn())
                    {
                        Intent intent = new Intent(mContext, MyFavourites.class);
                        startActivity(intent);

                    }
                    else
                    {
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        intent.putExtra("key", "ItemActivity");
                        startActivity(intent);
                    }

                }
            });

        }

    }

    private void setFilterMetadata()
    {
        expandableAdapter = new ExpandableAdapter(mContext, expandableDomains);
        expandableAdapter.registerCheckboxFilterListerner(this);
        exp_flavour.setAdapter(expandableAdapter);

        for (int i=0;i<expandableAdapter.getGroupCount();i++)
        {
            exp_flavour.expandGroup(i);
        }
    }


    @Override
    public void onBackPressed() {

        if(filterTab)
        {
            filterTab=false;
            img_resetFilter.setVisibility(View.INVISIBLE);
            img_normal_actionbar.setVisibility(View.VISIBLE);
            txt_filter_actionbar.setVisibility(View.GONE);
            txt_normal_actionbar.setVisibility(View.VISIBLE);
            txt_normal_actionbar.setText(CatgName);
            ll_filter.startAnimation(animDown);
            ll_filter.setVisibility(View.GONE);
            ll_items.setVisibility(View.VISIBLE);
            fl_cart.setVisibility(View.VISIBLE);
            img_favourite.setVisibility(View.VISIBLE);
            img_back.setVisibility(View.VISIBLE);
            img_backfilter.setVisibility(View.GONE);

        }
        else
        {
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Products Screen");
        if(CatgName.equalsIgnoreCase("DEALS & OFFERS"))
        {
            getCoupons();
        }
        else
        {

            scrollView1.fullScroll(View.FOCUS_UP);
            LocalCartList = CartDatabase.getAllCartList();
            CartCount = CartDatabase.getCartlistCount();
            if (CartCount > 0) {
                txt_cartCount.setVisibility(View.VISIBLE);
                txt_cartCount.setText("" + CartCount);
            } else {
                txt_cartCount.setVisibility(View.GONE);
            }

            if (preferenceUtils.isLoggedIn()) {
                Userdata = preferenceUtils.getUserDetails();
                // CustomerID,CustomerName,CustomerContact,AccessToken
                CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
                CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
                CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
                AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

                LogUtils.error(ItemsActivity.class.getSimpleName(), "CustomerID=" + CustomerID);
                LogUtils.error(ItemsActivity.class.getSimpleName(), "CustomerName=" + CustomerName);
                LogUtils.error(ItemsActivity.class.getSimpleName(), "CustomerContact=" + CustomerContact);
                LogUtils.error(ItemsActivity.class.getSimpleName(), "AccessToken=" + AccessToken);

            }

            getItems(CatgId);
            getFilterMetadata(CatgId);
        }
    }

    private void applyFilter()
    {
       // queryParamsFilterApply

        /*{
            "where": {
            "and": [{
                "or": [{
                    "flavourId": "56cee5d6c4b8f7197ced15ad"
                }, {
                    "flavourId": "56cee5d6c4b8f7197ced15a7"
                }]
            }, {
                "or":
                [{
                    "brandId": "56cee53dc4b8f7197ced15a0"
                }, {
                    "brandId": "56cee53dc4b8f7197ced15a1"
                }]
            },
            {
                "categoryId": "56cee6e8c4b8f7197ced15d1"
            },
            {
                "price":
                {
                    "gte": 200
                }
            },
            {
                "price":
                {
                    "lte": 250
                }
            }
            ]
        }
        }*/

        JSONObject jObject=new JSONObject();
        JSONObject jsonAndObject=new JSONObject();
        JSONObject priceStartObject=new JSONObject();
        JSONObject pricegteObject=new JSONObject();
        JSONObject priceEndObject=new JSONObject();
        JSONObject pricelteObject=new JSONObject();
        JSONObject categoryIdObject=new JSONObject();

        JSONObject brandObject;
        JSONArray BrandOrArray=new JSONArray();
        JSONObject BrandOrObject=new JSONObject();

        JSONObject flavourObject;
        JSONArray FlavourOrArray=new JSONArray();
        JSONObject FlavourOrObject=new JSONObject();

        JSONArray AndArray=new JSONArray();

        try {
            pricegteObject.put("gte",priceStart);
            priceStartObject.put("price",pricegteObject);

            pricelteObject.put("lte",priceEnd);
            priceEndObject.put("price",pricelteObject);

            categoryIdObject.put("categoryId",CatgId);
            if(BrandFilterlist.size()>0)
            {
                for(int i=0;i<BrandFilterlist.size();i++)
                {
                    brandObject=new JSONObject();
                    brandObject.put("brandId",BrandFilterlist.get(i).Id);
                   LogUtils.error(ItemsActivity.class.getSimpleName(), "BrandFilterlist id at =" + i + "= " + BrandFilterlist.get(i).Id);
                    BrandOrArray.put(brandObject);
                }

                BrandOrObject.put("or",BrandOrArray);
                AndArray.put(BrandOrObject);
            }

            if(FlavourFilterlist.size()>0)
            {
                for(int j=0;j<FlavourFilterlist.size();j++)
                {
                    flavourObject=new JSONObject();
                    flavourObject.put("flavourId",FlavourFilterlist.get(j).Id);
                    LogUtils.error(ItemsActivity.class.getSimpleName(), "FlavourFilterlist id at ="+j+"= " + FlavourFilterlist.get(j).Id);
                    FlavourOrArray.put(flavourObject);
                }
                FlavourOrObject.put("or",FlavourOrArray);
                AndArray.put(FlavourOrObject);
            }
            AndArray.put(categoryIdObject);
            AndArray.put(priceEndObject);
            AndArray.put(priceStartObject);
            jsonAndObject.put("and",AndArray);
            jObject.put("where",jsonAndObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        queryParamsFilterApply=jObject.toString();
        LogUtils.error(ItemsActivity.class.getSimpleName(),"queryParamsFilterApply="+queryParamsFilterApply);


        showLoaderNew();
        if(new CommonBL(mContext, ItemsActivity.this).getApplyFilter(queryParamsFilterApply))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }


    private void getCoupons()
    {
        showLoaderNew();
        if(new CommonBL(mContext, ItemsActivity.this).getCoupons()) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }

    private void getFilterMetadata(String CatgId)
    {
//URL : http://104.196.99.177:4848/api/Categories/getFlavoursAndBrands?categoryId=56cee6e8c4b8f7197ced15cf
        if(new CommonBL(mContext, ItemsActivity.this).getFilterMetadata(CatgId))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }

    private void addFavouritesToServer(ItemsDO model)
    {
// URL : http://104.196.99.177:4848/api/Favourites?access_token=PEbhQsJBHFBTFvVIFTXezw7hpelRNFNCHVC2KQd0CcatrcJy6Vu1HNgHXBtac7oU

        String queryParam=AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, ItemsActivity.this).addFavouritesToServer(CustomerID, model, queryParam))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }

    private void getItems(String CatgId)
    {
        // URL : http://104.196.99.177:4848/api/Items?filter={"where":{"categoryId":"56cee6e8c4b8f7197ced15cf"}}&access_token=
        showLoaderNew();
        JSONObject jsonObject=new JSONObject();
        //{"where":{"categoryId":"56cee6e8c4b8f7197ced15cf"}}
        JSONObject jObject=new JSONObject();
        try {
            jObject.put("categoryId",CatgId);
            jsonObject.put("where",jObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String queryParam=jsonObject.toString()+"&access_token="+AccessToken;
        if(new CommonBL(mContext, ItemsActivity.this).getItems(queryParam)) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void removeFavouriteItemFromServer(ItemsDO model) {

        String queryParam=model.favouriteId+"?access_token="+AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, ItemsActivity.this).removeFavouriteItemFromServer(queryParam))
        {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }

    private void baseInitializeControls()
    {
        if(CatgName.equalsIgnoreCase("DEALS & OFFERS"))
        {
            mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
            txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
            txt_filter_actionbar=(TextView)findViewById(R.id.txt_filter_actionbar);
            img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
            img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
            img_favourite=(ImageView)findViewById(R.id.img_favourite);
            img_back=(ImageView)findViewById(R.id.img_back);

            lst_coupons      = (ListView) findViewById(R.id.lst_coupons);
            ll_NoCoupons  =(LinearLayout) findViewById(R.id.ll_NoCoupons);

            txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
            fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
            fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
            txt_addfavourite=(TextView) findViewById(R.id.txt_addfavourite);
        }
        else
        {
            mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
            txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
            txt_filter_actionbar=(TextView)findViewById(R.id.txt_filter_actionbar);
            img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
            img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
            img_favourite=(ImageView)findViewById(R.id.img_favourite);
            img_back=(ImageView)findViewById(R.id.img_back);
            img_backfilter=(ImageView)findViewById(R.id.img_backfilter);

            grd_items      = (GridView) findViewById(R.id.grd_items);
            tvItemsNoData  =(TextView) findViewById(R.id.tvItemsNoData);
            txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
            txt_resetFilter=(TextView) findViewById(R.id.txt_resetFilter);
            fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
            ll_filter=(LinearLayout) findViewById(R.id.ll_filter);
            ll_items=(FrameLayout) findViewById(R.id.ll_items);
            exp_flavour=(ExpandableListView) findViewById(R.id.exp_flavour);
            leftIndexValue = (TextView) findViewById(R.id.leftIndexValue);
            rightIndexValue = (TextView) findViewById(R.id.rightIndexValue);
            rangebar = (RangeBar) findViewById(R.id.rangebar1);
            scrollView1= (ScrollView) findViewById(R.id.scrollView1);
            btn_apply = (Button) findViewById(R.id.btn_apply);
            btn_reset= (Button) findViewById(R.id.btn_reset);

            ll_NoItems= (LinearLayout) findViewById(R.id.ll_NoItems);

            ll_filter.bringToFront();
        }


    }

    @Override
    public void dataRetreived(Response data) {


        if (data != null && data.method == ServiceMethods.WS_GET_ITEMS)
        {
            hideloader();
            if(!data.isError)
            {
                arrItems = (ArrayList<ItemsDO>) data.data;
                if(arrItems!=null&&arrItems.size()>0)
                {
                    for (int i = 0; i < arrItems.size(); i++)
                    {
                        Message = arrItems.get(i).Status;
                    }
                    if (Message.equalsIgnoreCase("success"))
                    {
                        ll_NoItems.setVisibility(View.GONE);
                        grd_items.setVisibility(View.VISIBLE);
                        if (LocalCartList.size() > 0)
                        {
                            for (int i = 0; i < arrItems.size(); i++) {
                                for (int j = 0; j < LocalCartList.size(); j++) {
                                    if (LocalCartList.get(j).ItemId.equalsIgnoreCase(arrItems.get(i).ItemId)) {
                                        arrItems.get(i).cartIconStatus = true;
                                        arrItems.get(i).ItemQty = LocalCartList.get(j).ItemQty;
                                    }
                                }
                            }
                        }

                        itemsGridAdapter = new ItemsGridAdapter(mContext, arrItems);
                        itemsGridAdapter.registerCartListener(this);
                        grd_items.setAdapter(itemsGridAdapter);

                    }
                    else
                    {
                        ll_NoItems.setVisibility(View.VISIBLE);
                        grd_items.setVisibility(View.GONE);
                    }
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_APPLY_FILTER)
        {
            hideloader();
            if(!data.isError)
            {
                arrItemsFilter = (ArrayList<ItemsDO>) data.data;
                if(arrItemsFilter!=null&&arrItemsFilter.size()>0)
                {
                    for (int i = 0; i < arrItemsFilter.size(); i++)
                    {
                        Message = arrItemsFilter.get(i).Status;
                    }
                    if (Message.equalsIgnoreCase("success"))
                    {
                        ll_NoItems.setVisibility(View.GONE);
                        grd_items.setVisibility(View.VISIBLE);
                        if (LocalCartList.size() > 0) {
                            for (int i = 0; i < arrItemsFilter.size(); i++) {
                                for (int j = 0; j < LocalCartList.size(); j++) {
                                    if (LocalCartList.get(j).ItemId.equalsIgnoreCase(arrItemsFilter.get(i).ItemId)) {
                                        arrItemsFilter.get(i).cartIconStatus = true;
                                        arrItemsFilter.get(i).ItemQty = LocalCartList.get(j).ItemQty;
                                    }
                                }
                            }
                        }

                        itemsGridAdapter = new ItemsGridAdapter(mContext, arrItemsFilter);
                        itemsGridAdapter.registerCartListener(this);
                        grd_items.setAdapter(itemsGridAdapter);


                    }
                    else
                    {
                        ll_NoItems.setVisibility(View.VISIBLE);
                        grd_items.setVisibility(View.GONE);
                    }
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_ADD_FAVOURITE_ITEM)
        {
            hideloader();
            if(!data.isError)
            {
                arrAddFavouriteDO = (ArrayList<AddFavouriteDO>) data.data;
                if(arrAddFavouriteDO!=null&&arrAddFavouriteDO.size()>0)
                {
                    getItems(CatgId);
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_DELETE_FAVOURITE_ITEMS)
        {
            hideloader();
            if(!data.isError)
            {
                arrAddFavouriteDO = (ArrayList<AddFavouriteDO>) data.data;
                if(arrAddFavouriteDO!=null&&arrAddFavouriteDO.size()>0)
                {
                    getItems(CatgId);
                }
            }
        }


       else if (data != null && data.method == ServiceMethods.WS_SHOW_COUPON)
        {
            hideloader();
            if(!data.isError)
            {
                arrShowCouponDO = (ArrayList<ShowCouponDO>) data.data;
                if(arrShowCouponDO!=null&&arrShowCouponDO.size()>0)
                {
                    for (int i=0;i<arrShowCouponDO.size();i++)
                    {
                        Message=arrShowCouponDO.get(i).Status;
                    }
                    if(Message.equalsIgnoreCase("success"))
                    {
                        ll_NoCoupons.setVisibility(View.GONE);
                        lst_coupons.setVisibility(View.VISIBLE);

                        showCouponAdapter =new ShowCouponAdapter(mContext, arrShowCouponDO);

                        lst_coupons.setAdapter(showCouponAdapter);
                    }
                    else
                    {
                        ll_NoCoupons.setVisibility(View.VISIBLE);
                        lst_coupons.setVisibility(View.GONE);
                    }


                }
                else
                {
                    ll_NoCoupons.setVisibility(View.VISIBLE);
                    lst_coupons.setVisibility(View.GONE);
                }
            }
        }



        else if (data != null && data.method == ServiceMethods.WS_GET_FILTER_METADATA)
        {

            if(!data.isError)
            {
                arrFilterMetadDO = (ArrayList<FilterMetadataDO>) data.data;
                if(arrFilterMetadDO!=null&&arrFilterMetadDO.size()>0)
                {
                    flavourlist=arrFilterMetadDO.get(0).flavourlist;

                    brandslist=arrFilterMetadDO.get(0).brandslist;

                    domain1 = new FilterMetadataDO();

                    domain2 = new FilterMetadataDO();

                    domain1.parent = "Flavours";

                    for(int i=0;i<flavourlist.size();i++)
                    {
                        domain1.flavourlist.add(flavourlist.get(i));
                    }

                    domain2.parent = "Brands";

                    for(int j=0;j<brandslist.size();j++)
                    {
                        domain2.flavourlist.add(brandslist.get(j));
                    }

                    expandableDomains=new ArrayList<>();

                    expandableDomains.add(domain1);
                    expandableDomains.add(domain2);

                    expandableAdapter = new ExpandableAdapter(mContext, expandableDomains);
                    expandableAdapter.registerCheckboxFilterListerner(this);
                    exp_flavour.setAdapter(expandableAdapter);

                    for (int i=0;i<expandableAdapter.getGroupCount();i++)
                    {
                        exp_flavour.expandGroup(i);
                    }

                }
                else
                {

                }
            }
        }
    }

    @Override
    public void getCartList(ItemsDO model) {

        LocalCartList=CartDatabase.getAllCartList();
        txt_cartCount.setVisibility(View.VISIBLE);
        txt_cartCount.setText("" + CartDatabase.getCartlistCount());
    }


    @Override
    public void getLocalDatabaseCartList() {

    }

    @Override
    public void getTheIncreasedSubtotal(Double itemFinalPrice) {

    }

    @Override
    public void getTheDecreasedSubtotal(Double itemFinalPrice) {

    }

    @Override
    public void addFavouriteItem(ItemsDO model)
    {
        if(preferenceUtils.isLoggedIn())
        {
            addFavouritesToServer(model);
        }
        else
        {
            Intent intent=new Intent(mContext,LoginActivity.class);
            intent.putExtra("key","ItemActivity");
            startActivity(intent);
        }
    }

    @Override
    public void removeFavouriteItem(ItemsDO model) {

        removeFavouriteItemFromServer(model);

    }



    @Override
    public void getSizeFavouriteItemList(int Size) {

    }


    @Override
    public void getCheckBoxFilter(FlavourDO flavourDO)
    {
        if(flavourDO.isChecked)
        {
           if(flavourDO.Type==1)
           {
               FlavourFilterlist.add(flavourDO);
           }
            else
           {
               BrandFilterlist.add(flavourDO);
           }
        }
        else
        {
            if(flavourDO.Type==1)
            {
                for(int i=0;i<FlavourFilterlist.size();i++)
                {

                    if(flavourDO.Id.equalsIgnoreCase(FlavourFilterlist.get(i).Id))
                    {
                        FlavourFilterlist.remove(i);
                    }
                }
            }
            else
            {
                for(int i=0;i<BrandFilterlist.size();i++)
                {

                    if(flavourDO.Id.equalsIgnoreCase(BrandFilterlist.get(i).Id))
                    {
                        BrandFilterlist.remove(i);
                    }
                }
            }

        }
        LogUtils.error(ItemsActivity.class.getSimpleName(),"FlavourFilterlist="+FlavourFilterlist.size());
        LogUtils.error(ItemsActivity.class.getSimpleName(),"BrandFilterlist="+BrandFilterlist.size());

    }
}
