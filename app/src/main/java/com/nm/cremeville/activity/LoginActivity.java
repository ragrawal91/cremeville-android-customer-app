package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.CheckRegistrationDO;
import com.nm.cremeville.objects.LoginDO;
import com.nm.cremeville.objects.UpdateNameDO;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;

public class LoginActivity extends BaseNew implements DataListener {
    RelativeLayout registration_layout;
    EditText edt_name,edt_otp,edt_mobile,edt_firstmobile;
    Button btn_signup,btn_next,btn_resendOTP;
    Context mContext;
    char mDigit;
    String UserName,UserFirstMobile,UserOTP,UserOTPValue;
    String AccessToken,CustomerId,Password,CustomerPhone,CustomerName,TotalLoyaltyPoint,LoyaltyAmount;
    String  Message;
    private ArrayList<LoginDO> arrLogin;
    private ArrayList<UpdateNameDO> arrUpdateName;
    private ArrayList<CheckRegistrationDO> arrChkRegDO;
    PreferenceUtils preferenceUtils;
    String key,Error="";
    LinearLayout ll_checkRegistration;

    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_favourite;
    FrameLayout fl_cart,fl_Filter;

    double subtotal,GrandTotal,vatTax;
    String deliveryValue;
    String accesstoken,customerid,password,customerphone,customername,customer_email,totalloyaltypoint,loyaltyamount;
    FontType fonttype;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        mContext=LoginActivity.this;
        initializeControls();
        preferenceUtils=new PreferenceUtils(mContext);

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);

        }
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/

        fl_cart.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        txt_normal_actionbar.setText("Basic Details");
        img_favourite.setVisibility(View.GONE);

        registration_layout.setVisibility(View.GONE);
        ViewGroup root = (ViewGroup)findViewById(R.id.ll_login);
        fonttype = new FontType(mContext, root);

        key = getIntent().getExtras().getString("key");

        if(key.equalsIgnoreCase("Cart"))
        {
            subtotal = getIntent().getExtras().getDouble("subtotal");
            vatTax = getIntent().getExtras().getDouble("vatTax");
            deliveryValue = getIntent().getExtras().getString("deliveryCharges");
            GrandTotal = getIntent().getExtras().getDouble("GrandTotal");
        }



        edt_mobile.setEnabled(true);
        edt_name.setEnabled(true);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                UserFirstMobile=edt_firstmobile.getText().toString();
                if (UserFirstMobile.length() > 0 )
                {
                    mDigit = UserFirstMobile.charAt(0);
                    if (UserFirstMobile.length() == 10)
                    {
                        if (mDigit == '7' || mDigit == '8' || mDigit == '9' )
                        {
                            checkUserExist(UserFirstMobile);
                        }
                        else
                        {
                            showAlertDialog("Enter a valid mobile number", "Ok");
                        }
                    }
                    else
                    {
                        showAlertDialog("Enter a valid mobile number", "Ok");
                    }
                }
                else
                {
                    showAlertDialog("Please enter your mobile number", "Ok");
                }

            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                getTextSignup();

                if (UserName.length() > 0 && UserOTP.length() > 0)
                {
                        if (UserOTP.equalsIgnoreCase(UserOTPValue))
                        {
                            getLogin(UserOTP,UserFirstMobile);
                        }
                        else
                        {
                            showAlertDialog("Enter correct OTP", "Ok");
                        }
                }
                else
                {
                    showAlertDialog("Please fill mandatory fields", "Ok");
                }
            }
        });

        btn_resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                UserFirstMobile=edt_firstmobile.getText().toString();
                if (UserFirstMobile.length() > 0 )
                {
                    mDigit = UserFirstMobile.charAt(0);
                    if (UserFirstMobile.length() == 10)
                    {
                        if (mDigit == '7' || mDigit == '8' || mDigit == '9' )
                        {
                            checkUserExist(UserFirstMobile);
                        }
                        else
                        {
                            showAlertDialog("Enter a valid mobile number", "Ok");
                        }
                    }
                    else
                    {
                        showAlertDialog("Enter a valid mobile number", "Ok");
                    }
                }
                else
                {
                    showAlertDialog("Please enter your mobile number", "Ok");
                }

            }
        });
    }

    private void getTextSignup()
    {
        UserName=edt_name.getText().toString();
        UserOTP=edt_otp.getText().toString();
    }

    private void checkUserExist(String userFirstMobile) {

        showLoaderNew();

        if(new CommonBL(mContext, LoginActivity.this).checkUserExist(userFirstMobile))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void getLogin(String UserOTP, String UserFirstMobile) {

        showLoaderNew();

        if(new CommonBL(mContext, LoginActivity.this).getLogin(UserOTP, UserFirstMobile))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }


    private void updateCustomerName(String name, String customerid, String accesstoken) {

        showLoaderNew();
        String queryparam=customerid+"?access_token="+accesstoken;
        if(new CommonBL(mContext, LoginActivity.this).updateCustomerName(name,queryparam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void initializeControls() {

        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        edt_mobile=(EditText) findViewById(R.id.edt_mobile);
        edt_name=(EditText) findViewById(R.id.edt_name);
        edt_otp=(EditText) findViewById(R.id.edt_otp);
        edt_firstmobile=(EditText) findViewById(R.id.edt_firstmobile);
        btn_next=(Button) findViewById(R.id.btn_next);
        btn_signup=(Button) findViewById(R.id.btn_signup);
        btn_resendOTP=(Button) findViewById(R.id.btn_resendOTP);
        registration_layout=(RelativeLayout) findViewById(R.id.registration_layout);
        ll_checkRegistration=(LinearLayout) findViewById(R.id.ll_checkRegistration);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Login Screen");
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
       if (data != null && data.method == ServiceMethods.WS_CHECK_CUSTOMER_EXISTS)
        {
            if(!data.isError)
            {
                arrChkRegDO = (ArrayList<CheckRegistrationDO>) data.data;
                if(arrChkRegDO!=null&&arrChkRegDO.size()>0)
                {
                    for(int i=0;i<arrChkRegDO.size();i++)
                    {
                        Message=arrChkRegDO.get(i).Message;
                        UserOTPValue=arrChkRegDO.get(i).OTPMessage;
                        CustomerName=arrChkRegDO.get(i).CustomerName;
                        LogUtils.error(LoginActivity.class.getSimpleName(),"Customer Name="+CustomerName);
                        LogUtils.error(LoginActivity.class.getSimpleName(),"UserOTPValue="+UserOTPValue);
                        LogUtils.error(LoginActivity.class.getSimpleName(),"Message="+Message);

                    }
                    if (Message.equalsIgnoreCase("Existed User"))
                    {
                       // Toast.makeText(mContext, "Existing Customer", Toast.LENGTH_SHORT).show();
                        registration_layout.setVisibility(View.VISIBLE);
                        ll_checkRegistration.setVisibility(View.GONE);
                        edt_mobile.setEnabled(false);
                        edt_name.setEnabled(false);
                        edt_mobile.setText(UserFirstMobile);
                        edt_name.setText(CustomerName);
                        btn_signup.setText("LOGIN");
                    }
                    else if(Message.equalsIgnoreCase("New User"))
                    {
                        //Toast.makeText(mContext, "New Registration", Toast.LENGTH_SHORT).show();
                        registration_layout.setVisibility(View.VISIBLE);
                        ll_checkRegistration.setVisibility(View.GONE);
                        edt_mobile.setEnabled(false);
                        edt_mobile.setText(UserFirstMobile);
                        btn_signup.setText("SIGNUP");
                    }
                }
            }
        }

       else if (data != null && data.method == ServiceMethods.WS_GET_LOGGED_IN)
        {
            if(!data.isError)
            {
                arrLogin = (ArrayList<LoginDO>) data.data;
                if(arrLogin!=null&&arrLogin.size()>0)
                {
                    for(int i=0;i<arrLogin.size();i++)
                    {
                        Message=arrLogin.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrLogin.size();i++)
                        {
                            //accesstoken,customerid,password,customerphone,customername,totalloyaltypoint,loyaltyamount
                            Message=arrLogin.get(i).Status;
                            customerid=arrLogin.get(i).CustomerId;
                            password=arrLogin.get(i).Password;
                            customerphone=arrLogin.get(i).CustomerPhone;
                            customername=arrLogin.get(i).CustomerName;
                            customer_email=arrLogin.get(i).CustomerEmail;
                            accesstoken=arrLogin.get(i).AccessToken;
                            totalloyaltypoint=arrLogin.get(i).TotalLoyaltyPoint;
                            loyaltyamount=arrLogin.get(i).LoyaltyAmount;
                            LogUtils.error(LoginActivity.class.getSimpleName(),"Logged IN");
                        }

                       if(customername.equalsIgnoreCase("") && customername.isEmpty())
                       {
                           updateCustomerName(UserName,customerid,accesstoken);

                       }
                        else
                       {
                           if(key.equalsIgnoreCase("ItemActivity"))
                           {
                               finish();
                           }
                           else if(key.equalsIgnoreCase("ItemActivity"))
                           {
                               Intent intent = new Intent(mContext, MyFavourites.class);
                               startActivity(intent);
                               finish();
                           }

                           else if(key.equalsIgnoreCase("cartActivity"))
                           {
                               Intent intent = new Intent(mContext, MyFavourites.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("Dashboard"))
                           {
                               Intent intent = new Intent(mContext, MyFavourites.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("MyProfile"))
                           {
                               Intent intent = new Intent(mContext, MyProfile.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("MyOrders"))
                           {
                               Intent intent = new Intent(mContext, MyOrders.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("SidebarLogin"))
                           {
                               Intent intent = new Intent(mContext, Dashboard.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("MyFavourites"))
                           {
                               Intent intent = new Intent(mContext, MyFavourites.class);
                               startActivity(intent);
                               finish();
                           }
                           else if(key.equalsIgnoreCase("DashboardLogin"))
                           {
                               finish();
                           }
                           else if(key.equalsIgnoreCase("ReviewActivity"))
                           {
                               finish();
                           }
                           else
                           {
                               Intent intent = new Intent(mContext, CheckoutActivity.class);
                               intent.putExtra("subtotal", subtotal);
                               intent.putExtra("vatTax", vatTax);
                               intent.putExtra("deliveryCharges", deliveryValue);
                               intent.putExtra("GrandTotal", GrandTotal);
                               startActivity(intent);
                               finish();
                           }
                       }
                        preferenceUtils.createLoginSession(customerid, password, customerphone, UserName, customer_email, accesstoken);
                        preferenceUtils.saveLoyaltyPoints(totalloyaltypoint,loyaltyamount);
                    }
                    else if (Message.equalsIgnoreCase("Login Failed"))
                    {
                        showAlertDialog("Invalid Credentials", "Ok");
                    }
                }
            }
        }

       else if (data != null && data.method == ServiceMethods.WS_UPDATE_CUSTOMERNAME)
       {
           if(!data.isError)
           {
               arrUpdateName = (ArrayList<UpdateNameDO>) data.data;
               if(arrUpdateName!=null&&arrUpdateName.size()>0)
               {
                   for(int i=0;i<arrUpdateName.size();i++)
                   {
                       Message=arrLogin.get(i).Status;
                   }

                   if (Message.equalsIgnoreCase("success"))
                   {
                       if(key.equalsIgnoreCase("ItemActivity"))
                       {
                           finish();
                       }
                       else if(key.equalsIgnoreCase("ItemActivity"))
                       {
                           Intent intent = new Intent(mContext, MyFavourites.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("cartActivity"))
                       {
                           Intent intent = new Intent(mContext, MyFavourites.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("Dashboard"))
                       {
                           Intent intent = new Intent(mContext, MyFavourites.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("MyProfile"))
                       {
                           Intent intent = new Intent(mContext, MyProfile.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("MyOrders"))
                       {
                           Intent intent = new Intent(mContext, MyOrders.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("SidebarLogin"))
                       {
                           Intent intent = new Intent(mContext, Dashboard.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("MyFavourites"))
                       {
                           Intent intent = new Intent(mContext, MyFavourites.class);
                           startActivity(intent);
                           finish();
                       }
                       else if(key.equalsIgnoreCase("DashboardLogin"))
                       {
                           finish();
                       }
                       else if(key.equalsIgnoreCase("ReviewActivity"))
                       {
                           finish();
                       }
                       else
                       {
                           LogUtils.error(LoginActivity.class.getSimpleName(), "Name Updated");
                           Intent intent = new Intent(mContext, CheckoutActivity.class);
                           intent.putExtra("subtotal", subtotal);
                           intent.putExtra("vatTax", vatTax);
                           intent.putExtra("deliveryCharges", deliveryValue);
                           intent.putExtra("GrandTotal", GrandTotal);
                           startActivity(intent);
                           finish();
                       }
                   }
                   else
                   {
                       showAlertDialog("Server Error", "Ok");
                   }
               }
           }
       }
    }
}
