package com.nm.cremeville.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nm.cremeville.R;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.CurrentAddressDO;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.GPSTracker;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.NetworkUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;

public class LocationActivity extends AppCompatActivity implements DataListener,LocationListener {

    Context mContext;
    TextView txt_MyLocation,txt_selectManually;
    PreferenceUtils preferenceUtils;
    boolean doubleBackToExitPressedOnce = false;
    FontType fonttype;
    Dialog dialog;
    NetworkUtils networkUtils;
    GPSTracker gpsTracker;
    LocationManager locationManager;
    String provider = "NA";
    Location currlocation;
    boolean isGPSEnabled;
    private static final int GPSREQUESTCODE = 1000;
    AlertDialog gpsalert;
    AlertDialog alertDialog;
    double currlongitude = 0, currlangitude = 0;
    ArrayList<CurrentAddressDO>  arrCurrAddressDO;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        mContext = LocationActivity.this;
        initializeControls();

        dialog = new Dialog(mContext);
        networkUtils = new NetworkUtils();
        preferenceUtils = new PreferenceUtils(mContext);
        gpsTracker = new GPSTracker(mContext);
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_location);
        fonttype = new FontType(mContext, root);

        txt_MyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gpsTracker.isGPSEnabled)
                {
                    onGPSButtonCLicked();
                }
                else
                {
                    showGPSDisabledAlertToUser();
                    return;
                }
            }
        });

        txt_selectManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(mContext,ManualLocation.class);
                startActivity(intent);
            }
        });

    }

    private void initializeControls()
    {
        txt_MyLocation=(TextView)findViewById(R.id.txt_MyLocation);
        txt_selectManually=(TextView)findViewById(R.id.txt_selectManually);
    }

    private void onGPSButtonCLicked() {

        if (!networkUtils.isNetworkConnectionAvailable(mContext))
        {
            //   showAlertDialog("Internet Connection", "OK");
            Toast.makeText(mContext,"Internet Connection",Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            startgpsservice();
            gpsTracker = new GPSTracker(mContext);
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);
            preferenceUtils = new PreferenceUtils(mContext);
            currlocation = locationManager.getLastKnownLocation(provider);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean playservicestatus = check_googleplay_service();

            if (!playservicestatus)
            {
                //   showAlertDialog("Google Play Service Error", "OK");
                Toast.makeText(mContext,"Google Play Service Error",Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                gpsTracker.startUsingGPS();
                gpsTracker.startLocationUpdates();
                if (isGPSEnabled)
                {
                    gpsTracker.isGPSEnabled = true;
                    gpsTracker.startUsingGPS();
                    gpsTracker.startLocationUpdates();
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
                    //requestfromresume=true;
                  //  Toast.makeText(mContext, "Location Latitude is=" + gpsTracker.latitude, Toast.LENGTH_SHORT).show();
                  //  Toast.makeText(mContext, "Location Longitude is=" + gpsTracker.longitude, Toast.LENGTH_SHORT).show();

                    if(gpsTracker.longitude==0)
                    {
                        LogUtils.error(LocationActivity.class.getSimpleName(), "when 0 longitude is" + gpsTracker.longitude + "latitude is" + gpsTracker.latitude);

                        onGPSButtonCLicked();
                    }
                    else
                    {
                        LogUtils.error(LocationActivity.class.getSimpleName(), "success longitude is" + gpsTracker.longitude + "latitude is" + gpsTracker.latitude);
                        preferenceUtils.saveCurrentLocation(String.valueOf(gpsTracker.latitude), String.valueOf(gpsTracker.longitude));

                        getCompleteAddress(gpsTracker.latitude, gpsTracker.longitude);

                    }
                  }
                else
                {
                    showGPSDisabledAlertToUser();
                    return;
                }
            }
        }
    }

    private void getCompleteAddress(double latitude, double longitude)
    {
        //	http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true
        String queryParam=latitude+","+longitude+"&sensor=true";
        //showLoaderNew();
        if(new CommonBL(mContext, LocationActivity.this).getCompleteAddress(queryParam))
        {
//			showLoader("Loading all brands...");

        }
        else
        {
        //    hideloader();
            //showAlertDialog("NO INTERNET","OK");
        }


    }
    @Override
    public void dataRetreived(Response data) {
        if (data != null && data.method == ServiceMethods.WS_GET_CURRENT_ADDRESS)
        {
            if(!data.isError)
            {
                arrCurrAddressDO = (ArrayList<CurrentAddressDO>) data.data;
                if(arrCurrAddressDO!=null&&arrCurrAddressDO.size()>0)
                {

                    // txt_actionbar.setText(arrCurrAddressDO.get(0).areaname+", "+arrCurrAddressDO.get(0).statename);

                    preferenceUtils.saveCurrentAddress(arrCurrAddressDO.get(0).areaname,arrCurrAddressDO.get(0).statename);
                    Intent intent = new Intent(mContext, Dashboard.class);
                    startActivity(intent);
                }
                else
                {

                }
            }
        }
    }


    protected void onResume()
    {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(dialog!=null)
        {
            dialog.dismiss();
        }

        if(gpsTracker!=null)
        {
            gpsTracker.stopLocationUpdates();

            gpsTracker.stopUsingGPS();
        }

        if(alertDialog!=null)
        {
            alertDialog.dismiss();
        }

        if(gpsalert!=null)
        {
            gpsalert.dismiss();
        }

        locationManager.removeUpdates(this);
    }

    private void startgpsservice() {

        Intent gpsintent=new Intent(this,GPSTracker.class);
        startService(gpsintent);
    }

    private void showGPSDisabledAlertToUser() {

        AlertDialog.Builder  alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                //startActivity(callGPSSettingIntent);
                                startActivityForResult(callGPSSettingIntent,GPSREQUESTCODE);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        gpsalert = alertDialogBuilder.create();
        gpsalert.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        LogUtils.error(LocationActivity.class.getSimpleName(), "Location Activity :  OnActivity Rseult called");
        super.onActivityResult(requestCode, resultCode, data);
            if(requestCode==GPSREQUESTCODE)
            {
                gpsTracker.isGPSEnabled=true;
                onGPSButtonCLicked();
            }
    }

    @Override
    public void onLocationChanged(Location location) {

        currlangitude=location.getLatitude();
        currlongitude=location.getLongitude();


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @SuppressWarnings("deprecation")
    private boolean check_googleplay_service() {

        // Getting google play service status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);

        // Showing status
        if(status== ConnectionResult.SUCCESS)
        {
            return true;
        }
        else if(status== ConnectionResult.SERVICE_MISSING)
        {
            //getCustomToast("GOOGLE PLAY SERVICE MISSING!!");
            return false;
        }
        else if(status== ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED)
        {
            // getCustomToast("PLEASE UPDATE GOOGLE PLAY SERVICE!!!");
            return false;
        }
        else if(status== ConnectionResult.SERVICE_DISABLED)
        {
            // getCustomToast("GOOGLE PLAY SERVICE IS DISABLED");
            return false;
        }
        else if(status== ConnectionResult.SERVICE_INVALID)
        {
            //getCustomToast("GOOGLE PLAY SERVICE INVALID");
            return false;
        }
        else
        {
            return false;
        }

    }
}
