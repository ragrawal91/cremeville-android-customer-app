package com.nm.cremeville.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.PreferenceUtils;

public class MyCremeeActivity extends BaseNew {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;
    FrameLayout fl_cart,fl_Filter;
    FontType fonttype;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycremee);
        mContext=MyCremeeActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_coupons);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("My Cremee");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("MyCremee Screen");
    }




    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
    }

}
