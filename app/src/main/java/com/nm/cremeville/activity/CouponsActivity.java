package com.nm.cremeville.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.ShowCouponAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.ShowCouponDO;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;

public class CouponsActivity extends BaseNew implements DataListener {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,txt_cartCount;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite;
    PreferenceUtils preferenceUtils;

    FrameLayout fl_cart,fl_Filter;
    FontType fonttype;
    private ArrayList<ShowCouponDO> arrShowCouponDO;
    ShowCouponAdapter showCouponAdapter;
    private ListView lst_coupons;
    LinearLayout ll_NoCoupons;
    TextView txt_addfavourite;
    String Message;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);
        mContext=CouponsActivity.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_coupons);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("Coupons");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Coupons Screen");
        getCoupons();

    }

    private void getCoupons()
    {
        showLoaderNew();
        if(new CommonBL(mContext, CouponsActivity.this).getCoupons()) {
        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET", "OK");
        }
    }


    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);

        lst_coupons      = (ListView) findViewById(R.id.lst_coupons);
        ll_NoCoupons  =(LinearLayout) findViewById(R.id.ll_NoCoupons);

        txt_cartCount=(TextView) findViewById(R.id.txt_cartCount);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);
        txt_addfavourite=(TextView) findViewById(R.id.txt_addfavourite);
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_SHOW_COUPON)
        {
            if(!data.isError)
            {
                arrShowCouponDO = (ArrayList<ShowCouponDO>) data.data;
                if(arrShowCouponDO!=null&&arrShowCouponDO.size()>0)
                {
                    for (int i=0;i<arrShowCouponDO.size();i++)
                    {
                       Message=arrShowCouponDO.get(i).Status;
                    }
                    if(Message.equalsIgnoreCase("success"))
                    {
                        ll_NoCoupons.setVisibility(View.GONE);
                        lst_coupons.setVisibility(View.VISIBLE);

                        showCouponAdapter =new ShowCouponAdapter(mContext, arrShowCouponDO);

                        lst_coupons.setAdapter(showCouponAdapter);
                    }
                    else
                    {
                        ll_NoCoupons.setVisibility(View.VISIBLE);
                        lst_coupons.setVisibility(View.GONE);
                    }


                }
                else
                {
                    ll_NoCoupons.setVisibility(View.VISIBLE);
                    lst_coupons.setVisibility(View.GONE);
                }
            }
        }
    }


}
