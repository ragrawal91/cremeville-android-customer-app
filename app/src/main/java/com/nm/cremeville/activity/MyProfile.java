package com.nm.cremeville.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nm.cremeville.R;
import com.nm.cremeville.adapter.CustomerAddressAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.AddFavouriteDO;
import com.nm.cremeville.objects.AddressDO;
import com.nm.cremeville.objects.EditAddressDO;
import com.nm.cremeville.objects.SaveAddressDO;
import com.nm.cremeville.objects.UpdateNameDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyProfile extends BaseNew implements DataListener {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar,tv_customerMobile,txt_addresstype,tv_save,tv_edit;
    public ImageView img_normal_actionbar,img_back,img_resetFilter,img_favourite,img_backfilter;
    PreferenceUtils preferenceUtils;
    public HashMap<String,String> Userdata;
    String CustomerID,CustomerName,CustomerContact,CustomerEmail,AccessToken,EditedCustomerName;
    FrameLayout fl_cart,fl_Filter;
    ArrayList<SaveAddressDO>  arrSaveAddressDO;
    ArrayList<AddressDO>  arrAddressDO;
    String AddressFlag;
    AddressDO addressDO;
    CustomerAddressAdapter customerAddressAdapter;
    FontType fonttype;
    String addressKey,DeliveryAddress="null",HomeAddressId="null",OfficeAddressId="null",OtherAddressId="null",AddressId;
    String HouseNumber,Address,Landmark,City,Pincode,AreaName;
    EditText edt_housenumber,edt_address,edt_landmark,edt_city,edt_pincode,edt_areaname,edt_email,edt_customername;
    Button btn_SaveAddress,btn_editAddress;

    TextView txt_homeAddress,txt_officeAddress,txt_othersAddress;
    CheckBox chk_addHomeAddress,chk_addofficeAddress,chk_addothersAddress;

    StringBuilder strHomeAddress,strOfficeAddress,strOthersAddress;
    String StoreID,OfficeStoreID,HomeStoreID,OthersStoreID;

    LinearLayout ll_addresslayout,ll_myprofilelayout;

    public HashMap<String,String> StoreData;
    public HashMap<String,String> UserAddress;
    String Areaname,CityName;

    private Animation animUp;
    private Animation animDown;

    public ImageView img_homeedit,img_homedelete,img_officeedit,img_officedelete,img_othersedit,img_othersdelete;
    Dialog dialog  ;
    TextView txt_no,txt_yes,txt_text;
    private ArrayList<AddFavouriteDO> arrAddFavouriteDO;
    private ArrayList<EditAddressDO> arrEditAddressDO;
    private ArrayList<UpdateNameDO> arrUpdateEmail;
    String Message, Email;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        mContext=MyProfile.this;
        preferenceUtils=new PreferenceUtils(mContext);
        baseInitializeControls();

        if(mtoolbar!=null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_myprofile);
        fonttype = new FontType(mContext, root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        animUp = AnimationUtils.loadAnimation(this, R.anim.anim_up);
        animDown = AnimationUtils.loadAnimation(this, R.anim.anim_down);

        img_normal_actionbar.setVisibility(View.VISIBLE);
        img_normal_actionbar.setImageResource(R.mipmap.filter);
        img_back.setImageResource(R.mipmap.back_arrow);
        img_favourite.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        fl_cart.setVisibility(View.GONE);

        txt_normal_actionbar.setText("My Profile");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        edt_email.setEnabled(false);
        edt_customername.setEnabled(false);


        chk_addHomeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_myprofilelayout.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Home Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey = "Home";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);

                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Home Address");
            }
        });

        chk_addofficeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_myprofilelayout.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Office Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey = "Office";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);

                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Office Address");
            }
        });

        chk_addothersAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_addresslayout.startAnimation(animUp);
                ll_addresslayout.setVisibility(View.VISIBLE);
                ll_myprofilelayout.setVisibility(View.GONE);
                img_favourite.setVisibility(View.VISIBLE);
                img_favourite.setImageResource(R.mipmap.x_icon);
                txt_normal_actionbar.setText("Add Address");
                txt_addresstype.setText("Others Address");
                img_back.setVisibility(View.GONE);
                img_backfilter.setVisibility(View.VISIBLE);
                addressKey = "Others";
                edt_areaname.setText(Areaname);
                edt_city.setText(CityName);


                edt_pincode.setText("");
                edt_landmark.setText("");
                edt_address.setText("");
                edt_housenumber.setText("");
                btn_SaveAddress.setText("");

                edt_pincode.setHint("Type  pincode");
                edt_landmark.setHint("*Type landmark");
                edt_address.setHint("*Type address");
                edt_housenumber.setHint("*Type house number");
                btn_SaveAddress.setHint("Save Others Address");
            }
        });

        btn_SaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                HouseNumber=edt_housenumber.getText().toString();
                Address=edt_address.getText().toString();
                Landmark=edt_landmark.getText().toString();
                City=edt_city.getText().toString();
                Pincode=edt_pincode.getText().toString();
                AreaName=edt_areaname.getText().toString();

                StoreData=preferenceUtils.getStoreId();
                String SavedStoreID;
                SavedStoreID=StoreData.get(AppConstants.KEY_STORE_ID);
                LogUtils.error(MyProfile.class.getSimpleName(), "SavedStoreID=" + SavedStoreID);

                checkAddresskeyvalidations(HouseNumber, Address, Landmark, City, Pincode, AreaName, SavedStoreID);
            }
        });

        img_backfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                closeAddressLayout();

            }
        });

        img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                closeAddressLayout();
            }
        });


        edt_areaname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("You can change your locality only from dashboard", "OK");
            }
        });

        edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog("You can change your locality only from dashboard", "OK");
            }
        });

        img_homedelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(HomeAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Home address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showDialogRemove(mContext, 700, 450, HomeAddressId, "Home Address");
                }

            }
        });

        img_officedelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(OfficeAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Office address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showDialogRemove(mContext, 700, 450, OfficeAddressId, "Office Address");
                }
            }
        });

        img_othersdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(OtherAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Others address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showDialogRemove(mContext, 700, 450, OtherAddressId, "Others Address");
                }
            }
        });


        img_homeedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(HomeAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Home address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    OpenAddressLayout("Home Address", "Home");
                    for(int i=0;i<arrAddressDO.size();i++)
                    {
                        addressDO=arrAddressDO.get(i);
                        switch (addressDO.AddressType)
                        {
                            case "Home":
                                displayHomeEditAddress(addressDO);
                                break;
                        }
                    }
                }
            }
        });

        img_officeedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(OfficeAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Office address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    OpenAddressLayout("Office Address", "Office");
                    for(int i=0;i<arrAddressDO.size();i++)
                    {
                        addressDO=arrAddressDO.get(i);
                        switch (addressDO.AddressType)
                        {
                            case "Office":
                                displayOfficeEditAddress(addressDO);
                                break;

                        }
                    }
                }
            }
        });

        img_othersedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(OtherAddressId.equalsIgnoreCase("null"))
                {
                    Toast.makeText(mContext,"Others address not added",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    OpenAddressLayout("Others Address","Others");
                    for(int i=0;i<arrAddressDO.size();i++)
                    {
                        addressDO=arrAddressDO.get(i);
                        switch (addressDO.AddressType)
                        {
                            case "Others":
                                displayOthersEditAddress(addressDO);
                                break;
                        }
                    }
                }
            }
        });

        btn_editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String housenumber,address,landmark,city,pincode,areaname;

                housenumber=edt_housenumber.getText().toString();
                address=edt_address.getText().toString();
                landmark=edt_landmark.getText().toString();
                city=edt_city.getText().toString();
                pincode=edt_pincode.getText().toString();
                areaname=edt_areaname.getText().toString();

                checkEditAddresskeyvalidations(housenumber, address, landmark, city, pincode, areaname, AddressId);

                closeAddressLayout();

               // Toast.makeText(mContext,"Address edited",Toast.LENGTH_SHORT).show();

            }
        });

        tv_save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Email=edt_email.getText().toString();
                EditedCustomerName=edt_customername.getText().toString();

                if(Email.equals("") || Email.length()<0 || Email.isEmpty())
                {
                    Toast.makeText(mContext,"Email cannot be left blank",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    updateEmailAtServer(CustomerID,Email,EditedCustomerName);
                }
            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edt_customername.setEnabled(true);
                edt_email.setEnabled(true);
                tv_edit.setVisibility(View.GONE);
                edt_email.setBackgroundResource(R.drawable.edt_bordergray);
                edt_customername.setBackgroundResource(R.drawable.edt_bordergray);
                openKeyBoard(edt_email);
            }
        });
    }



    private void OpenAddressLayout(String AddressType, String addressKey)
    {
        ll_addresslayout.startAnimation(animUp);
        ll_addresslayout.setVisibility(View.VISIBLE);
        ll_myprofilelayout.setVisibility(View.GONE);
        img_favourite.setVisibility(View.VISIBLE);
        img_favourite.setImageResource(R.mipmap.x_icon);
        txt_normal_actionbar.setText("Add Address");
        txt_addresstype.setText(AddressType);
        img_back.setVisibility(View.GONE);
        img_backfilter.setVisibility(View.VISIBLE);
        addressKey = addressKey;
        btn_editAddress.setVisibility(View.VISIBLE);
        btn_SaveAddress.setVisibility(View.GONE);

    }

    public void showDialogRemove(Context context, int x, int y, final String AddressId,String addressType)
    {
        dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cart_dialogremove);
        dialog.setCanceledOnTouchOutside(false);

        txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
        txt_no= (TextView) dialog.findViewById(R.id.txt_no);
        txt_text= (TextView) dialog.findViewById(R.id.txt_text);


        ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
        fonttype = new FontType(context, root);

        txt_text.setText(addressType + " ?");

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteTheExistingAddress(AddressId);
                dialog.dismiss();

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        dialog.show();
    }


    private void closeAddressLayout() {

        img_favourite.setVisibility(View.INVISIBLE);
        img_normal_actionbar.setVisibility(View.VISIBLE);
        txt_normal_actionbar.setText("My Profile");
        ll_addresslayout.startAnimation(animDown);
        ll_addresslayout.setVisibility(View.GONE);
        ll_myprofilelayout.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);
        img_backfilter.setVisibility(View.GONE);
        btn_editAddress.setVisibility(View.GONE);
        btn_SaveAddress.setVisibility(View.VISIBLE);

    }

    private void checkAddresskeyvalidations(String houseNumber, String address, String landmark, String city, String pincode,String areaName,String storeId)
    {

        if(!houseNumber.equalsIgnoreCase("") && !houseNumber.isEmpty())
        {
            if(!address.equalsIgnoreCase("") && !address.isEmpty())
            {
                if(!landmark.equalsIgnoreCase("") && !landmark.isEmpty())
                {
                    if(!city.equalsIgnoreCase("") && !city.isEmpty())
                    {
                        SaveAddressDetailsToServer(CustomerID,houseNumber,address,landmark,city,pincode,areaName,storeId);
                    }
                    else
                    {
                        showAlertDialog("Please enter city name","OK");
                    }
                }
                else
                {
                    showAlertDialog("Please enter landmark","OK");
                }
            }
            else
            {
                showAlertDialog("Please enter complete address","OK");
            }

        }
        else
        {
            showAlertDialog("Please enter house number","OK");
        }

    }

    private void checkEditAddresskeyvalidations(String houseNumber, String address, String landmark, String city, String pincode,String areaName,String addressId)
    {

        if(!houseNumber.equalsIgnoreCase("") && !houseNumber.isEmpty())
        {
            if(!address.equalsIgnoreCase("") && !address.isEmpty())
            {
                if(!landmark.equalsIgnoreCase("") && !landmark.isEmpty())
                {
                    if(!city.equalsIgnoreCase("") && !city.isEmpty())
                    {
                        SaveEditedAddressDetailsToServer(houseNumber, address, landmark, city, pincode, areaName, addressId);
                    }
                    else
                    {
                        showAlertDialog("Please enter city name","OK");
                    }
                }
                else
                {
                    showAlertDialog("Please enter landmark","OK");
                }
            }
            else
            {
                showAlertDialog("Please enter complete address","OK");
            }

        }
        else
        {
            showAlertDialog("Please enter house number","OK");
        }

    }

    private void SaveEditedAddressDetailsToServer(String houseNumber, String address, String landmark,
                                            String city, String pincode,String AreaName,String addressId)
    {
        String queryParam=addressId;
        showLoaderNew();
        if(new CommonBL(mContext, MyProfile.this).SaveEditedAddressDetailsToServer(houseNumber, address, landmark, pincode, AreaName, queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void updateEmailAtServer(String customerID, String email,String EditedCustomerName) {

        String queryParam=customerID+"?access_token="+AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, MyProfile.this).updateEmailAtServer(email, queryParam,EditedCustomerName))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void SaveAddressDetailsToServer(String customerID, String houseNumber, String address, String landmark,
                                            String city, String pincode,String AreaName,String storeId)
    {
        String queryParam=CustomerID+"/addresses?access_token="+AccessToken;
        showLoaderNew();
        if(new CommonBL(mContext, MyProfile.this).saveAddressDetailsToServer(customerID, houseNumber, address, landmark,
                city, pincode, AreaName, storeId, addressKey, queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }



    private void deleteTheExistingAddress(String addressId) {

        String queryParam=addressId;
        showLoaderNew();
        if(new CommonBL(mContext, MyProfile.this).deleteTheExistingAddress(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }



    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("My Profile Screen");
        if(preferenceUtils.isLoggedIn())
        {
            Userdata = preferenceUtils.getUserDetails();
            // CustomerID,CustomerName,CustomerContact,AccessToken
            CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
            CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
            CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
            CustomerEmail= Userdata.get(AppConstants.KEY_CUSTOMER_EMAIL);
            AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

            LogUtils.error(MyProfile.class.getSimpleName(), "CustomerID=" + CustomerID);
            LogUtils.error(MyProfile.class.getSimpleName(), "CustomerName=" + CustomerName);
            LogUtils.error(MyProfile.class.getSimpleName(), "CustomerContact=" + CustomerContact);
            LogUtils.error(MyProfile.class.getSimpleName(), "AccessToken=" + AccessToken);
            LogUtils.error(MyProfile.class.getSimpleName(), "CustomerEmail=" + CustomerEmail);

            UserAddress=preferenceUtils.getUserAddress();
            Areaname= UserAddress.get(AppConstants.KEY_SUB_AREA_NAME);
            CityName=UserAddress.get(AppConstants.KEY_CITY_NAME);

            edt_customername.setText(CustomerName);
            tv_customerMobile.setText(CustomerContact);
            edt_email.setText(CustomerEmail);
        }
        getAddressDetails();
    }

    private void getAddressDetails()
    {
        JSONObject jsonObject=new JSONObject();
        //{"where":{"categoryId":"56cee6e8c4b8f7197ced15cf"}}
        JSONObject jObject=new JSONObject();
        try {
            jObject.put("customerId",CustomerID);
            jsonObject.put("where",jObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String queryParam=jsonObject.toString();
        showLoaderNew();
        if(new CommonBL(mContext, MyProfile.this).getAddressDetails(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }

    }

    private void baseInitializeControls()
    {
        mtoolbar= (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar=(TextView)findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar=(ImageView)findViewById(R.id.img_normal_actionbar);
        img_resetFilter=(ImageView)findViewById(R.id.img_resetFilter);
        img_favourite=(ImageView)findViewById(R.id.img_favourite);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_backfilter=(ImageView)findViewById(R.id.img_backfilter);
        fl_cart=(FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter=(FrameLayout) findViewById(R.id.fl_Filter);

        edt_customername=(EditText) findViewById(R.id.edt_customername);
        tv_customerMobile=(TextView) findViewById(R.id.tv_customerMobile);
        txt_addresstype=(TextView) findViewById(R.id.txt_addresstype);

        txt_homeAddress=(TextView) findViewById(R.id.txt_homeAddress);
        txt_officeAddress=(TextView) findViewById(R.id.txt_officeAddress);
        txt_othersAddress=(TextView) findViewById(R.id.txt_othersAddress);

        chk_addHomeAddress=(CheckBox) findViewById(R.id.chk_addHomeAddress);
        chk_addofficeAddress=(CheckBox) findViewById(R.id.chk_addofficeAddress);
        chk_addothersAddress=(CheckBox) findViewById(R.id.chk_addothersAddress);

        ll_addresslayout=(LinearLayout)findViewById(R.id.ll_addresslayout);
        ll_myprofilelayout=(LinearLayout)findViewById(R.id.ll_myprofilelayout);

        ll_addresslayout.bringToFront();

        edt_housenumber=(EditText) findViewById(R.id.edt_housenumber);
        edt_address=(EditText) findViewById(R.id.edt_address);
        edt_landmark=(EditText) findViewById(R.id.edt_landmark);
        edt_city=(EditText) findViewById(R.id.edt_city);
        edt_pincode=(EditText) findViewById(R.id.edt_pincode);
        edt_areaname=(EditText) findViewById(R.id.edt_areaname);

        btn_SaveAddress=(Button) findViewById(R.id.btn_SaveAddress);
        btn_editAddress=(Button) findViewById(R.id.btn_editAddress);

        img_homeedit=(ImageView)findViewById(R.id.img_homeedit);
        img_homedelete=(ImageView)findViewById(R.id.img_homedelete);
        img_officeedit=(ImageView)findViewById(R.id.img_officeedit);
        img_officedelete=(ImageView)findViewById(R.id.img_officedelete);
        img_othersedit=(ImageView)findViewById(R.id.img_othersedit);
        img_othersdelete=(ImageView)findViewById(R.id.img_othersdelete);


        edt_email=(EditText) findViewById(R.id.edt_email);
        tv_save=(TextView) findViewById(R.id.tv_save);
        tv_edit=(TextView) findViewById(R.id.tv_edit);

    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_SAVE_ADDRESS_DETAILS)
        {
            if(!data.isError)
            {
                arrSaveAddressDO = (ArrayList<SaveAddressDO>) data.data;
                if(arrSaveAddressDO!=null&&arrSaveAddressDO.size()>0)
                {
                    getAddressDetails();
                    closeAddressLayout();
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_DELETE_EXISTING_ADDRESS)
        {
            if(!data.isError)
            {
                arrAddFavouriteDO = (ArrayList<AddFavouriteDO>) data.data;
                if(arrAddFavouriteDO!=null&&arrAddFavouriteDO.size()>0)
                {

                    getAddressDetails();
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_EDIT_ADDRESS_DETAILS)
        {
            if(!data.isError)
            {
                arrEditAddressDO = (ArrayList<EditAddressDO>) data.data;
                if(arrEditAddressDO!=null&&arrEditAddressDO.size()>0)
                {
                    getAddressDetails();
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_GET_ADDRESS_DETAILS)
        {
            if(!data.isError)
            {
               // arrAddressDO= new ArrayList<AddressDO>();
                hideVisibilityAddressLayouts();
                arrAddressDO = (ArrayList<AddressDO>) data.data;
                if(arrAddressDO!=null&&arrAddressDO.size()>0)
                {
                    for(int i=0;i<arrAddressDO.size();i++)
                    {
                        AddressFlag=arrAddressDO.get(i).Status;
                    }

                    if(AddressFlag.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrAddressDO.size();i++)
                        {
                            addressDO=arrAddressDO.get(i);
                            switch (addressDO.AddressType)
                            {
                                case "Home":
                                    displayHomeAddress(addressDO);
                                    break;

                                case "Office":
                                    displayOfficeAddress(addressDO);
                                    break;

                                case "Others":
                                    displayOthersAddress(addressDO);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        hideVisibilityAddressLayouts();
                    }
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_UPDATE_EMAIL)
        {
            if(!data.isError)
            {
                arrUpdateEmail = (ArrayList<UpdateNameDO>) data.data;
                if(arrUpdateEmail!=null&&arrUpdateEmail.size()>0)
                {
                    for(int i=0;i<arrUpdateEmail.size();i++)
                    {
                        Message=arrUpdateEmail.get(i).Status;
                    }

                    if (Message.equalsIgnoreCase("success"))
                    {

                        edt_email.setEnabled(false);
                        edt_customername.setEnabled(false);
                        tv_edit.setVisibility(View.VISIBLE);
                        edt_email.setBackgroundResource(R.color.transparent);
                        edt_customername.setBackgroundResource(R.color.transparent);
                        hideKeyBoard(edt_email);
                        preferenceUtils.updateEmail(Email,EditedCustomerName);
                    }
                }
            }
        }

    }

    private void hideVisibilityAddressLayouts() {

        HomeAddressId="null";OfficeAddressId="null";OtherAddressId="null";

        chk_addHomeAddress.setVisibility(View.VISIBLE);
        txt_homeAddress.setVisibility(View.INVISIBLE);

        chk_addofficeAddress.setVisibility(View.VISIBLE);
        txt_officeAddress.setVisibility(View.INVISIBLE);

        chk_addothersAddress.setVisibility(View.VISIBLE);
        txt_othersAddress.setVisibility(View.INVISIBLE);
    }

    private void displayOthersAddress(AddressDO addressDO) {

            OtherAddressId=addressDO.Addressid;
            OthersStoreID=addressDO.StoreId;
            LogUtils.error(MyProfile.class.getSimpleName(), "OtherAddressId="+OtherAddressId);
            strOthersAddress=new StringBuilder();
            strOthersAddress.append(addressDO.HouseNumber);
            strOthersAddress.append(",");
            strOthersAddress.append(addressDO.Areaname);
            strOthersAddress.append(",");
            strOthersAddress.append(addressDO.Address);
            strOthersAddress.append(",");
            strOthersAddress.append(addressDO.City);
            strOthersAddress.append(",");
            strOthersAddress.append(addressDO.Pincode);
            strOthersAddress.append(",");
            strOthersAddress.append(addressDO.Landmark);

            chk_addothersAddress.setVisibility(View.INVISIBLE);
            txt_othersAddress.setVisibility(View.VISIBLE);
            txt_othersAddress.setText(strOthersAddress.toString());

    }

    private void displayOfficeAddress(AddressDO addressDO) {

            OfficeAddressId=addressDO.Addressid;
            OfficeStoreID=addressDO.StoreId;
            LogUtils.error(MyProfile.class.getSimpleName(), "OfficeAddressId="+OfficeAddressId);
            strOfficeAddress=new StringBuilder();
            strOfficeAddress.append(addressDO.HouseNumber);
            strOfficeAddress.append(",");
            strOfficeAddress.append(addressDO.Areaname);
            strOfficeAddress.append(",");
            strOfficeAddress.append(addressDO.Address);
            strOfficeAddress.append(",");
            strOfficeAddress.append(addressDO.City);
            strOfficeAddress.append(",");
            strOfficeAddress.append(addressDO.Pincode);
            strOfficeAddress.append(",");
            strOfficeAddress.append(addressDO.Landmark);

            chk_addofficeAddress.setVisibility(View.INVISIBLE);
            txt_officeAddress.setVisibility(View.VISIBLE);
            txt_officeAddress.setText(strOfficeAddress.toString());

    }

    private void displayHomeAddress(AddressDO addressDO) {
            HomeAddressId=addressDO.Addressid;
            HomeStoreID=addressDO.StoreId;
            LogUtils.error(MyProfile.class.getSimpleName(), "HomeAddressId=" + HomeAddressId);
            strHomeAddress=new StringBuilder();
            strHomeAddress.append(addressDO.HouseNumber);
            strHomeAddress.append(",");
            strHomeAddress.append(addressDO.Areaname);
            strHomeAddress.append(",");
            strHomeAddress.append(addressDO.Address);
            strHomeAddress.append(",");
            strHomeAddress.append(addressDO.City);
            strHomeAddress.append(",");
            strHomeAddress.append(addressDO.Pincode);
            strHomeAddress.append(",");
            strHomeAddress.append(addressDO.Landmark);

            chk_addHomeAddress.setVisibility(View.INVISIBLE);
            txt_homeAddress.setVisibility(View.VISIBLE);
            txt_homeAddress.setText(strHomeAddress.toString());
    }


    private void displayHomeEditAddress(AddressDO addressDO)
    {
        HomeAddressId=addressDO.Addressid;
        AddressId=HomeAddressId;
        HomeStoreID=addressDO.StoreId;
        edt_housenumber.setText(addressDO.HouseNumber);
        edt_areaname.setText(addressDO.Areaname);
        edt_address.setText(addressDO.Address);
        edt_city.setText(addressDO.City);
        edt_pincode.setText(addressDO.Pincode);
        edt_landmark.setText(addressDO.Landmark);
    }

    private void displayOfficeEditAddress(AddressDO addressDO) {

        OfficeAddressId=addressDO.Addressid;
        AddressId=OfficeAddressId;
        OfficeStoreID=addressDO.StoreId;
        edt_housenumber.setText(addressDO.HouseNumber);
        edt_areaname.setText(addressDO.Areaname);
        edt_address.setText(addressDO.Address);
        edt_city.setText(addressDO.City);
        edt_pincode.setText(addressDO.Pincode);
        edt_landmark.setText(addressDO.Landmark);

    }

    private void displayOthersEditAddress(AddressDO addressDO) {

        OtherAddressId=addressDO.Addressid;
        AddressId=OtherAddressId;
        OthersStoreID=addressDO.StoreId;
        edt_housenumber.setText(addressDO.HouseNumber);
        edt_areaname.setText(addressDO.Areaname);
        edt_address.setText(addressDO.Address);
        edt_city.setText(addressDO.City);
        edt_pincode.setText(addressDO.Pincode);
        edt_landmark.setText(addressDO.Landmark);

    }
}
