package com.nm.cremeville.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.adapter.OrderedItemsAdapter;
import com.nm.cremeville.businesslayer.CommonBL;
import com.nm.cremeville.businesslayer.DataListener;
import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.LoyaltyDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;
import com.nm.cremeville.webaccess.Response;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReceiptActivity extends BaseNew implements DataListener {

    Context mContext;
    public Toolbar mtoolbar;
    public TextView txt_normal_actionbar;
    public ImageView img_normal_actionbar, img_back;
    PreferenceUtils preferenceUtils;
    ImageView img_favourite;
    FrameLayout fl_cart, fl_Filter;
    String OrderId;
    LinearLayout ll_coupon;
    DecimalFormat df = new DecimalFormat("00.00######");
    public HashMap<String, String> Userdata;
    String CustomerID, CustomerName, CustomerContact, AccessToken;
    ListView lst_orderedItems;
    TextView txt_address,txt_grandtotal,txt_back,txt_couponCode,txt_couponamount;
    ArrayList<GetOrderDO>  arrgetOrderDO;
    ArrayList<LoyaltyDO>  arrLoyaltyDO;
    ArrayList<ProductDO> productList;
    OrderedItemsAdapter orderedItemsAdapter;
    FontType fonttype;
    String Message,totalloyaltypoint,loyaltyamount;
    public View footer_view;
    String  CouponStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receipt);
        mContext = ReceiptActivity.this;
        preferenceUtils = new PreferenceUtils(mContext);
        CartDatabase.init(this);
        initializeControls();

        lst_orderedItems.addFooterView(footer_view, null, false);

        if (mtoolbar != null)
        {
            setSupportActionBar(mtoolbar);
        }

        ViewGroup root = (ViewGroup)findViewById(R.id.ll_receipt);
        fonttype = new FontType(mContext, root);

        productList=new ArrayList<>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txt_normal_actionbar.setText("Receipt");

        fl_cart.setVisibility(View.GONE);
        fl_Filter.setVisibility(View.GONE);
        img_favourite.setVisibility(View.GONE);

        OrderId = getIntent().getExtras().getString("OrderId");
        CartDatabase.clearDatabase();

        getCurrentOrder(OrderId);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
            }
        });

        txt_normal_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
            }
        });

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Dashboard.class);
                startActivity(intent);
            }
        });

    }

    private void getCurrentOrder(String orderId)
    {
        String queryParam=orderId;
        showLoaderNew();
        if(new CommonBL(mContext, ReceiptActivity.this).getCurrentOrder(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    private void getCurrentLoyaltyPoints(String customerID)
    {
        //URL http://104.196.99.177:4848/api/Customers/56f64821d37cab220c5d685f
        String queryParam=customerID;
        showLoaderNew();
        if(new CommonBL(mContext, ReceiptActivity.this).getCurrentLoyaltyPoints(queryParam))
        {

        }
        else
        {
            hideloader();
            showAlertDialog("NO INTERNET","OK");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ReceiptActivity.this, Dashboard.class);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Receipt Screen");
        Userdata = preferenceUtils.getUserDetails();
        // CustomerID,CustomerName,CustomerContact,AccessToken
        CustomerID = Userdata.get(AppConstants.KEY_CUSTOMER_ID);
        CustomerName = Userdata.get(AppConstants.KEY_CUSTOMER_NAME);
        CustomerContact = Userdata.get(AppConstants.KEY_CUSTOMER_CONTACT);
        AccessToken = Userdata.get(AppConstants.KEY_ACCESS_TOKEN);

        LogUtils.error(ReceiptActivity.class.getSimpleName(), "CustomerID=" + CustomerID);
        LogUtils.error(ReceiptActivity.class.getSimpleName(), "CustomerName=" + CustomerName);
        LogUtils.error(ReceiptActivity.class.getSimpleName(), "CustomerContact=" + CustomerContact);
        LogUtils.error(ReceiptActivity.class.getSimpleName(), "AccessToken=" + AccessToken);

    }

    private void initializeControls() {
        mtoolbar = (Toolbar) findViewById(R.id.normal_toolbar);
        txt_normal_actionbar = (TextView) findViewById(R.id.txt_normal_actionbar);
        img_normal_actionbar = (ImageView) findViewById(R.id.img_normal_actionbar);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_favourite = (ImageView) findViewById(R.id.img_favourite);
        fl_cart = (FrameLayout) findViewById(R.id.fl_cart);
        fl_Filter = (FrameLayout) findViewById(R.id.fl_Filter);

        txt_address = (TextView)findViewById(R.id.txt_address);
        txt_back = (TextView)findViewById(R.id.txt_back);
        lst_orderedItems = (ListView)findViewById(R.id.lst_orderedItems);
        footer_view=getLayoutInflater().inflate(R.layout.footer_receipt,null);

        ll_coupon=(LinearLayout) footer_view.findViewById(R.id.ll_coupon);
        txt_grandtotal=(TextView)footer_view.findViewById(R.id.txt_grandtotal);
        txt_couponCode=(TextView)footer_view.findViewById(R.id.txt_couponCode);
        txt_couponamount=(TextView)footer_view.findViewById(R.id.txt_couponamount);
    }

    @Override
    public void dataRetreived(Response data) {

        hideloader();
        if (data != null && data.method == ServiceMethods.WS_GET_CURRENT_ORDER) {
            if (!data.isError) {
                arrgetOrderDO = (ArrayList<GetOrderDO>) data.data;
                if(arrgetOrderDO!=null&&arrgetOrderDO.size()>0)
                {
                    String Address= arrgetOrderDO.get(0).Address;
                    String grandTotal=arrgetOrderDO.get(0).Grandtotal;
                    productList=arrgetOrderDO.get(0).productList;
                    CouponStatus=arrgetOrderDO.get(0).CouponStatus;

                    txt_address.setText(Address);
                    txt_grandtotal.setText("₹ "+grandTotal);

                    Map<String, Object> receiptScreen = new HashMap<String, Object>();
                    receiptScreen.put(AFInAppEventParameterName.LEVEL,10);
                    receiptScreen.put(AFInAppEventParameterName.SCORE,100);
                    receiptScreen.put(AFInAppEventParameterName.REVENUE,grandTotal);
                    AppsFlyerLib.getInstance().trackEvent(mContext, AFInAppEventType.PURCHASE,receiptScreen);

                    if(CouponStatus.equals("1"))
                    {
                        ll_coupon.setVisibility(View.VISIBLE);
                        txt_couponCode.setText(arrgetOrderDO.get(0).CouponCode);
                        txt_couponamount.setText("-₹ " +arrgetOrderDO.get(0).CouponAmount);
                    }
                    orderedItemsAdapter=new OrderedItemsAdapter(mContext,productList);
                    lst_orderedItems.setAdapter(orderedItemsAdapter);

                    getCurrentLoyaltyPoints(CustomerID);
                }
            }
        }

        else if (data != null && data.method == ServiceMethods.WS_GET_CURRENT_LOYALTY_POINTS) {
            if (!data.isError) {
                arrLoyaltyDO = (ArrayList<LoyaltyDO>) data.data;
                if(arrLoyaltyDO!=null&&arrLoyaltyDO.size()>0)
                {
                    for(int i=0;i<arrLoyaltyDO.size();i++)
                    {
                        Message=arrLoyaltyDO.get(i).Status;
                    }
                    if (Message.equalsIgnoreCase("success"))
                    {
                        for(int i=0;i<arrLoyaltyDO.size();i++)
                        {
                            totalloyaltypoint=arrLoyaltyDO.get(i).TotalLoyaltyPoint;
                            loyaltyamount=arrLoyaltyDO.get(i).LoyaltyAmount;
                        }
                        LogUtils.error(LoginActivity.class.getSimpleName(), "totalloyaltypoint="+totalloyaltypoint);
                        LogUtils.error(LoginActivity.class.getSimpleName(), "loyaltyamount="+loyaltyamount);
                        preferenceUtils.saveLoyaltyPoints(totalloyaltypoint,loyaltyamount);
                    }
                }
            }
        }
    }
}
