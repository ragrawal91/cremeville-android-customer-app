package com.nm.cremeville.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;


public class PreferenceUtils 
{
	private SharedPreferences preferences;
	private SharedPreferences.Editor edit;
	public static String KEY 			= 	"key";

	public PreferenceUtils(Context context) 
	{
		preferences		=	PreferenceManager.getDefaultSharedPreferences(context);
		edit			=	preferences.edit();
	}
	
	public void saveString(String strKey,String strValue)
	{
		edit.putString(strKey, strValue);
		edit.commit();
	}
	
	public void saveInt(String strKey,int value)
	{
		edit.putInt(strKey, value);
		edit.commit();
	}

	public String getStringFromPreference(String strKey,String defaultValue )
	{
		return preferences.getString(strKey, defaultValue);
	}
	


	public int getIntFromPreference(String strKey,int defaultValue)
	{
		return preferences.getInt(strKey, defaultValue);
	}
	


	public void createLoginSession(String CustomerId, String Password,String  CustomerPhone,String CustomerName,String CustomerEmail,String AccessToken)
	{
		edit.putString(AppConstants.KEY_CUSTOMER_ID,CustomerId);

		edit.putString(AppConstants.KEY_CUSTOMER_NAME,CustomerName);

		edit.putString(AppConstants.KEY_CUSTOMER_EMAIL,CustomerEmail);

		edit.putString(AppConstants.KEY_CUSTOMER_PASSWORD, Password);

		edit.putBoolean(AppConstants.KEY_IS_LOGIN, true);

		edit.putString(AppConstants.KEY_CUSTOMER_CONTACT, CustomerPhone);

		edit.putString(AppConstants.KEY_ACCESS_TOKEN, AccessToken);

		edit.commit();
	}

	public void saveCurrentAddress(String subareaname, String cityname)
	{
		edit.putString(AppConstants.KEY_SUB_AREA_NAME,subareaname);

		edit.putString(AppConstants.KEY_CITY_NAME, cityname);


		edit.commit();
	}

	public void saveCurrentLocation(String location_latitude, String location_longitude)
	{

		edit.putString(AppConstants.KEY_LATITUDE, location_latitude);

		edit.putString(AppConstants.KEY_LONGITUDE, location_longitude);

		edit.commit();
	}

	public void saveLoyaltyPoints(String totalLoyaltyPoint, String loyaltyAmount) {
		edit.putString(AppConstants.KEY_TOTAL_POINTS, totalLoyaltyPoint);
		edit.putString(AppConstants.KEY_LOYALTY_AMOUNT, loyaltyAmount);
		edit.commit();
	}

	public void createAccessToken(String accessToken)
	{
		edit.putString(AppConstants.KEY_ACCESS_TOKEN, accessToken);
	}

	public void saveStoreId(String StoreId)
	{
		edit.putString(AppConstants.KEY_STORE_ID, StoreId);
		edit.commit();
	}

	public HashMap<String, String> getStoreId()
	{
		HashMap<String, String> storedata = new HashMap<String, String>();
		// access Token
		storedata.put(AppConstants.KEY_STORE_ID, preferences.getString(AppConstants.KEY_STORE_ID, "0"));

		return storedata;
	}

	public void clearStoreId()
	{
		edit.putString(AppConstants.KEY_STORE_ID, "NA");
		edit.commit();
	}

	public void updateEmail(String Email,String CustomerName)
	{
		edit.putString(AppConstants.KEY_CUSTOMER_EMAIL, Email);
		edit.putString(AppConstants.KEY_CUSTOMER_NAME, CustomerName);
		edit.commit();
	}

	public void clearLoyaltyPoints()
	{
		edit.putString(AppConstants.KEY_TOTAL_POINTS, "0");
		edit.putString(AppConstants.KEY_LOYALTY_AMOUNT, "0");
		edit.commit();
	}

	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// user id
		user.put(AppConstants.KEY_CUSTOMER_ID, preferences.getString(AppConstants.KEY_CUSTOMER_ID, "0"));
		// user name
		user.put(AppConstants.KEY_CUSTOMER_NAME, preferences.getString(AppConstants.KEY_CUSTOMER_NAME, "NA"));
		// user password
		user.put(AppConstants.KEY_CUSTOMER_PASSWORD, preferences.getString(AppConstants.KEY_CUSTOMER_PASSWORD, "0000000000"));
		// user contact number
		user.put(AppConstants.KEY_CUSTOMER_CONTACT, preferences.getString(AppConstants.KEY_CUSTOMER_CONTACT, "NA"));
		// user email
		user.put(AppConstants.KEY_CUSTOMER_EMAIL, preferences.getString(AppConstants.KEY_CUSTOMER_EMAIL, "NA"));
		// user access token
		user.put(AppConstants.KEY_ACCESS_TOKEN, preferences.getString(AppConstants.KEY_ACCESS_TOKEN, "NA"));

		// return user
		return user;
	}

	public HashMap<String, String> getUserLocation(){
		HashMap<String, String> location = new HashMap<String, String>();
		// latitude
		location.put(AppConstants.KEY_LATITUDE, preferences.getString(AppConstants.KEY_LATITUDE, "Latitude"));
		// longitude
		location.put(AppConstants.KEY_LONGITUDE, preferences.getString(AppConstants.KEY_LONGITUDE, "Longitude"));

		return location;
	}

	public HashMap<String, String> getUserAddress(){
		HashMap<String, String> address = new HashMap<String, String>();
		// sub area name
		address.put(AppConstants.KEY_SUB_AREA_NAME, preferences.getString(AppConstants.KEY_SUB_AREA_NAME, "NA"));
		// city name
		address.put(AppConstants.KEY_CITY_NAME, preferences.getString(AppConstants.KEY_CITY_NAME, "NA"));


		return address;
	}

	public HashMap<String, String> getLoyaltyPoints(){
		HashMap<String, String> loyaltyPoint = new HashMap<String, String>();
		// user id
		loyaltyPoint.put(AppConstants.KEY_TOTAL_POINTS, preferences.getString(AppConstants.KEY_TOTAL_POINTS, "0"));
		// user name
		loyaltyPoint.put(AppConstants.KEY_LOYALTY_AMOUNT, preferences.getString(AppConstants.KEY_LOYALTY_AMOUNT, "0"));
		// user password
		// return user
		return loyaltyPoint;
	}

	public void logoutUser()
	{
		edit.clear();
		edit.commit();
		/*edit.putBoolean(AppConstants.KEY_IS_LOGIN, false);
		edit.commit();*/

	}

	public void saveLoginState(boolean b) {
		edit.putBoolean(AppConstants.KEY_LOGINSTATE, b);
		edit.commit();

	}

	public Boolean getLoginState()
	{
		return preferences.getBoolean(AppConstants.KEY_LOGINSTATE, false);
	}

	public HashMap<String, String> getAccessToken()
	{
		HashMap<String, String> user = new HashMap<String, String>();
		// access Token
		user.put(AppConstants.KEY_CUSTOMER_ID, preferences.getString(AppConstants.KEY_ACCESS_TOKEN, "0"));

		return user;
	}

	public String getthe_socialtype()
	{
		return preferences.getString(AppConstants.KEY_SOCIAL_TYPE, "NA");
	}

	public boolean isLoggedIn(){
		return preferences.getBoolean(AppConstants.KEY_IS_LOGIN, false);
	}



}
