package com.nm.cremeville.utilities;

import android.graphics.Typeface;

import java.net.HttpURLConnection;

import de.keyboardsurfer.android.widget.crouton.Configuration;


public class AppConstants
{
	//Device height & width
	public static int DEVICE_DISPLAY_WIDTH;
	public static int DEVICE_DISPLAY_HEIGHT;
	
	public static final int GET  = 1;
	public static final int POST = 2;
	public static final int PUT = 3;
	public static final int DELETE = 4;
	
	public static Typeface typeFace;

	public static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder().setDuration(Configuration.DURATION_INFINITE).build();

	public static final Configuration CONFIGURATION_LONG = new Configuration.Builder().setDuration(Configuration.DURATION_LONG).build();

	public static final Configuration CONFIGURATION_SHORT = new Configuration.Builder().setDuration(Configuration.DURATION_SHORT).build();

	public static String ContentTypeJson = "application/json";

	public static String ContentTypeXML = "application/xml";
	
	public static String ContentTypeForm = "application/x-www-form-urlencoded";
	
	public static String CHECK_NETWORK_CONN="Please check internet connection!!";
	
	public static String NO_RESPONSE="No Response from Server";

	public int  UNAUTHORISED= HttpURLConnection.HTTP_UNAUTHORIZED;

	public static String KEY_CUSTOMER_ID="";
	public static String KEY_CUSTOMER_NAME="customername";
	public static String KEY_CUSTOMER_EMAIL="customeremail";
	public static String KEY_CUSTOMER_PASSWORD="customerapssword";
	public static String KEY_CUSTOMER_CONTACT="contact";
	public static String KEY_ACCESS_TOKEN="accessToken";
	public static String KEY_IS_LOGIN = "IsLoggedIn";
	public static String KEY_SOCIAL_TYPE="socialtype";
	public static String KEY_TOTAL_POINTS="loyaltypoints";
	public static String KEY_LOYALTY_AMOUNT="loyaltyamount";
	public static String KEY_STORE_ID="storeId";
	public static String KEY_CURRENTDATE="currentdate";
	public static String KEY_LOGINSTATE="loginstate";
	public static String KEY_APPACCESSTOKENREFRESHSTATE="appaceestate";




	public static String KEY_SUB_AREA_NAME="subareaname";
	public static String KEY_CITY_NAME="cityname";
	public static String KEY_LATITUDE="latitude";
	public static String KEY_LONGITUDE="longitude";

	public static String KEY_USER_ADDRESS="userAddress";


}
