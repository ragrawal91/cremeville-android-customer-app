package com.nm.cremeville.utilities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GPSTracker extends Service implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,com.google.android.gms.location.LocationListener {

    private static final String TAG=GPSTracker.class.getSimpleName();

    private  Context mContext;

    //flag for GPS Status
    public boolean isGPSEnabled = false;

    //flag for network status
    public boolean isNetworkEnabled = false;

    public boolean canGetLocation = false;

    public static Location location;

    public double latitude;

    public double longitude;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //10 metters

    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    protected LocationManager locationManager;

    private GPSPreference gpsPreference;

    private LocationRequest mLocationRequest;

    private GoogleApiClient mGoogleApiClient;

    public GPSTracker()
    {

    }


    public GPSTracker(Context context)
    {
        this.mContext=context;

        gpsPreference = new GPSPreference(mContext);

        createLocationRequest();

        createGoogleApiClient();

        connectGoogleApiClient();

        startLocationUpdates();

        getLocation();
    }



    private void createGoogleApiClient() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(GPSConstants.INTERVAL);
        mLocationRequest.setFastestInterval(GPSConstants.FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        GPSLogutils.debug(TAG, "LocationRequest Created");
    }

    private void createLocationRequest() {

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        GPSLogutils.debug(TAG, "GoogleApi Client Created");

    }

    public void startLocationUpdates()
    {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            GPSLogutils.debug(TAG, "Location update started");
            return;
        }
    }

    public void stopLocationUpdates()
    {
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            GPSLogutils.debug(TAG, "Location update stopped ");

        }
    }

    public void connectGoogleApiClient()
    {
        if(mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
            GPSLogutils.debug(TAG, "GoogleApi Client connected");
        }
    }

    public void disConnectGoogleApiClient()
    {
        if(mGoogleApiClient != null)
        {
            mGoogleApiClient.disconnect();
            GPSLogutils.debug(TAG, "GoogleApi Client disConnected");
            stopSelf();
            GPSLogutils.debug(TAG, "GPSTrackerService stopped");

        }
    }

    public GoogleApiClient getGoogleApiClient()
    {
        return mGoogleApiClient;
    }

    public LatLng getLatLng()
    {
        String lattitude = gpsPreference.getStringFromPreference(GPSPreference.CURRENT_LOCATION_LATTITUDE, "0.0");
        String longitude = gpsPreference.getStringFromPreference(GPSPreference.CURRENT_LOCATION_LONGITUDE, "0.0");
        return new LatLng(Double.parseDouble(lattitude), Double.parseDouble(longitude));
    }

    /**
     * Method to get the location based on location provider for google map.
     * @return
     */
    public Location getLocation()
    {

        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

        // getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

       // LoggerUtils.debug(GPSTracker.class.getSimpleName(),"GPS ENABLED is"+isGPSEnabled);

        // getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {

            // no network provider is enabled
        }
        else
        {
            this.canGetLocation = true;
            // if Network Enabled get lat/long using GPS Services
            if (isNetworkEnabled) {

                try
                {
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        //location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    }
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }

                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    try
                    {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.

                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        }



                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }

                    }catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }

                }
            }
            else
            {
                //showGPSSettingsAlert();
            }


        }


        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            try
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    locationManager.removeUpdates(GPSTracker.this);
                    return;
                }


            }catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }

    /**
     * Start using GPS listner.
     * Calling this function will start using GPS in your app
     */

    public void startUsingGPS()
    {
        if(locationManager != null){
            try
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    return;
                }
            }catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */
    public void showGPSSettingsAlert(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

    }

    /**
     * Method to update the GPS cordinate based on the location.
     * @param location
     */
    public void updateGPSCoordinates(Location location)
    {
        if (location != null)
        {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    /**
     * Method the geocoder address.
     * @param context
     * @return
     */
    public List<Address> getGeocoderAddress(Context context)
    {

        if (location != null)
        {
            Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);

            try {

                return geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Method to get the address line according to index from Geocoder of google map.
     * @param context
     * @param index
     * @return
     */
    public String getAddressLine(Context context,int index)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(index);

            return address.getAddressLine(index);
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the Address line from Geocoder of google map.
     * @param context
     * @return
     */

    public String getAddressLine(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            String addressLine = null ;

            for (int i = 0; i < addresses.size(); i++) {
                addressLine += address.getAddressLine(0);
                if(i!=addresses.size()-1)
                    addressLine += ", ";
            }
            return addressLine;
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the address from Geocoder using the latitude and longitude.
     * @param context
     * @param latitude
     * @param longitude
     * @return
     */
    public static List<Address> getGeocoderAddress(Context context, double latitude, double longitude)
    {
        if (location != null)
        {
            Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
            try
            {
                return geocoder.getFromLocation(latitude, longitude, 1);
            }
            catch (IOException e)
            {
                Log.e("Error : Geocoder", "Impossible to connect to Geocoder", e);
            }
        }

        return null;
    }

    /**
     * Method to get the address line from the latitude and longitude.
     * @param context
     * @param latitude
     * @param longitude
     * @return
     */
    public static String getAddressLine(Context context, double latitude, double longitude)
    {
        StringBuilder addrs_sb=new StringBuilder();

        List<Address> addresses = getGeocoderAddress(context, latitude, longitude);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            String adddressline=address.getAddressLine(0);

            String sublocality=address.getSubLocality();

            String locality=address.getLocality();

            String state=address.getAdminArea();

            String postalcode=address.getPostalCode();

            String countryname=address.getCountryName();

            addrs_sb.append(adddressline);

            addrs_sb.append(",");

            addrs_sb.append(sublocality);

            addrs_sb.append(",");

            addrs_sb.append(locality);

            addrs_sb.append(",");

            addrs_sb.append(state);

            addrs_sb.append(",");

            addrs_sb.append(postalcode);

            addrs_sb.append(",");

            addrs_sb.append(countryname);

            return addrs_sb.toString();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method
     * @param context
     * @param latitude
     * @param longitude
     * @return
     */
    public static String getAreaPostalCode(Context context,double latitude, double longitude)
    {

        List<Address> addresses = getGeocoderAddress(context, latitude, longitude);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getPostalCode();
        }
        else
        {
            return "NA";
        }
    }

    public static String getCityName(Context context,double latitude,double longitude)
    {

        List<Address> addresses = getGeocoderAddress(context, latitude, longitude);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getLocality();
        }
        else {
            return "NA";
        }
    }

    /**
     * Method to get the locality name for the geocoder address.
     * @param context
     * @return
     */
    public String getLocality(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getLocality();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the postalcode/PinCode for the geocoder address.
     * @param context
     * @return
     */

    public String getPostalCode(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getPostalCode();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to sublocality name for the geocoder address.
     * @param context
     * @return
     */
    public String getSubLocality(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getSubLocality();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the state name for the geocoder address.
     * @param context
     * @return
     */

    public String getState(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getAdminArea();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the country name for the geocoder address.
     * @param context
     * @return
     */
    public String getCountryName(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getCountryName();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the country code for the geocoder address.
     * @param context
     * @return
     */
    public String getCountryCode(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);
        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getCountryCode();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to the feature place name for the geocoder address.
     * @param context
     * @return
     */
    public String getFeaturePlaceName(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getFeatureName();
        }
        else
        {
            return "NA";
        }

    }

    /**
     * Method to get the sunadmin area name for the geocoder address.
     * @param context
     * @return
     */
    public String getSubadminarea(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getSubAdminArea();
        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to get the max address line for the geocoder address.
     * @param context
     * @return
     */
    public int getMaxaddresslineindex(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            return address.getMaxAddressLineIndex();
        }
        else
        {
            return -1;
        }
    }

    /**
     * Method to get the street number for the geocoder address.
     * @param context
     * @return
     */

    public String getStreetNumber(Context context)
    {
        List<Address> addresses = getGeocoderAddress(context);

        if (addresses != null && addresses.size() > 0)
        {
            Address address = addresses.get(0);

            if(address.getSubThoroughfare()==null)
            {
                return "NA";
            }
            else {
                return address.getSubThoroughfare();
            }


        }
        else
        {
            return "NA";
        }
    }

    /**
     * Method to provide the complete address for the requested location.
     * @param context
     * @return
     */
    public String getMapAddress(Context context)
    {
        StringBuilder addsb=new StringBuilder();

        addsb.append(getAddressLine(context, 0));

        addsb.append(",");

        addsb.append(getSubLocality(context));

        addsb.append(",");

        addsb.append(getLocality(context));

        addsb.append(",");

        addsb.append(getState(context));

        addsb.append("-");

        addsb.append(getPostalCode(context));

        addsb.append(",");

        addsb.append(getCountryName(context));

        return addsb.toString();

    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

        updateGPSCoordinates(location);

        //Location Listener.
        GPSLogutils.error(TAG, "Firing onLocationChanged Callback");
        GPSTracker.location = location;
        String mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        if (GPSTracker.location != null)
        {
            String lat = String.valueOf(GPSTracker.location.getLatitude());
            String lng = String.valueOf(GPSTracker.location.getLongitude());

            gpsPreference.saveString(GPSPreference.CURRENT_LOCATION_LATTITUDE, lat);
            gpsPreference.saveString(GPSPreference.CURRENT_LOCATION_LONGITUDE, lng);

            /*GPSLogutils.error(TAG, "At Time: " + mLastUpdateTime + "\n" +
                    "Latitude: " + lat + "\n" +
                    "Longitude: " + lng + "\n" +
                    "Accuracy: " + mCurrentLocation.getAccuracy() + "\n" +
                    "Provider: " + mCurrentLocation.getProvider()+""
                    + "\n===================================================");*/

//            Toast.makeText(mContext, "At Time: " + mLastUpdateTime + "\n" +
//                    "Latitude: " + lat + "\n" +
//                    "Longitude: " + lng + "\n" +
//                    "Accuracy: " + mCurrentLocation.getAccuracy() + "\n" +
//                    "Provider: " + mCurrentLocation.getProvider(), Toast.LENGTH_LONG).show();
        }
        else
        {
            GPSLogutils.error(TAG, "location is null ");

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(Bundle bundle) {

        GPSLogutils.error(TAG, "onConnected - isConnected : " + mGoogleApiClient.isConnected());
        startLocationUpdates();


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        GPSLogutils.error(TAG, "Connection failed: " + connectionResult.toString());
    }


}
