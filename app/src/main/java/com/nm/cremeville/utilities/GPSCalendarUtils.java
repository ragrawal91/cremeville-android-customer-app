package com.nm.cremeville.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;

public class GPSCalendarUtils 
{
   
	public static final String DATE_STD_PATTERN  = "yyyy-MM-dd HH:mm a";
	public static final String DATE_STD_PATTERN2 = "dd-MM-yyyy HH:mm ";

	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDate() 
	{
		 String sdf = new SimpleDateFormat(DATE_STD_PATTERN).format(System.currentTimeMillis());
		return sdf;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getCurrentDateForLogs() 
	{
		String sdf = new SimpleDateFormat(DATE_STD_PATTERN2).format(System.currentTimeMillis());
		return sdf;
	}
	
}
