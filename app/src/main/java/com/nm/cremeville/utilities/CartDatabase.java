package com.nm.cremeville.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;

import java.util.ArrayList;

public class CartDatabase {
	public static final boolean DEBUG = true;

	/******************** Logcat TAG ************/
	public static final String LOG_TAG = "CARTDATABASE";

	/********************Cartlist Table Fields ************/
	public static final String Column_itemid = "itemid";
	public static final String Column_itemname = "itemname";
	public static final String Column_itemqty = "itemqty";
	public static final String Column_itmprice = "itmprice";
	public static final String Column_itemimgurl = "itemimgurl";
	public static final String Column_itemdesc= "itemdesc";
	public static final String Column_itemadded= "itemadded";
	public static final String Column_itemsize= "itemsize";



	public static final String Column_coupon_itemid = "itemid";
	public static final String Column_coupon_itemname = "itemname";
	public static final String Column_coupon_itemqty = "itemqty";
	public static final String Column_coupon_itmprice = "itmprice";
	public static final String Column_coupon_itemimgurl = "itemimgurl";
	public static final String Column_coupon_itemdesc= "itemdesc";
	public static final String Column_coupon_itemadded= "itemadded";
	public static final String Column_coupon_itemsize= "itemsize";




	/******************** Database Name ************/
	public static final String DATABASE_NAME = "cart";

	/******************** Database Version (Increase one if want to also upgrade your database) ************/
	public static final int DATABASE_VERSION = 1;// started at 1

	/** Table names */
	public static final String Table_Cartlist = "cartlist";
	public static final String Table_CouponCartlist = "couponcartlist";

	/******************** Set all table with comma seperated like USER_TABLE,ABC_TABLE ************/
	private static final String[] ALL_TABLES = { Table_Cartlist,Table_CouponCartlist};

	/** Create table syntax */
	private static final String USER_CREATECARTLIST = "create table cartlist(itemid TEXT,itemname TEXT,itemqty INTEGER,itmprice TEXT,itemimgurl TEXT,itemdesc TEXT,itemadded TEXT,itemsize TEXT);";

	private static final String USER_CREATECOUPONCARTLIST = "create table couponcartlist(itemid TEXT,itemname TEXT,itemqty INTEGER,itmprice TEXT,itemimgurl TEXT,itemdesc TEXT,itemadded TEXT,itemsize TEXT);";


	/******************** Used to open database in syncronized way ************/
	private static DataBaseHelper DBHelper = null;

	protected CartDatabase() {
	}
	/******************* Initialize database *************/
	public static void init(Context context) {
		if (DBHelper == null) {
			if (DEBUG)
				Log.i("DBAdapter", context.toString());
			DBHelper = new DataBaseHelper(context);
		}
	}

	/********************** Main Database creation INNER class ********************/
	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			if (DEBUG)
				Log.i(LOG_TAG, "new create");
			try {
				db.execSQL(USER_CREATECARTLIST);
				db.execSQL(USER_CREATECOUPONCARTLIST);


			} catch (Exception exception) {
				if (DEBUG)
					Log.i(LOG_TAG, "Exception onCreate() exception");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (DEBUG)
				Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
						+ "to" + newVersion + "...");

			for (String table : ALL_TABLES) {
				db.execSQL("DROP TABLE IF EXISTS " + table);
			}
			onCreate(db);
		}

	} // Inner class closed


	/********************** Open database for insert,update,delete in syncronized manner ********************/
	private static synchronized SQLiteDatabase open() throws SQLException {
		return DBHelper.getWritableDatabase();
	}


	/************************ General functions**************************/


	/*********************** Escape string for single quotes (Insert,Update)************/
	private static String sqlEscapeString(String aString) {
		String aReturn = "";

		if (null != aString) {
			//aReturn = aString.replace("'", "''");
			aReturn = DatabaseUtils.sqlEscapeString(aString);
			// Remove the enclosing single quotes ...
			aReturn = aReturn.substring(1, aReturn.length() - 1);
		}

		return aReturn;
	}
	/*********************** UnEscape string for single quotes (show data)************/
	private static String sqlUnEscapeString(String aString) {

		String aReturn = "";

		if (null != aString) {
			aReturn = aString.replace("''", "'");
		}

		return aReturn;
	}
	/********************************************************************/

	public static void addCartData(ItemsCartsDO itemCartsDO) {
		final SQLiteDatabase db = open();
		String itemid = sqlEscapeString(itemCartsDO.ItemId);
		String itemname = sqlEscapeString(itemCartsDO.ItemName);
		int itemqty = itemCartsDO.ItemQty;
		String itemprice = sqlEscapeString(itemCartsDO.ItemPrice);
		String itemImgUrl = sqlEscapeString(itemCartsDO.ItemImageUrl);
		String itemDesc=sqlEscapeString(itemCartsDO.ItemDesc);
		String Size=sqlEscapeString(itemCartsDO.ItemSize);

		ContentValues value = new ContentValues();
		value.put(Column_itemid, itemid);
		value.put(Column_itemname, itemname);
		value.put(Column_itemqty, itemqty);
		value.put(Column_itmprice, itemprice);
		value.put(Column_itemimgurl, itemImgUrl);
		value.put(Column_itemdesc, itemDesc);
		value.put(Column_itemadded, "true");
		value.put(Column_itemsize, Size);
		db.insert(Table_Cartlist, null, value);
		db.close(); // Closing database connection
	}


	public static void addCouponCartData(ItemsCartsDO itemCartsDO) {
		final SQLiteDatabase db = open();
		String itemid = sqlEscapeString(itemCartsDO.ItemId);
		String itemname = sqlEscapeString(itemCartsDO.ItemName);
		int itemqty = itemCartsDO.ItemQty;
		String itemprice = sqlEscapeString(itemCartsDO.ItemPrice);
		String itemImgUrl = sqlEscapeString(itemCartsDO.ItemImageUrl);
		String itemDesc=sqlEscapeString(itemCartsDO.ItemDesc);
		String Size=sqlEscapeString(itemCartsDO.ItemSize);

		ContentValues value = new ContentValues();
		value.put(Column_coupon_itemid, itemid);
		value.put(Column_coupon_itemname, itemname);
		value.put(Column_coupon_itemqty, itemqty);
		value.put(Column_coupon_itmprice, itemprice);
		value.put(Column_coupon_itemimgurl, itemImgUrl);
		value.put(Column_coupon_itemdesc, itemDesc);
		value.put(Column_coupon_itemadded, "true");
		value.put(Column_coupon_itemsize, Size);
		db.insert(Table_CouponCartlist, null, value);
		db.close(); // Closing database connection
	}

	// Getting single item in cartlist
	public static ArrayList<ItemsCartsDO> getSingleItemInCartlist(String id) {
		ArrayList<ItemsCartsDO> singlecartList = new ArrayList<ItemsCartsDO>();
		final SQLiteDatabase db = open();

		Cursor cursor = db.query(Table_Cartlist, new String[]{Column_itemid, Column_itemname, Column_itemqty, Column_itmprice, Column_itemimgurl, Column_itemdesc, Column_itemadded, Column_itemsize}, Column_itemid + "=?",
				new String[]{String.valueOf(id)}, null, null, null, null);
		if (cursor.moveToFirst())
		{
			do {
				ItemsCartsDO data = new ItemsCartsDO();
				data.ItemId=cursor.getString(0);
				data.ItemName=cursor.getString(1);
				data.ItemQty=cursor.getInt(2);
				data.ItemPrice=cursor.getString(3);
				data.ItemImageUrl=cursor.getString(4);
				data.ItemDesc=cursor.getString(5);
				data.itemAdded=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				// Adding contact to list
				singlecartList.add(data);
			} while (cursor.moveToNext());
		}
		return singlecartList;
	}

	// Getting All Cartlist
	public static ArrayList<ItemsCartsDO> getAllCartList() {
		ArrayList<ItemsCartsDO> cartList = new ArrayList<ItemsCartsDO>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + Table_Cartlist;

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemsCartsDO data = new ItemsCartsDO();
				data.ItemId=cursor.getString(0);
				data.ItemName=cursor.getString(1);
				data.ItemQty=cursor.getInt(2);
				data.ItemPrice=cursor.getString(3);
				data.ItemImageUrl=cursor.getString(4);
				data.ItemDesc=cursor.getString(5);
				data.itemAdded=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				// Adding contact to list
				cartList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return cartList;
	}



	// Getting All Coupon Cartlist
	public static ArrayList<ItemsCartsDO> getAllCouponCartList() {
		ArrayList<ItemsCartsDO> couponcartList = new ArrayList<ItemsCartsDO>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + Table_CouponCartlist;

		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ItemsCartsDO data = new ItemsCartsDO();
				data.ItemId=cursor.getString(0);
				data.ItemName=cursor.getString(1);
				data.ItemQty=cursor.getInt(2);
				data.ItemPrice=cursor.getString(3);
				data.ItemImageUrl=cursor.getString(4);
				data.ItemDesc=cursor.getString(5);
				data.itemAdded=cursor.getString(6);
				data.ItemSize=cursor.getString(7);
				// Adding contact to list
				couponcartList.add(data);
			} while (cursor.moveToNext());
		}

		// return cartlist list
		return couponcartList;
	}

	public static int updateQuantityInCartlist(ItemsDO data, int quantity) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemqty, quantity);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateQuantityInCartlistCartsDO(ItemsCartsDO data,int quantity) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemqty, quantity);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { data.ItemId });
	}

	public static int updateQuantityFromProfileScreen(String ItemID,int quantity) {
		final SQLiteDatabase db = open();

		ContentValues values = new ContentValues();
		values.put(Column_itemqty, quantity);

		// updating row
		return db.update(Table_Cartlist, values, Column_itemid + " = ?",new String[] { ItemID });
	}

	// Deleting single item in cartlist
	public static void deletecartlist(ItemsCartsDO data)
	{
		final SQLiteDatabase db = open();
		db.delete(Table_Cartlist, Column_itemid + " = ?", new String[]{data.ItemId});
		db.close();
	}


	// Getting cartlist Count
	public static int getCartlistCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_Cartlist,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}

	// Getting coupon cartlist Count
	public static int getCouponCartlistCount()
	{
		SQLiteDatabase db = open();
		Cursor cursor;
		cursor=db.rawQuery("SELECT count(*) FROM "+Table_CouponCartlist,null );
		cursor.moveToFirst();
		int icount = cursor.getInt(0);
		cursor.close();
		return icount;
	}


	public static void clearDatabase()
	{
		SQLiteDatabase db = open();
		db.execSQL("delete from " + Table_Cartlist);
		db.execSQL("delete from " + Table_CouponCartlist);

	}

	public static void clearCouponDatabase()
	{
		SQLiteDatabase db = open();
		db.execSQL("delete from " + Table_CouponCartlist);

	}

}
