package com.nm.cremeville.businesslayer;

import android.content.Context;

import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.webaccess.BaseWA;
import com.nm.cremeville.webaccess.BuildXMLRequest;
import com.nm.cremeville.webaccess.ServiceMethods;

import java.util.ArrayList;


public class CommonBL extends BaseBL {

	public CommonBL(Context mContext, DataListener listener) {
		super(mContext, listener);
	}

	public boolean getAllCategories()
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_CATEGORIES, BuildXMLRequest.getAllCategories());
	}

	public boolean getCoupons()
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_SHOW_COUPON,BuildXMLRequest.getCoupons());
	}

	public boolean getNotifications()
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_SHOW_NOTIFICATIONS,BuildXMLRequest.getNotifications());
	}


	public boolean getTaxes()
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_TAXES,BuildXMLRequest.getTaxes());
	}

	public boolean checkUserExist(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_CHECK_CUSTOMER_EXISTS,BuildXMLRequest.checkUserExist(),queryParams);
	}

	public boolean deleteTheExistingAddress(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_DELETE_EXISTING_ADDRESS,BuildXMLRequest.deleteTheExistingAddress(),queryParams);
	}

	public boolean getCurrentOrder(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_CURRENT_ORDER,BuildXMLRequest.getCurrentOrder(),queryParams);
	}

	public boolean getCurrentLoyaltyPoints(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_CURRENT_LOYALTY_POINTS,BuildXMLRequest.getCurrentLoyaltyPoints(),queryParams);
	}

	public boolean getExistingOrders(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_EXISTING_ORDER,BuildXMLRequest.getExistingOrders(),queryParams);
	}

	public boolean getNearestStoreInfo(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_NEAREST_STORE,BuildXMLRequest.getNearestStoreInfo(),queryParams);
	}

	public boolean getCompleteAddress(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_CURRENT_ADDRESS,BuildXMLRequest.getCompleteAddress(),queryParams);
	}

	public boolean getLogin(String userOTP, String userFirstMobile)
	{

		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_LOGGED_IN,BuildXMLRequest.getLogin(userOTP,userFirstMobile));

	}

	public boolean applyCoupon(String customerID, double grandTotal, String couponCode, ArrayList<ItemsCartsDO> cartList, String deliveryValue, String loyaltyAmount)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_APPLY_COUPON,BuildXMLRequest.applyCoupon(customerID, grandTotal, couponCode, cartList,deliveryValue,loyaltyAmount));
	}

	public boolean postOrderDetails(String customerID, String storeId, String addressId, String deliveryAddress,
									ArrayList<ItemsCartsDO> cartList, double subtotal, double vatTax, String deliveryValue,
									double grandTotal, String couponCode, String couponStatus, String couponAmount,
									String loyaltyStatus, String loyaltyAmount, String loyaltyPoints, String paymentMode,
									String orderOriginId, String transactionId, String paymentStatus, String queryParam,String OrderStatus)
	{

		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_ORDER_DETAILS,BuildXMLRequest.postOrderDetails
				(
						customerID, storeId, addressId, deliveryAddress,
						cartList, subtotal, vatTax, deliveryValue, grandTotal, couponCode, couponStatus, couponAmount, loyaltyStatus, loyaltyAmount,
						loyaltyPoints, paymentMode, orderOriginId, transactionId, paymentStatus, OrderStatus),queryParam);



	}

	public boolean getItems(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_ITEMS,BuildXMLRequest.getItems(),queryParams);
	}

	public boolean getAddressDetails(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_ADDRESS_DETAILS,BuildXMLRequest.getAddressDetails(),queryParams);
	}

	public boolean saveAddressDetailsToServer(String customerID,String houseNumber,String address,String landmark,String city,String pincode,String AreaName,String storeId,String addressKey,String queryParam)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_SAVE_ADDRESS_DETAILS,BuildXMLRequest.saveAddressDetailsToServer(customerID, houseNumber, address, landmark, city, pincode, AreaName, storeId, addressKey),queryParam);
	}

	public boolean SaveEditedAddressDetailsToServer(String houseNumber,String address,String landmark,String pincode,String AreaName,String queryParam)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_EDIT_ADDRESS_DETAILS,BuildXMLRequest.SaveEditedAddressDetailsToServer(houseNumber, address, landmark, pincode, AreaName),queryParam);
	}

	public boolean updateEmailAtServer(String email,String queryParam,String EditedCustomerName)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_UPDATE_EMAIL,BuildXMLRequest.updateEmailAtServer(email,EditedCustomerName),queryParam);
	}

	public boolean getFavouriteItems(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_FAVOURITE_ITEMS,BuildXMLRequest.getFavouriteItems(),queryParams);
	}

	public boolean removeFavouriteItemFromServer(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_DELETE_FAVOURITE_ITEMS,BuildXMLRequest.removeFavouriteItemFromServer(),queryParams);
	}

	public boolean updateCustomerName(String userName,String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_UPDATE_CUSTOMERNAME,BuildXMLRequest.updateCustomerName(userName),queryParams);
	}

	public boolean getFilterMetadata(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_FILTER_METADATA,BuildXMLRequest.getFilterMetadata(),queryParams);
	}

	public boolean addFavouritesToServer(String customerID, ItemsDO model,String queryParam)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_ADD_FAVOURITE_ITEM,BuildXMLRequest.addFavouritesToServer(customerID, model),queryParam);
	}



	public boolean getApplyFilter(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_APPLY_FILTER,BuildXMLRequest.getApplyFilter(),queryParams);
	}

	public boolean getItemsProfile(String queryParams)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_GET_ITEM_PROFILE,BuildXMLRequest.getItemsProfile(),queryParams);
	}


	public boolean postReview(float rating, String itemId, String customerID, String comment, String item,String CustomerName)
	{
		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_ITEM_REVIEW,BuildXMLRequest.postReview(rating,itemId,customerID,comment,item,CustomerName));
	}

	public boolean applystockCheck(ArrayList<ItemsCartsDO> cartList, String storeID) {

		return new BaseWA(mContext, this).startDataDownload(ServiceMethods.WS_POST_STOCK_CHECK,BuildXMLRequest.applystockCheck(cartList,storeID));
	}

}
