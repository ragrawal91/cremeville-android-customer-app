package com.nm.cremeville.adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.activity.CartActivity;
import com.nm.cremeville.activity.OrderDetails;
import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.CurrentDate;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class MyNewOrderAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<GetOrderDO> arrgetOrderDO;
	private ArrayList<ProductDO> couponProductList;
	GetOrderDO getOrderDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;
	CurrentDate currentDate;
	StringBuilder strProduct;
	CartDatabase cartDatabase;
	int CartCount;
	Dialog dialog  ;

	TextView txt_no,txt_yes,txt_text,txt_text1,txt_text2;

	public MyNewOrderAdapter(Context context, ArrayList<GetOrderDO> arrgetOrderDO)
	{
		this.context       = context;
		this.arrgetOrderDO    = arrgetOrderDO;
		couponProductList=new ArrayList<>();
		currentDate=new CurrentDate();
		CartDatabase.init(context);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount()
	{
		return arrgetOrderDO.size();
	}

	@Override
	public Object getItem(int position)
	{
		return arrgetOrderDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_my_orders_parent, parent,false);
			viewHolder = new ViewHolder();

			viewHolder.tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
			viewHolder.tv_dateTime = (TextView) convertView.findViewById(R.id.tv_dateTime);
			viewHolder.tv_deliveryAddress = (TextView) convertView.findViewById(R.id.tv_deliveryAddress);
			viewHolder.tv_reorder= (TextView) convertView.findViewById(R.id.tv_reorder);
			viewHolder.tv_orderid= (TextView) convertView.findViewById(R.id.tv_orderid);
			viewHolder.tv_viewdetial= (TextView) convertView.findViewById(R.id.tv_viewdetial);
			viewHolder.tv_productdetails= (TextView) convertView.findViewById(R.id.tv_productdetails);
			viewHolder.ll_parent= (LinearLayout) convertView.findViewById(R.id.ll_parent);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		getOrderDO = (GetOrderDO) getItem(position);
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.ll_parent);
		fonttype = new FontType(context, root);

        /*if (groupPosition % 2 == 1)
        {

            parentHolder.ll_parent.setBackgroundColor(Color.GRAY);
        }
        else
        {
            parentHolder.ll_parent.setBackgroundColor(Color.WHITE);
        }*/

		//String Ordertime=currentDate.getOrdertime(getOrderDO.CreatedTime);

		viewHolder.tv_dateTime.setText(getOrderDO.OrderTime);
		viewHolder.tv_amount.setText("₹ "+getOrderDO.Grandtotal);
		viewHolder.tv_orderid.setText("Order id: # "+getOrderDO.OrderId);
		viewHolder.tv_deliveryAddress.setText("Delivery Address: "+getOrderDO.Address);


		 strProduct=new StringBuilder();

		for(int j=0;j<getOrderDO.productList.size();j++)
		{
			strProduct.append(getOrderDO.productList.get(j).ItemName);
			strProduct.append(" ");
			strProduct.append("(");
			strProduct.append(""+getOrderDO.productList.get(j).Quantity);
			strProduct.append(")");

			if(j!=getOrderDO.productList.size()-1)
			{
				strProduct.append(", ");
			}
		}
		viewHolder.tv_productdetails.setText(strProduct);

		viewHolder.tv_reorder.setTag(getOrderDO);
		viewHolder.tv_reorder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				GetOrderDO model = (GetOrderDO) v.getTag();
				CartCount=cartDatabase.getCartlistCount();
				couponProductList.clear();

				if(model.CouponStatus.equals("1"))
				{
					showDialogCouponCheck(context, 700, 450,model,"This order contains coupons","Please make a","fresh order");
				}
				else
				{
					for(int i=0;i<model.productList.size();i++)
					{
						ProductDO productDO=model.productList.get(i);
						if (productDO.ItemPrice.equals("0"))
						{
							couponProductList.add(productDO);
						}
					}
					if(couponProductList.size()>0)
					{
						showDialogCouponCheck(context, 700, 450,model,"This order contains coupons","Please make a","fresh order");
					}
					else
					{
						if(CartCount>0)
						{
							showDialogRemove(context, 700, 450,model,"Your cart contains items.","Do you want to discard the","selection and add these items.");
						}
						else
						{
							addItemToCartList(model);
						}

					}
				}
			}
		});


		viewHolder.tv_viewdetial.setTag(getOrderDO);
		viewHolder.tv_viewdetial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				GetOrderDO model = (GetOrderDO) v.getTag();
				Intent intent=new Intent(context, OrderDetails.class);
				Bundle bundle=new Bundle();
				bundle.putString("OrderID",model.OrderId);
				bundle.putString("TotalAmount",model.Grandtotal);
				bundle.putString("OrderTime",model.OrderTime);
				bundle.putString("Address",model.Address);
				bundle.putString("Subtotal",model.Subtotal);
				bundle.putString("DeliveryCharge",model.DeliveryCharge);
				bundle.putString("CouponCode",model.CouponCode);
				bundle.putString("CouponAmount",model.CouponAmount);
				bundle.putString("CouponStatus",model.CouponStatus);
				bundle.putString("PaymentMode",model.PaymentMode);
				bundle.putParcelableArrayList("ProductList",model.productList);
				intent.putExtras(bundle);
				context.startActivity(intent);
			}
		});

		return convertView;
	}

	private void addItemToCartList(GetOrderDO model)
	{
		for(int i=0;i<model.productList.size();i++)
		{
			cartDatabase.addCartData(new ItemsCartsDO(model.productList.get(i).ItemId, model.productList.get(i).ItemName, Integer.valueOf(model.productList.get(i).Quantity), model.productList.get(i).ItemPrice, model.productList.get(i).ItemImage, "Desc","true",model.productList.get(i).ItemSize));
		}
		Intent intent=new Intent(context, CartActivity.class);
		context.startActivity(intent);
	}


	private class ViewHolder {
		TextView tv_dateTime, tv_deliveryAddress, tv_amount,tv_reorder,tv_orderid,tv_productdetails,tv_viewdetial;
		LinearLayout ll_parent;

	}

	public void showDialogRemove(Context context, int x, int y, final GetOrderDO modelgetOrderDO,String text1,String text2, String text )
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogremove);
		dialog.setCanceledOnTouchOutside(false);

		txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
		txt_no= (TextView) dialog.findViewById(R.id.txt_no);
		txt_text= (TextView) dialog.findViewById(R.id.txt_text);
		txt_text1= (TextView) dialog.findViewById(R.id.txt_text1);
		txt_text2= (TextView) dialog.findViewById(R.id.txt_text2);


		ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
		fonttype = new FontType(context, root);

		txt_text1.setText(text1);
		txt_text2.setText(text2);
		txt_text.setText(text);

		txt_yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CartDatabase.clearDatabase();
				addItemToCartList(modelgetOrderDO);
				dialog.dismiss();
			}
		});

		txt_no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		dialog.show();
	}


	public void showDialogCouponCheck(Context context, int x, int y, final GetOrderDO modelgetOrderDO,String text1,String text2, String text )
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogcouponcheck);
		dialog.setCanceledOnTouchOutside(false);

		txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
		txt_text= (TextView) dialog.findViewById(R.id.txt_text);
		txt_text1= (TextView) dialog.findViewById(R.id.txt_text1);
		txt_text2= (TextView) dialog.findViewById(R.id.txt_text2);


		ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
		fonttype = new FontType(context, root);

		txt_text1.setText(text1);
		txt_text2.setText(text2);
		txt_text.setText(text);

		txt_yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});


		dialog.show();
	}


}