package com.nm.cremeville.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class OrderedItemsAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ProductDO> productList;
	ProductDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;



	public OrderedItemsAdapter(Context context, ArrayList<ProductDO> productList)
	{
		this.context       = context;
		this.productList    = productList;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount()
	{
		return productList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return productList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_ordereditems, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemname 	    = (TextView)convertView.findViewById(R.id.txt_itemname);
			viewHolder.txt_itemqty 	    = (TextView)convertView.findViewById(R.id.txt_itemqty);
			viewHolder.txt_totalprice 	    = (TextView)convertView.findViewById(R.id.txt_totalprice);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_ordereditems);
		fonttype = new FontType(context, root);

		model = (ProductDO) getItem(position);

		viewHolder.txt_itemname.setText(model.ItemName);
		viewHolder.txt_itemqty.setText(model.Quantity);
		viewHolder.txt_totalprice.setText("₹ " + model.Total);

		return convertView;
	}

	private class ViewHolder
	{
		TextView txt_itemname,txt_itemqty,txt_totalprice;
	}
}