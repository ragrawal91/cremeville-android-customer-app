package com.nm.cremeville.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nm.cremeville.R;
import com.nm.cremeville.objects.ShowCouponDO;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class ShowCouponAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ShowCouponDO> arrShowCouponDO;
	ShowCouponDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;




	public ShowCouponAdapter(Context context, ArrayList<ShowCouponDO> arrShowCouponDO)
	{
		this.context       = context;
		this.arrShowCouponDO    = arrShowCouponDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.noimage)
				.showImageOnLoading(R.mipmap.noimage)
				.showImageForEmptyUri(R.mipmap.noimage)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrShowCouponDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrShowCouponDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_coupons, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_couponvalue 	    = (TextView)convertView.findViewById(R.id.txt_couponvalue);
			viewHolder.ll_coupnimage=(LinearLayout)convertView.findViewById(R.id.ll_coupnimage);
			viewHolder.txt_copy=(TextView)convertView.findViewById(R.id.txt_copy);



			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_coupons);
		fonttype = new FontType(context, root);

		model = (ShowCouponDO) getItem(position);

		viewHolder.txt_couponvalue.setText("USE CODE : " + model.FinalValue);

		viewHolder.txt_copy.setTag(model);
		viewHolder.txt_copy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ShowCouponDO model = (ShowCouponDO) view.getTag();

				int sdk = android.os.Build.VERSION.SDK_INT;
				if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB)
				{
					android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
					clipboard.setText(model.FinalValue);
				}
				else
				{
					android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
					android.content.ClipData clip = android.content.ClipData.newPlainText("Code",model.FinalValue);
					clipboard.setPrimaryClip(clip);
				}

				Toast.makeText(context,"Code copied",Toast.LENGTH_SHORT).show();

			}
		});


		viewHolder.ll_coupnimage.setTag(model);
		imgLoader.getInstance().displayImage(model.ImageUrl, new BgViewAware(viewHolder.ll_coupnimage), dispImage);

		return convertView;
	}



	private class ViewHolder {
		TextView txt_couponvalue,txt_copy;
		LinearLayout ll_coupnimage;
	}
}