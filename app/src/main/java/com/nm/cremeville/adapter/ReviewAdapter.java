package com.nm.cremeville.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.ReviewDO;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class ReviewAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ReviewDO> reviewList;
	ReviewDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;



	public ReviewAdapter(Context context, ArrayList<ReviewDO> reviewList)
	{
		this.context       = context;
		this.reviewList    = reviewList;
		CartDatabase.init(context);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount()
	{
		return reviewList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return reviewList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_reviews, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_personName 	    = (TextView)convertView.findViewById(R.id.txt_personName);
			viewHolder.txt_comment 	    = (TextView)convertView.findViewById(R.id.txt_comment);
			viewHolder.ratBar1=(RatingBar)convertView.findViewById(R.id.ratBar1);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_reviews);
		fonttype = new FontType(context, root);

		model = (ReviewDO) getItem(position);

		viewHolder.txt_personName.setText(model.UserName);
		viewHolder.txt_comment.setText(model.Comment);
		viewHolder.ratBar1.setRating(Float.valueOf(model.Rating));

		return convertView;
	}

	private class ViewHolder {
		TextView txt_personName,txt_comment;
		RatingBar ratBar1;
	}
}