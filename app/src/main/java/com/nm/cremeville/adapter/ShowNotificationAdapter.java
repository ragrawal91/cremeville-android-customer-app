package com.nm.cremeville.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.ShowNotificationDO;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class ShowNotificationAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ShowNotificationDO> arrShowNotificationDO;
	ShowNotificationDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;

	public ShowNotificationAdapter(Context context, ArrayList<ShowNotificationDO> arrShowNotificationDO)
	{
		this.context       = context;
		this.arrShowNotificationDO    = arrShowNotificationDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount()
	{
		return arrShowNotificationDO.size();
	}

	@Override
	public Object getItem(int position)
	{
		return arrShowNotificationDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_notification, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_header 	    = (TextView)convertView.findViewById(R.id.txt_header);
			viewHolder.txt_description 	    = (TextView)convertView.findViewById(R.id.txt_description);
			viewHolder.txt_couponcode 	    = (TextView)convertView.findViewById(R.id.txt_couponcode);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_notifications);
		fonttype = new FontType(context, root);

		model = (ShowNotificationDO) getItem(position);

		viewHolder.txt_header.setText(model.header);
		viewHolder.txt_description.setText(model.description);
		viewHolder.txt_couponcode.setText("COUPON CODE : "+model.couponCode);

		return convertView;
	}

	private class ViewHolder {
		TextView txt_header,txt_description,txt_couponcode;
	}
}