package com.nm.cremeville.adapter;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nm.cremeville.activity.MyApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nm.cremeville.R;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;



public class CartAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsCartsDO> arrCartDO;
	ItemsCartsDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	int qty;
	Dialog dialog  ;
	CartListListener cartListListener;
	TextView txt_no,txt_yes,txt_text;

	public CartAdapter(Context context, ArrayList<ItemsCartsDO> arrCartDO)
	{
		this.context       = context;
		this.arrCartDO    = arrCartDO;
		CartDatabase.init(context);
		//animBounce = AnimationUtils.loadAnimation(context,R.anim.bounce);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.noimage_48)
				.showImageOnLoading(R.mipmap.noimage_48)
				.showImageForEmptyUri(R.mipmap.noimage_48)
				.displayer(new RoundedBitmapDisplayer((int) 50))
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrCartDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrCartDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_cart, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemcart 	    = (TextView)convertView.findViewById(R.id.txt_itemcart);
			viewHolder.txt_itemprice 	    = (TextView)convertView.findViewById(R.id.txt_itemprice);
			viewHolder.txt_itemmultipliedprice 	    = (TextView)convertView.findViewById(R.id.txt_itemmultipliedprice);
			viewHolder.txt_itemSize 	    = (TextView)convertView.findViewById(R.id.txt_itemSize);
			viewHolder.txt_qty 	    = (TextView)convertView.findViewById(R.id.txt_qty);

			viewHolder.img_minus 	    = (ImageView)convertView.findViewById(R.id.img_minus);
			viewHolder.img_plus 	    = (ImageView)convertView.findViewById(R.id.img_plus);
			viewHolder.img_remove 	    = (ImageView)convertView.findViewById(R.id.img_remove);
			viewHolder.img_itemimage = (ImageView)convertView.findViewById(R.id.img_itemimage);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cart);
		fonttype = new FontType(context, root);

		model = (ItemsCartsDO) getItem(position);

		qty=model.ItemQty;

		viewHolder.txt_itemcart.setText(model.ItemName);
		viewHolder.txt_itemprice.setText("₹ "+model.ItemPrice);

		viewHolder.txt_qty.setTag(model);
		viewHolder.txt_qty.setText("" + qty);
		viewHolder.txt_itemSize.setText(model.ItemSize);


		String multipliedPrice=String.format("%.0f",Double.parseDouble(model.ItemPrice) * qty);
		viewHolder.txt_itemmultipliedprice.setText("₹ "+multipliedPrice);


		viewHolder.img_minus.setTag(model);
		viewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsCartsDO model = (ItemsCartsDO) view.getTag();
				int crtQuantity = model.ItemQty;
				if (crtQuantity <= 1)
				{
					model.ItemQty = 1;

				}
				else
				{
					model.ItemQty = crtQuantity - 1;
					cartListListener.getTheDecreasedSubtotal(Double.parseDouble(model.ItemPrice));
				}

				CartDatabase.updateQuantityInCartlistCartsDO(model, model.ItemQty);
				notifyDataSetChanged();


			}
		});


		viewHolder.img_plus.setTag(model);
		viewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsCartsDO model = (ItemsCartsDO) view.getTag();
				int crtQuantity = model.ItemQty;
				if (crtQuantity >= 5)
				{
					model.ItemQty = 5;

				}
				else
				{
					model.ItemQty = crtQuantity + 1;
					cartListListener.getTheIncreasedSubtotal(Double.parseDouble(model.ItemPrice));
				}
				CartDatabase.updateQuantityInCartlistCartsDO(model, model.ItemQty);

				notifyDataSetChanged();

			}
		});

		viewHolder.img_remove.setTag(model);
		viewHolder.img_remove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsCartsDO model = (ItemsCartsDO) view.getTag();
				showDialogRemove(context, 700, 450, model);
			}
		});

		viewHolder.img_itemimage.setTag(model);
		imgLoader.getInstance().displayImage(model.ItemImageUrl, viewHolder.img_itemimage, dispImage);

		return convertView;
	}


	private class ViewHolder {
		TextView txt_itemcart,txt_itemprice,txt_qty,txt_itemmultipliedprice,txt_itemSize;
		ImageView img_minus,img_plus,img_remove,img_itemimage;

	}

	public void registerLocaldatabaseListener(CartListListener cartListListener)
	{
		this.cartListListener=cartListListener;
	}

	public void showDialogRemove(Context context, int x, int y, final ItemsCartsDO model)
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogremove);
		dialog.setCanceledOnTouchOutside(false);

		txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
		txt_no= (TextView) dialog.findViewById(R.id.txt_no);
		txt_text= (TextView) dialog.findViewById(R.id.txt_text);


		ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
		fonttype = new FontType(context, root);

		txt_text.setText(model.ItemName+" ?");

		txt_yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				MyApplication.getInstance().trackEvent("Delete", model.ItemName, "Product Deleted after adding in cart");
				CartDatabase.deletecartlist(model);
				cartListListener.getLocalDatabaseCartList();
				dialog.dismiss();

			}
		});

		txt_no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});

		dialog.show();
	}
}