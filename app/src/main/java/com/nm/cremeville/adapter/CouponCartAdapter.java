package com.nm.cremeville.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nm.cremeville.R;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class CouponCartAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ProductDO> arrCartDO;
	ProductDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	int qty;

	public CouponCartAdapter(Context context, ArrayList<ProductDO> arrCartDO)
	{
		this.context       = context;
		this.arrCartDO    = arrCartDO;
		CartDatabase.init(context);
		//animBounce = AnimationUtils.loadAnimation(context,R.anim.bounce);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.noimage_48)
				.showImageOnLoading(R.mipmap.noimage_48)
				.showImageForEmptyUri(R.mipmap.noimage_48)
				.displayer(new RoundedBitmapDisplayer((int) 50))
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrCartDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrCartDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_couponcart, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemcart 	    = (TextView)convertView.findViewById(R.id.txt_itemcart);
			viewHolder.txt_itemprice 	    = (TextView)convertView.findViewById(R.id.txt_itemprice);
			viewHolder.txt_itemSize 	    = (TextView)convertView.findViewById(R.id.txt_itemSize);
			viewHolder.txt_itemmultipliedprice 	    = (TextView)convertView.findViewById(R.id.txt_itemmultipliedprice );

			viewHolder.txt_qty 	    = (TextView)convertView.findViewById(R.id.txt_qty);


			viewHolder.img_itemimage = (ImageView)convertView.findViewById(R.id.img_itemimage);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_cart);
		fonttype = new FontType(context, root);

		model = (ProductDO) getItem(position);

		qty=Integer.valueOf(model.Quantity);

		viewHolder.txt_itemcart.setText(model.ItemName);
		viewHolder.txt_itemprice.setText("₹ "+model.ItemPrice);

		viewHolder.txt_qty.setTag(model);
		viewHolder.txt_qty.setText("" + qty);
		viewHolder.txt_itemSize.setText(model.ItemSize);


		String multipliedPrice=String.format("%.0f",Double.parseDouble(model.ItemPrice) * qty);
		viewHolder.txt_itemmultipliedprice.setText("₹ "+multipliedPrice);


		viewHolder.img_itemimage.setTag(model);
		imgLoader.getInstance().displayImage(model.ItemImage, viewHolder.img_itemimage, dispImage);

		return convertView;
	}


	private class ViewHolder {
		TextView txt_itemcart,txt_itemprice,txt_qty,txt_itemmultipliedprice,txt_itemSize;
		ImageView img_itemimage;

	}
}