package com.nm.cremeville.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.nm.cremeville.R;
import com.nm.cremeville.listener.FilterCheckboxListener;
import com.nm.cremeville.objects.FlavourDO;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {


    Context context;
    ArrayList<FlavourDO> datalist;
    FlavourDO flavourDO;
    FilterCheckboxListener filterCheckboxListener;
    LayoutInflater inflater;
    FontType fonttype;

    GridViewAdapter(Context context,ArrayList<FlavourDO> datalist)
    {
        this.datalist=datalist;

        this.context=context;

        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        flavourDO= (FlavourDO) getItem(position);

        ViewHolder viewHolder=new ViewHolder();
        if (convertView==null)
        {
            convertView=inflater.inflate(R.layout.check_box,null);

            viewHolder.checkbox= (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder= (ViewHolder) convertView.getTag();
        }

        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_checkbox);
        fonttype = new FontType(context, root);

        viewHolder.checkbox.setTag(flavourDO);
        viewHolder.checkbox.setText(flavourDO.Name);
        viewHolder.checkbox.setChecked(flavourDO.isChecked);
        viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                flavourDO=(FlavourDO)v.getTag();
                if(flavourDO.isChecked)
                {
                    flavourDO.isChecked=false;

                }
                else
                {
                    flavourDO.isChecked=true;
                }
                filterCheckboxListener.getCheckBoxFilter(flavourDO);
            }
        });

        return convertView;
    }

    class ViewHolder
    {
        CheckBox checkbox;
    }

    public void registerCheckboxFilterListerner(FilterCheckboxListener filterCheckboxListener)
    {
         this.filterCheckboxListener= filterCheckboxListener;
    }
}
