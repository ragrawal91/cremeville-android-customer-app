package com.nm.cremeville.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.R;
import com.nm.cremeville.activity.ItemsActivity;
import com.nm.cremeville.activity.MyApplication;
import com.nm.cremeville.objects.CategoryDO;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.FontType;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoryAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<CategoryDO> arrCategoryDO;
	CategoryDO categoryDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;

	public CategoryAdapter(Context context, ArrayList<CategoryDO> arrCategoryDO)
	{
		this.context       = context;
		this.arrCategoryDO    = arrCategoryDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.chocolate)
				.showImageOnLoading(R.mipmap.chocolate)
				.showImageForEmptyUri(R.mipmap.chocolate)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();

	}

	@Override
	public int getCount()
	{

		return arrCategoryDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrCategoryDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_catg, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_catg 	    = (TextView)convertView.findViewById(R.id.txt_catg);
			viewHolder.ll_catg = (LinearLayout) convertView.findViewById(R.id.ll_catg);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		categoryDO = (CategoryDO) getItem(position);

		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_catg);
		fonttype = new FontType(context, root);

		viewHolder.txt_catg.setText(categoryDO.CatgName);

		viewHolder.ll_catg.setTag(categoryDO);
		viewHolder.ll_catg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				categoryDO = (CategoryDO) view.getTag();
				String CategoryId = categoryDO.CatgId;
				String CatgName = categoryDO.CatgName;

				MyApplication.getInstance().trackEvent("Category", CatgName, "Category Clicked");

				Map<String, Object> categoryClicked = new HashMap<String, Object>();
				categoryClicked.put(AFInAppEventParameterName.LEVEL,1);
				categoryClicked.put(AFInAppEventParameterName.SCORE,10);
				AppsFlyerLib.getInstance().trackEvent(context,CatgName+" is Clicked",categoryClicked);

				Intent intent = new Intent(context, ItemsActivity.class);
				intent.putExtra("CatgId", CategoryId);
				intent.putExtra("CatgName", CatgName);
				context.startActivity(intent);

				//Toast.makeText(context, "Row  Clicked", Toast.LENGTH_SHORT).show();
			}
		});

		viewHolder.ll_catg.setTag(categoryDO);
		imgLoader.getInstance().displayImage(categoryDO.CatgImageUrl, new BgViewAware(viewHolder.ll_catg), dispImage);

		return convertView;
	}


	private class ViewHolder
	{
		TextView  txt_catg;
		ImageView img_catg;
		LinearLayout row_catg,ll_catg;
	}



}
