package com.nm.cremeville.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class StockCheckAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ProductDO> arrCartDO;
	ProductDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	FontType fonttype;
	int qty;

	public StockCheckAdapter(Context context, ArrayList<ProductDO> arrCartDO)
	{
		this.context       = context;
		this.arrCartDO    = arrCartDO;
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount()
	{
		return arrCartDO.size();
	}

	@Override
	public Object getItem(int position)
	{
		return arrCartDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_stockcheck, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemcart 	    = (TextView)convertView.findViewById(R.id.txt_itemcart);

			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_stockcheck);
		fonttype = new FontType(context, root);

		model = (ProductDO) getItem(position);

		qty=Integer.valueOf(model.Quantity);

		viewHolder.txt_itemcart.setText(model.ItemName);

		return convertView;
	}

	private class ViewHolder {
		TextView txt_itemcart;

	}
}