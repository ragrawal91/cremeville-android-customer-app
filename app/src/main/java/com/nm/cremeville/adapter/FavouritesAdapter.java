package com.nm.cremeville.adapter;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nm.cremeville.R;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;


public class FavouritesAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsDO> arrfavouriteDO;
	ItemsDO model;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	CartDatabase cartDatabase;
	int QTY;
	Dialog dialog  ;
	CartListListener cartListListener;
	TextView txt_no,txt_yes,txt_text;
	ArrayList<ItemsCartsDO> LocalCartList;
	ArrayList<ItemsCartsDO> SingleItemCartList;



	public FavouritesAdapter(Context context, ArrayList<ItemsDO> arrfavouriteDO)
	{
		this.context       = context;
		this.arrfavouriteDO    = arrfavouriteDO;
		CartDatabase.init(context);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.error)
				.showImageOnLoading(R.mipmap.error)
				.showImageForEmptyUri(R.mipmap.error)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{

		return arrfavouriteDO.size();

	}

	@Override
	public Object getItem(int position)
	{
		return arrfavouriteDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_favourites, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_itemcart 	    = (TextView)convertView.findViewById(R.id.txt_itemcart);
			viewHolder.txt_itemprice 	    = (TextView)convertView.findViewById(R.id.txt_itemprice);
			viewHolder.txt_itemSize 	    = (TextView)convertView.findViewById(R.id.txt_itemSize);


			viewHolder.img_fav 	    = (ImageView)convertView.findViewById(R.id.img_fav);
			viewHolder.txt_add 	    = (TextView)convertView.findViewById(R.id.txt_add);
			viewHolder.ll_itemimage=(LinearLayout)convertView.findViewById(R.id.ll_itemimage);
			viewHolder.txt_qty= (TextView)convertView.findViewById(R.id.txt_qty);
			viewHolder.img_minus= (ImageView)convertView.findViewById(R.id.img_minus);
			viewHolder.img_plus= (ImageView)convertView.findViewById(R.id.img_plus);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_favourites);
		fonttype = new FontType(context, root);

		model = (ItemsDO) getItem(position);

		SetAddButtonVisibility(model.ItemId);
		viewHolder.txt_qty.setText("" + model.ItemQty);
		viewHolder.txt_itemcart.setText(model.ItemName);
		viewHolder.txt_itemprice.setText("₹ " + model.ItemPrice);
		viewHolder.txt_itemSize.setText(model.Size);


		viewHolder.img_fav.setTag(model);
		viewHolder.img_fav.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model = (ItemsDO) view.getTag();
				showDialogRemove(context, 700, 450, model);
			}
		});

		viewHolder.img_plus.setTag(model);
		viewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model = (ItemsDO) view.getTag();
				String ItemId=model.ItemId;
				int crtQuantity = model.ItemQty;
				if (crtQuantity >= 5)
				{
					model.ItemQty = 5;
				}
				else
				{
					model.ItemQty = crtQuantity + 1;
				}
				CartDatabase.updateQuantityFromProfileScreen(ItemId, model.ItemQty);
				notifyDataSetChanged();

			}
		});

		viewHolder.img_minus.setTag(model);
		viewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view)
			{
				ItemsDO model = (ItemsDO) view.getTag();
				String ItemId=model.ItemId;
				int crtQuantity = model.ItemQty;
				if (crtQuantity <= 1)
				{
					model.ItemQty = 1;

				}
				else
				{
					model.ItemQty = crtQuantity - 1;
				}
				CartDatabase.updateQuantityFromProfileScreen(ItemId, model.ItemQty);
				notifyDataSetChanged();
			}
		});

		viewHolder.txt_add.setTag(model);
		viewHolder.txt_add.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				ItemsDO model = (ItemsDO) view.getTag();
				String ItemId=model.ItemId;
			//	LocalCartList=CartDatabase.getAllCartList();
				//SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);

				addItemToCartList(model);

				/*if(LocalCartList.size()>0)
				{
					if(SingleItemCartList.size()>0)
					{
						Toast.makeText(context, "This item is already added in cart", Toast.LENGTH_SHORT).show();
					}
					else
					{
						addItemToCartList(model);
					}
				}
				else
				{
					addItemToCartList(model);
				}
*/
			}
		});


		viewHolder.ll_itemimage.setTag(model);
		imgLoader.getInstance().displayImage(model.ItemImageUrl_100, new BgViewAware(viewHolder.ll_itemimage), dispImage);

		return convertView;
	}

	public void SetAddButtonVisibility(String ItemId)
	{
		SingleItemCartList=CartDatabase.getSingleItemInCartlist(ItemId);
		if(SingleItemCartList.size()>0)
		{
			viewHolder.txt_add.setVisibility(View.INVISIBLE);

		}
		else {
			viewHolder.txt_add.setVisibility(View.VISIBLE);
		}
	}

	private void addItemToCartList(ItemsDO model)
	{
		Toast.makeText(context, "Item added to cart", Toast.LENGTH_SHORT).show();
		notifyDataSetChanged();
		cartDatabase.addCartData(new ItemsCartsDO(model.ItemId, model.ItemName, 1, model.ItemPrice, model.ItemImageUrl_100, "ItemDesc", "true",model.Size));
		cartListListener.getCartList(model);
		viewHolder.txt_add.setVisibility(View.INVISIBLE);
		viewHolder.txt_qty.setText("1");
	}


	public void showDialogRemove(Context context, int x, int y,  final ItemsDO model)
	{
		dialog  = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.cart_dialogremove);
		dialog.setCanceledOnTouchOutside(false);

		txt_yes = (TextView) dialog.findViewById(R.id.txt_yes);
		txt_no= (TextView) dialog.findViewById(R.id.txt_no);
		txt_text= (TextView) dialog.findViewById(R.id.txt_text);

		ViewGroup root = (ViewGroup) dialog.findViewById(R.id.row_dialog);
		fonttype = new FontType(context, root);

		txt_text.setText(model.ItemName + " ?");

		txt_yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				cartListListener.removeFavouriteItem(model);

				arrfavouriteDO.remove(model);
				cartListListener.getSizeFavouriteItemList(arrfavouriteDO.size());
				notifyDataSetChanged();
				dialog.dismiss();

			}
		});

		txt_no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});

		dialog.show();
	}

	public void registerFavouriteItemListener(CartListListener cartListListener)
	{
		this.cartListListener=cartListListener;
	}

	private class ViewHolder {
		TextView txt_itemcart,txt_itemprice,txt_add,txt_itemSize;
		ImageView img_fav;
		LinearLayout ll_itemimage;
		TextView txt_qty;
		ImageView img_minus,img_plus;

	}
}