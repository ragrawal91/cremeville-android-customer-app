package com.nm.cremeville.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.SearchPlaceDomain;

import java.util.ArrayList;


public class GoogleSearchPlaceAdapter extends BaseAdapter
{
    LayoutInflater inflater;

    ViewHolder holder;

    Context context;

    ArrayList<SearchPlaceDomain> searchPlacelist;

    SearchPlaceDomain searchPlaceDomain;


    public GoogleSearchPlaceAdapter(Context context,ArrayList<SearchPlaceDomain> searchPlacelist)
    {
        this.context=context;

        this.searchPlacelist=searchPlacelist;

        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return searchPlacelist.size();
    }

    @Override
    public Object getItem(int position) {
        return searchPlacelist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            holder=new ViewHolder();

            convertView=inflater.inflate(R.layout.row_googleplacelayout,parent,false);

            holder.tv_placedetailname= (TextView) convertView.findViewById(R.id.tv_placedetailname);

            convertView.setTag(holder);
        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }

        searchPlaceDomain= (SearchPlaceDomain) getItem(position);

        holder.tv_placedetailname.setText(searchPlaceDomain.location_address);

        return convertView;
    }


    private class ViewHolder
    {
        TextView tv_placedetailname;
    }
}
