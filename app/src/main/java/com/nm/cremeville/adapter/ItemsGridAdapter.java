package com.nm.cremeville.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.nm.cremeville.activity.MyApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nm.cremeville.R;
import com.nm.cremeville.activity.ItemProfileActivity;
import com.nm.cremeville.listener.CartListListener;
import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.utilities.BgViewAware;
import com.nm.cremeville.utilities.CartDatabase;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemsGridAdapter extends BaseAdapter
{

	private Context context;
	private ArrayList<ItemsDO> arrItemsDO;
	ViewHolder viewHolder;
	LayoutInflater inflater;
	ImageLoaderConfiguration imgLoaderConf;
	DisplayImageOptions dispImage;
	ImageLoader imgLoader= ImageLoader.getInstance();
	FontType fonttype;
	CartDatabase cartDatabase;
	ItemsDO model;
	CartListListener cartListListener;


	public ItemsGridAdapter(Context context, ArrayList<ItemsDO> arrItemsDO)
	{
		this.context       = context;
		this.arrItemsDO    = arrItemsDO;
		CartDatabase.init(context);
		inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//animBounce = AnimationUtils.loadAnimation(context, R.anim.bounce);
		imgLoaderConf = new ImageLoaderConfiguration.Builder(context).build();
		imgLoader.init(imgLoaderConf);
		dispImage=new DisplayImageOptions.Builder()
				.showImageOnFail(R.mipmap.noimage_48)
				.showImageOnLoading(R.mipmap.noimage_48)
				.showImageForEmptyUri(R.mipmap.noimage_48)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.delayBeforeLoading(100)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}

	@Override
	public int getCount()
	{
		return arrItemsDO.size();
	}

	@Override
	public Object getItem(int position)
	{
		return arrItemsDO.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		if(convertView==null)
		{
			convertView = inflater.inflate(R.layout.row_griditems, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.txt_item 	    = (TextView)convertView.findViewById(R.id.txt_item);
			viewHolder.txt_basePrice 	    = (TextView)convertView.findViewById(R.id.txt_basePrice);
			viewHolder.txt_qty 	    = (TextView)convertView.findViewById(R.id.txt_qty);
			viewHolder.txt_itemSize 	    = (TextView)convertView.findViewById(R.id.txt_itemSize);
			viewHolder.row_griditem 	    = (LinearLayout)convertView.findViewById(R.id.row_griditem);
			viewHolder.ll_itmimg 	    = (FrameLayout)convertView.findViewById(R.id.ll_itmimg);

			viewHolder.ll_addBtn 	    = (LinearLayout)convertView.findViewById(R.id.ll_addBtn);
			viewHolder.ll_qty 	    = (LinearLayout)convertView.findViewById(R.id.ll_qty);

			viewHolder.img_plus 	    = (ImageView)convertView.findViewById(R.id.img_plus);
			viewHolder.img_minus 	    = (ImageView)convertView.findViewById(R.id.img_minus);
			viewHolder.img_favourite 	    = (ImageView)convertView.findViewById(R.id.img_favourite);
			viewHolder.img_offer 	    = (ImageView)convertView.findViewById(R.id.img_offer);
			viewHolder.txt_offerdesc 	    = (TextView)convertView.findViewById(R.id.txt_offerdesc);


			convertView.setTag(viewHolder);
		}
		else
		{
			viewHolder= (ViewHolder) convertView.getTag();
		}

		model = (ItemsDO) getItem(position);
		ViewGroup root = (ViewGroup) convertView.findViewById(R.id.row_griditem);
		fonttype = new FontType(context, root);


		if(model.cartIconStatus)
		{
			viewHolder.ll_addBtn.setVisibility(View.INVISIBLE);
			viewHolder.ll_qty.setVisibility(View.VISIBLE);
		}
		else
		{
			viewHolder.ll_addBtn.setVisibility(View.VISIBLE);
			viewHolder.ll_qty.setVisibility(View.INVISIBLE);
		}

		if(model.FavouriteStatus)
		{
			viewHolder.img_favourite.setImageResource(R.mipmap.favourite_pink);
		}
		else
		{
			viewHolder.img_favourite.setImageResource(R.mipmap.favourite_black);
		}

		viewHolder.img_offer.setImageResource(model.OfferIcon);
		viewHolder.txt_offerdesc.setVisibility(model.offerDescStatus);
		viewHolder.txt_offerdesc.setText(model.offerDescription);
		viewHolder.txt_item.setText(model.ItemName);
		viewHolder.txt_qty.setText(""+model.ItemQty);
		viewHolder.txt_basePrice.setText("₹ " + model.ItemPrice);
		viewHolder.txt_itemSize.setText(model.Size);

		viewHolder.ll_itmimg.setTag(model);
		viewHolder.ll_itmimg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model = (ItemsDO) view.getTag();
				String ItemId = model.ItemId;
				String ItemName = model.ItemName;

				MyApplication.getInstance().trackEvent("Product", ItemName, "Product Clicked");

				Map<String, Object> productClicked = new HashMap<String, Object>();
				productClicked.put(AFInAppEventParameterName.LEVEL,2);
				productClicked.put(AFInAppEventParameterName.SCORE,20);
				AppsFlyerLib.getInstance().trackEvent(context,ItemName+" is Clicked",productClicked);

				Intent intent = new Intent(context, ItemProfileActivity.class);
				intent.putExtra("ItemId", ItemId);
				intent.putExtra("ItemName", ItemName);
				context.startActivity(intent);
			}
		});


		viewHolder.img_minus.setTag(model);
		viewHolder.img_minus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model = (ItemsDO) view.getTag();
				int crtQuantity = model.ItemQty;
				if (crtQuantity <= 1) {
					model.ItemQty = 1;

				} else {
					model.ItemQty = crtQuantity - 1;
					//cartListListener.getTheDecreasedSubtotal(Double.parseDouble(model.ItemFinalPrice));
				}

				CartDatabase.updateQuantityInCartlist(model, model.ItemQty);
				notifyDataSetChanged();

			}
		});


		viewHolder.img_favourite.setTag(model);
		viewHolder.img_favourite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view)
			{
				ItemsDO model= (ItemsDO) view.getTag();
				if(model.FavouriteStatus)
				{
					cartListListener.removeFavouriteItem(model);
				}
				else
				{
					cartListListener.addFavouriteItem(model);
				}

			}
		});


		viewHolder.img_plus.setTag(model);
		viewHolder.img_plus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model= (ItemsDO) view.getTag();
				int crtQuantity=model.ItemQty;

				if (crtQuantity >= 5)
				{
					model.ItemQty = 5;

				}
				else
				{
					model.ItemQty = crtQuantity + 1;
					cartListListener.getTheIncreasedSubtotal(Double.parseDouble(model.ItemPrice));
				}

				Map<String, Object> quantityIncrease = new HashMap<String, Object>();
				quantityIncrease.put(AFInAppEventParameterName.LEVEL,4);
				quantityIncrease.put(AFInAppEventParameterName.SCORE,40);
				AppsFlyerLib.getInstance().trackEvent(context,"quantity increased",quantityIncrease);

				CartDatabase.updateQuantityInCartlist(model, model.ItemQty);
				notifyDataSetChanged();
			}
		});


		viewHolder.ll_addBtn.setTag(model);
		viewHolder.ll_addBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ItemsDO model= (ItemsDO) view.getTag();
				MyApplication.getInstance().trackEvent("Add to cart", model.ItemName, "Product added to cart");
				viewHolder.ll_addBtn.setVisibility(View.INVISIBLE);
				viewHolder.ll_qty.setVisibility(View.VISIBLE);

				Map<String, Object> addToCart = new HashMap<String, Object>();
				addToCart.put(AFInAppEventParameterName.LEVEL,3);
				addToCart.put(AFInAppEventParameterName.SCORE,30);
				AppsFlyerLib.getInstance().trackEvent(context,model.ItemName+" added to cart",addToCart);

				addItemToCartList(model);
			}
		});

		viewHolder.ll_itmimg.setTag(model);
		imgLoader.getInstance().displayImage(model.ItemImageUrl_100, new BgViewAware(viewHolder.ll_itmimg), dispImage);

		return convertView;
	}

	private void addItemToCartList(ItemsDO model)
	{
		model.cartIconStatus=true;
		notifyDataSetChanged();
		cartDatabase.addCartData(new ItemsCartsDO(model.ItemId, model.ItemName, 1, model.ItemPrice, model.ItemImageUrl_100, model.ItemDesc,"true",model.Size));
		cartListListener.getCartList(model);

	}

	private class ViewHolder
	{
		TextView  txt_item,txt_basePrice,txt_qty,txt_itemSize,txt_offerdesc;
		LinearLayout row_griditem,ll_addBtn,ll_qty;
		FrameLayout ll_itmimg;
		ImageView img_plus,img_minus,img_favourite,img_offer;
	}


	public void registerCartListener(CartListListener cartListListener)
	{
		this.cartListListener=cartListListener;
	}


}
