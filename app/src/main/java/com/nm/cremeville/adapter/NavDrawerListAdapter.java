package com.nm.cremeville.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.NavDrawerItem;
import com.nm.cremeville.utilities.FontType;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private LayoutInflater inflator;
    ViewHolder holder;
    NavDrawerItem model;
    FontType fonttype;

    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;

        inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflator.inflate(R.layout.drawer_list_item, parent, false);

            holder.nav_imgicon = (ImageView) convertView.findViewById(R.id.drawericon);

            holder.nav_tvtitle = (TextView) convertView.findViewById(R.id.drawertitle);

            holder.tv_count = (TextView) convertView.findViewById(R.id.tv_count);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ViewGroup root = (ViewGroup) convertView.findViewById(R.id.sidebar);
        fonttype = new FontType(context, root);

        model = (NavDrawerItem) getItem(position);

        holder.nav_imgicon.setImageResource(model.icon);

        holder.nav_tvtitle.setText(model.title);

        if (!model.isCounterVisible) {
            holder.tv_count.setVisibility(View.GONE);
        } else {
            holder.tv_count.setVisibility(View.VISIBLE);

            holder.tv_count.setText(model.count);

        }

        return convertView;
    }

    private class ViewHolder {
        ImageView nav_imgicon;
        TextView nav_tvtitle, tv_count;
    }

}
