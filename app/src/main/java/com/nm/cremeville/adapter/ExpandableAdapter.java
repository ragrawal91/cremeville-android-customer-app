package com.nm.cremeville.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nm.cremeville.R;
import com.nm.cremeville.listener.FilterCheckboxListener;
import com.nm.cremeville.objects.FilterMetadataDO;
import com.nm.cremeville.objects.FlavourDO;

import java.util.ArrayList;

/**
 * Created by User on 18-Feb-16.
 */
public class ExpandableAdapter extends BaseExpandableListAdapter implements FilterCheckboxListener {

    Context context;
    ArrayList<FilterMetadataDO> domains;
    LayoutInflater inflater;
    FilterCheckboxListener filterCheckboxListener;
    GridViewAdapter gridViewAdapter;

    public ExpandableAdapter(Context context, ArrayList<FilterMetadataDO> domains)
    {
        this.context=context;

        this.domains=domains;

        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return domains.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return domains.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return domains.get(groupPosition).flavourlist;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        FilterMetadataDO expandableDomain= (FilterMetadataDO) getGroup(groupPosition);

        ViewHolder holder=new ViewHolder();

        if (convertView==null)
        {
            convertView=inflater.inflate(R.layout.parent_view,null);

            holder.tv_simple= (TextView) convertView.findViewById(R.id.tv_simple);

            convertView.setTag(holder);
        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }

        holder.tv_simple.setText(expandableDomain.parent);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
        final FilterMetadataDO expandableDomain= (FilterMetadataDO) getGroup(groupPosition);

        final ArrayList<FlavourDO> datalist= (ArrayList<FlavourDO>) getChild(groupPosition,childPosition);

        ChildViewHolder holder1=new ChildViewHolder();

        final ArrayList<String> temp=new ArrayList<>();

        if (convertView==null)
        {
            convertView=inflater.inflate(R.layout.grid_as_child,null);

            holder1.grd_child= (GridView) convertView.findViewById(R.id.grd_child);

            convertView.setTag(holder1);
        }
        else
        {
            holder1= (ChildViewHolder) convertView.getTag();
        }

        gridViewAdapter=new GridViewAdapter(context, datalist);
        gridViewAdapter.registerCheckboxFilterListerner(this);
        holder1.grd_child.setAdapter(gridViewAdapter);

        int totalHeight=0;

        int eachitem_height=0;

        for (int i=0;i<gridViewAdapter.getCount();i++)
        {
            LinearLayout relativeLayout= (LinearLayout) gridViewAdapter.getView(i,null,holder1.grd_child);

            CheckBox checkBox= (CheckBox) relativeLayout.getChildAt(0);

            checkBox.measure(0,0);

            totalHeight=totalHeight+checkBox.getMeasuredHeight();

            eachitem_height=checkBox.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params=holder1.grd_child.getLayoutParams();

        if (params!=null)
        {
            params.height=totalHeight/2;

            params.height=params.height+(4*eachitem_height);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition)
    {


    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    @Override
    public void getCheckBoxFilter(FlavourDO flavourDO)
    {
        filterCheckboxListener.getCheckBoxFilter(flavourDO);
    }

    class ViewHolder
    {
        TextView tv_simple;
    }
    class ChildViewHolder
    {
        GridView grd_child;
    }

    public void registerCheckboxFilterListerner(FilterCheckboxListener filterCheckboxListener)
    {
        this.filterCheckboxListener= filterCheckboxListener;
    }

    public void notifyGridForUncheck()
    {
        gridViewAdapter.notifyDataSetChanged();
    }

}
