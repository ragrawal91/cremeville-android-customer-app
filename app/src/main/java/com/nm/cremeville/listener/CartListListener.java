package com.nm.cremeville.listener;


import com.nm.cremeville.objects.ItemsDO;

/**
 * Created by win8 on 14-01-2016.
 */
public interface CartListListener {


    public void getCartList(ItemsDO model);

    public void getLocalDatabaseCartList();

    public void getTheIncreasedSubtotal(Double itemFinalPrice);

    public void getTheDecreasedSubtotal(Double itemFinalPrice);

    public void addFavouriteItem(ItemsDO model);

    public void removeFavouriteItem(ItemsDO model);

    public void getSizeFavouriteItemList(int Size);
}
