package com.nm.cremeville.objects;

public class ItemsCartsDO {
	

	public String ItemName="",ItemId="",CategoryId="";
	public String ItemDesc="",ItemImageUrl="",ItemSize="";
	public int ItemQty;
	public String ItemPrice="";
	public boolean bounceEffect=false;
	public String itemAdded="";

	public ItemsCartsDO(String ItemId, String ItemName, int ItemQty, String ItemPrice, String ItemImageUrl, String ItemDesc,String itemAdded,String ItemSize) {

		this.ItemId=ItemId;
		this.ItemName=ItemName;
		this.ItemQty=ItemQty;
		this.ItemPrice=ItemPrice;
		this.ItemImageUrl=ItemImageUrl;
		this.ItemDesc=ItemDesc;
		this.itemAdded=itemAdded;
		this.ItemSize=ItemSize;
	}

	public ItemsCartsDO() {

	}
}
