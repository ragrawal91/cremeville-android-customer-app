package com.nm.cremeville.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetOrderDO implements Parcelable{
	

	public String OrderId="",CustomerId="",StoreId="",AddressId="",Address="",Subtotal="",Tax="",DeliveryCharge="";
	public String Grandtotal="",CouponCode="",CouponStatus="",CouponAmount="",LoyaltyStatus="",LoyaltyAmount="";
	public String LoyaltyPoint="",PaymentMode="",OrderOrigin="",PaymentStatus="",TransactionId="";
	public String Status="";
	public String CreatedTime="";
	public String OrderTime="";

	public GetOrderDO(){}

	public ArrayList<ProductDO> productList=new ArrayList<>();


	protected GetOrderDO(Parcel in) {
		OrderId = in.readString();
		CustomerId = in.readString();
		StoreId = in.readString();
		AddressId = in.readString();
		Address = in.readString();
		Subtotal = in.readString();
		Tax = in.readString();
		DeliveryCharge = in.readString();
		Grandtotal = in.readString();
		CouponCode = in.readString();
		CouponStatus = in.readString();
		CouponAmount = in.readString();
		LoyaltyStatus = in.readString();
		LoyaltyAmount = in.readString();
		LoyaltyPoint = in.readString();
		PaymentMode = in.readString();
		OrderOrigin = in.readString();
		PaymentStatus = in.readString();
		TransactionId = in.readString();
		Status = in.readString();
		CreatedTime = in.readString();
		OrderTime = in.readString();
	}

	public static final Creator<GetOrderDO> CREATOR = new Creator<GetOrderDO>() {
		@Override
		public GetOrderDO createFromParcel(Parcel in) {
			return new GetOrderDO(in);
		}

		@Override
		public GetOrderDO[] newArray(int size) {
			return new GetOrderDO[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(OrderId);
		dest.writeString(CustomerId);
		dest.writeString(StoreId);
		dest.writeString(AddressId);
		dest.writeString(Address);
		dest.writeString(Subtotal);
		dest.writeString(Tax);
		dest.writeString(DeliveryCharge);
		dest.writeString(Grandtotal);
		dest.writeString(CouponCode);
		dest.writeString(CouponStatus);
		dest.writeString(CouponAmount);
		dest.writeString(LoyaltyStatus);
		dest.writeString(LoyaltyAmount);
		dest.writeString(LoyaltyPoint);
		dest.writeString(PaymentMode);
		dest.writeString(OrderOrigin);
		dest.writeString(PaymentStatus);
		dest.writeString(TransactionId);
		dest.writeString(Status);
		dest.writeString(CreatedTime);
		dest.writeString(OrderTime);
	}
}
