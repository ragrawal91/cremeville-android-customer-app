package com.nm.cremeville.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductDO implements Parcelable{
	

	public String ItemId="",ItemName="",ItemPrice="",Quantity="",Total="",ItemImage="",ItemSize="";
	public boolean StockStatus;

	public ProductDO(){}


	protected ProductDO(Parcel in) {
		ItemId = in.readString();
		ItemName = in.readString();
		ItemPrice = in.readString();
		Quantity = in.readString();
		Total = in.readString();
		ItemImage = in.readString();
		ItemSize = in.readString();
		StockStatus = in.readByte() != 0;
	}

	public static final Creator<ProductDO> CREATOR = new Creator<ProductDO>() {
		@Override
		public ProductDO createFromParcel(Parcel in) {
			return new ProductDO(in);
		}

		@Override
		public ProductDO[] newArray(int size) {
			return new ProductDO[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(ItemId);
		dest.writeString(ItemName);
		dest.writeString(ItemPrice);
		dest.writeString(Quantity);
		dest.writeString(Total);
		dest.writeString(ItemImage);
		dest.writeString(ItemSize);
		dest.writeByte((byte) (StockStatus ? 1 : 0));
	}
}
