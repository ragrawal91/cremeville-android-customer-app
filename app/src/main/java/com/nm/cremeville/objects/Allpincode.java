package com.nm.cremeville.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Goutam on 12/23/2015.
 */
public class Allpincode implements Parcelable
{
    public long pincode;
    public String pincode_value;

    public Allpincode()
    {

    }

    protected Allpincode(Parcel in) {
        pincode = in.readLong();
        pincode_value = in.readString();
    }

    public static final Creator<Allpincode> CREATOR = new Creator<Allpincode>() {
        @Override
        public Allpincode createFromParcel(Parcel in) {
            return new Allpincode(in);
        }

        @Override
        public Allpincode[] newArray(int size) {
            return new Allpincode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(pincode);
        dest.writeString(pincode_value);
    }
}
