package com.nm.cremeville.objects;


public class NavDrawerItem extends BaseDO
{

    public String title;
    public int icon;
    public String count = "0";
    // boolean to set visiblity of the counter
    public boolean isCounterVisible = false;

    public NavDrawerItem(){}

    public NavDrawerItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

    public NavDrawerItem(String title, int icon, boolean isCounterVisible, String count){
        this.title = title;
        this.icon = icon;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
    }

}
