package com.nm.cremeville.objects;

import java.util.ArrayList;

/**
 * Created by Goutam on 9/28/2015.
 */
public class SearchPlaceDomain extends BaseDO
{
    public String location_address="NA";
    public String areadesc="NA";
    public String pincode="NA";
    public String streetbldgname="NA";
    public String subareaname="NA";
    public String areaname="NA";
    public String routename="NA";
    public String cityname="NA";
    public String districtname="NA";
    public String countryname="NA";
    public String formatted_address="NA";
    public String statename="NA";
    public String location_longitude;
    public String location_latitude;


    public ArrayList<Allpincode> pincodelist;


    public SearchPlaceDomain() {

    }


}
