package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.AddressDO;
import com.nm.cremeville.objects.LoginDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetLoginHandler extends BaseHandler {
	ArrayList<LoginDO>  arrLoginDO;
	LoginDO	loginDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";



	public GetLoginHandler(String inputStream)
	{
		arrLoginDO =new ArrayList<LoginDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject CustomerObject=jsonObject.getJSONObject("customer");
					JSONObject loyaltyObject=CustomerObject.getJSONObject("loyalty");
					loginDO = new LoginDO();
					loginDO.AccessToken=jsonObject.getString("id");
					loginDO.CustomerId=CustomerObject.getString("id");
					loginDO.Password=CustomerObject.getString("otp");
					loginDO.CustomerPhone=CustomerObject.getString("phone");
					loginDO.CustomerEmail=CustomerObject.getString("email");
					loginDO.CustomerName=CustomerObject.getString("name");

					if(CustomerObject.has("loyalty"))
					{
						loginDO.TotalLoyaltyPoint=loyaltyObject.getString("totalPoints");
						loginDO.LoyaltyAmount=loyaltyObject.getString("amount");
					}
					if (jsonObject.has("addresses"))
					{
						JSONArray addressArray=jsonObject.getJSONArray("addresses");
						AddressDO addressDO;
						if(addressArray.length()>0)
						{
							for (int i = 0; i < addressArray.length(); i++)
							{
								JSONObject addressObject=addressArray.getJSONObject(i);
								addressDO=new AddressDO();
								addressDO.Addressid=addressObject.getString("id");
								addressDO.Address=addressObject.getString("streetAddress");
								addressDO.City=addressObject.getString("city");
								addressDO.AddressType=addressObject.getString("addressType");
								addressDO.HouseNumber=addressObject.getString("houseNumber");
								addressDO.Landmark=addressObject.getString("landMark");
								addressDO.Pincode=addressObject.getString("pincode");
								addressDO.Status=Status;
								loginDO.addressList.add(addressDO);
							}
						}
						else
						{
							addressDO=new AddressDO();
							addressDO.Status="No Address";
							loginDO.addressList.add(addressDO);
						}
					}
					errorMessage="null";
					loginDO.Status=Status;
					arrLoginDO.add(loginDO);
				}
			}
			else if (Status.equalsIgnoreCase("Login Failed"))
			{
				loginDO = new LoginDO();
				loginDO.Status=Status;
				errorMessage="null";
				arrLoginDO.add(loginDO);
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrLoginDO!=null&&arrLoginDO.size()>0)
			return arrLoginDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

