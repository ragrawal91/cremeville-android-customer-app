package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.utilities.CurrentDate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetExistingOrderDetailsHandler extends BaseHandler {
	ArrayList<GetOrderDO>  arrgetOrderDO;
	GetOrderDO	getOrderDO;
	String errorMessage="",Status="",Error="";
	CurrentDate currentDate;

	public GetExistingOrderDetailsHandler(String inputStream)
	{
		arrgetOrderDO =new ArrayList<GetOrderDO>();
		currentDate=new CurrentDate();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray = jObject.getJSONArray("data");
					if(jsonArray.length()>0)
					{
						for (int i = 0; i < jsonArray.length(); i++)
						{
							getOrderDO = new GetOrderDO();
							JSONObject jsonObject = jsonArray.getJSONObject(i);
							getOrderDO.OrderId = jsonObject.getString("id");
							getOrderDO.CustomerId = jsonObject.getString("customerId");
							getOrderDO.StoreId = jsonObject.getString("storeId");
							getOrderDO.AddressId = jsonObject.getString("addressId");
							getOrderDO.Address = jsonObject.getString("address");
							getOrderDO.Subtotal = jsonObject.getString("subTotal");
							getOrderDO.Tax = jsonObject.getString("tax");
							getOrderDO.DeliveryCharge = jsonObject.getString("deliveryCharges");
							getOrderDO.Grandtotal = jsonObject.getString("cartValue");
							getOrderDO.CouponCode = jsonObject.getString("couponCode");
							getOrderDO.CouponStatus = jsonObject.getString("couponStatus");
							getOrderDO.CouponAmount = jsonObject.getString("couponAmount");
							getOrderDO.LoyaltyStatus = jsonObject.getString("loyaltyStatus");
							getOrderDO.LoyaltyAmount = jsonObject.getString("loyaltyAmount");
							getOrderDO.LoyaltyPoint = jsonObject.getString("loyaltyPoint");
							getOrderDO.PaymentMode = jsonObject.getString("paymentMode");
							getOrderDO.OrderOrigin = jsonObject.getString("orderOriginId");
							getOrderDO.PaymentStatus = jsonObject.getString("paymentStatus");
							getOrderDO.TransactionId = jsonObject.getString("transactionId");
							getOrderDO.OrderId=jsonObject.getString("orderId");
							getOrderDO.OrderTime=currentDate.getOrdertime(jsonObject.getString("createdTime"));

							if (jsonObject.has("cartItems")) {
								JSONArray cartItemArray = jsonObject.getJSONArray("cartItems");
								ProductDO productDO;
								if (cartItemArray.length() > 0) {
									for (int j = 0; j < cartItemArray.length(); j++) {
										JSONObject cartItemObject = cartItemArray.getJSONObject(j);
										productDO = new ProductDO();
										productDO.ItemId = cartItemObject.getString("id");
										productDO.ItemName = cartItemObject.getString("name");
										productDO.ItemPrice = cartItemObject.getString("price");
										productDO.Quantity = cartItemObject.getString("quantity");
										productDO.Total = cartItemObject.getString("total");
										productDO.ItemImage = cartItemObject.getString("image");
										getOrderDO.productList.add(productDO);
									}
								}
							}
							getOrderDO.Status = Status;
							errorMessage = "null";
							arrgetOrderDO.add(getOrderDO);
						}
					}
					else
					{
						getOrderDO = new GetOrderDO();
						getOrderDO.Status = "No Order Found";
						errorMessage = "null";
						arrgetOrderDO.add(getOrderDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrgetOrderDO!=null&&arrgetOrderDO.size()>0)
			return arrgetOrderDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

