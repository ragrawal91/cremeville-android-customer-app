package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.ShowNotificationDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowNotificationsHandler extends BaseHandler {
	ArrayList<ShowNotificationDO>  arrShowNotificationDO;
	ShowNotificationDO	showNotificationDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public ShowNotificationsHandler(String inputStream)
	{
		arrShowNotificationDO =new ArrayList<ShowNotificationDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					for(int i=0; i<jsonArray.length();i++)
					{
						showNotificationDO = new ShowNotificationDO();
						JSONObject jsonObject=jsonArray.getJSONObject(i);
						showNotificationDO.header=jsonObject.getString("header");
						showNotificationDO.description=jsonObject.getString("description");
						showNotificationDO.couponCode=jsonObject.getString("couponCode");
						showNotificationDO.id=jsonObject.getString("id");
						showNotificationDO.Status=Status;
						errorMessage="null";
						arrShowNotificationDO.add(showNotificationDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrShowNotificationDO!=null&&arrShowNotificationDO.size()>0)
			return arrShowNotificationDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
