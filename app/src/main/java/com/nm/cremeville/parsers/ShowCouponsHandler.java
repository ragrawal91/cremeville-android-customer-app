package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.ShowCouponDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowCouponsHandler extends BaseHandler {
	ArrayList<ShowCouponDO>  arrShowCouponDO;
	ShowCouponDO	showCouponDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public ShowCouponsHandler(String inputStream)
	{
		arrShowCouponDO =new ArrayList<ShowCouponDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					for(int i=0; i<jsonArray.length();i++)
					{
						showCouponDO = new ShowCouponDO();
						JSONObject jsonObject=jsonArray.getJSONObject(i);
						showCouponDO.FinalValue=jsonObject.getString("code");
						showCouponDO.ImageUrl=jsonObject.getString("imageUrlM");
						showCouponDO.Status=Status;
						errorMessage="null";
						arrShowCouponDO.add(showCouponDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrShowCouponDO!=null&&arrShowCouponDO.size()>0)
			return arrShowCouponDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
