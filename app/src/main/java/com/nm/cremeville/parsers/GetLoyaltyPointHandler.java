package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.LoyaltyDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class GetLoyaltyPointHandler extends BaseHandler {
	ArrayList<LoyaltyDO>  arrLoyaltyDO;
	LoyaltyDO	loyaltyDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";



	public GetLoyaltyPointHandler(String inputStream)
	{
		arrLoyaltyDO =new ArrayList<LoyaltyDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject loyaltyObject=jsonObject.getJSONObject("loyalty");
					loyaltyDO = new LoyaltyDO();

					if(jsonObject.has("loyalty"))
					{
						loyaltyDO.TotalLoyaltyPoint=loyaltyObject.getString("totalPoints");
						loyaltyDO.LoyaltyAmount=loyaltyObject.getString("amount");
					}
					errorMessage="null";
					loyaltyDO.Status=Status;
					arrLoyaltyDO.add(loyaltyDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrLoyaltyDO!=null&&arrLoyaltyDO.size()>0)
			return arrLoyaltyDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

