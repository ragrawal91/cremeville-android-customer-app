package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.FilterMetadataDO;
import com.nm.cremeville.objects.FlavourDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFilterMetadataHandler extends BaseHandler {
	ArrayList<FilterMetadataDO>  arrFilterMetaDO;
	FilterMetadataDO	filterMetadataDO;
	FlavourDO flavourDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetFilterMetadataHandler(String inputStream)
	{
		arrFilterMetaDO =new ArrayList<FilterMetadataDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				filterMetadataDO = new FilterMetadataDO();
				if (jObject.has("data"))
				{
					filterMetadataDO.Status=Status;
					errorMessage="null";

					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject CategoryObject=jsonObject.getJSONObject("category");
					JSONArray FlavourArray=CategoryObject.getJSONArray("flavours");
					for(int i=0; i<FlavourArray.length();i++)
					{
						flavourDO=new FlavourDO();
						JSONObject flavourObject=FlavourArray.getJSONObject(i);
						flavourDO.Id=flavourObject.getString("id");
						flavourDO.Name=flavourObject.getString("name");
						flavourDO.Type=1;
						filterMetadataDO.flavourlist.add(flavourDO);

					}

					JSONArray BrandsArray=CategoryObject.getJSONArray("brands");
					for(int j=0; j<BrandsArray.length();j++)
					{
						flavourDO=new FlavourDO();
						JSONObject brandsObject=BrandsArray.getJSONObject(j);
						flavourDO.Id=brandsObject.getString("id");
						flavourDO.Name=brandsObject.getString("name");
						flavourDO.Type=2;
						filterMetadataDO.brandslist.add(flavourDO);
					}
					arrFilterMetaDO.add(filterMetadataDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrFilterMetaDO!=null&&arrFilterMetaDO.size()>0)
			return arrFilterMetaDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
