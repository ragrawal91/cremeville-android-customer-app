package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.CurrentAddressDO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetCurrentAddressHandler extends BaseHandler {
	ArrayList<CurrentAddressDO>  arrCurrAddressDO;
	CurrentAddressDO	currentAddressDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";
	private ArrayList<String> srchlocatypelist;


	public GetCurrentAddressHandler(String inputStream)
	{
		arrCurrAddressDO =new ArrayList<CurrentAddressDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject curraddrsjobj = new JSONObject(inputStream);

			if (curraddrsjobj.has("status"))
			{
				Status=curraddrsjobj.getString("status");
				errorMessage="null";

				if(Status.equalsIgnoreCase("OK"))
				{
					if(curraddrsjobj.has("results"))
					{
						currentAddressDO=new CurrentAddressDO();

						JSONArray addrsresultjsarry=curraddrsjobj.getJSONArray("results");

						for(int i=0;i<addrsresultjsarry.length();i++)
						{
							JSONObject addrsresultjobj=addrsresultjsarry.getJSONObject(i);

							if(addrsresultjobj.has("address_components"))
							{
								JSONArray addrscmpjsnArry=addrsresultjobj.getJSONArray("address_components");

								for(int j=0;j<addrscmpjsnArry.length();j++)
								{
									JSONObject addrscompjobj=addrscmpjsnArry.getJSONObject(j);

									if(addrscompjobj.has("types"))
									{
										JSONArray typesarry=addrscompjobj.getJSONArray("types");

										for(int count=0;count<typesarry.length();count++)
										{
											srchlocatypelist=new ArrayList<>();

											String types= (String) typesarry.get(count);

											srchlocatypelist.add(types);

											if(srchlocatypelist.size()>0)
											{
												for(String typevalue:srchlocatypelist)
												{
													if(typevalue.equals("street_number"))
													{

														currentAddressDO.streetbldgname= addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("route"))
													{

														currentAddressDO.routename= addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("sublocality_level_2"))
													{
														currentAddressDO.subareaname=addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("sublocality_level_1"))
													{
														currentAddressDO.areaname=addrscompjobj.getString("long_name");

													}

													if(typevalue.equals("administrative_area_level_1"))
													{
														currentAddressDO.statename=addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("administrative_area_level_2"))
													{
														currentAddressDO.districtname=addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("locality"))
													{
														currentAddressDO.cityname=addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("country"))
													{
														currentAddressDO.countryname=addrscompjobj.getString("long_name");
													}

													if(typevalue.equals("postal_code"))
													{
														currentAddressDO.pincode=addrscompjobj.getString("long_name");
													}

												}
											}
										}
									}

									if(addrsresultjobj.has("formatted_address"))
									{
										currentAddressDO.formatted_address=addrsresultjobj.getString("formatted_address");

									}

									arrCurrAddressDO.add(currentAddressDO);

								}
							}
						}

					}

				}
				else
				{
					errorMessage="error";
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}


	}


	@Override
	public Object getData() {
		if(arrCurrAddressDO!=null&&arrCurrAddressDO.size()>0)
			return arrCurrAddressDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
