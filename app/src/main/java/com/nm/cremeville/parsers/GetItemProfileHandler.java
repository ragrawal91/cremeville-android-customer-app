package com.nm.cremeville.parsers;


import com.nm.cremeville.R;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.objects.ReviewDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetItemProfileHandler extends BaseHandler {
	ArrayList<ItemsDO>  arrItemsDO;
	ItemsDO	itemDO;
	ReviewDO reviewDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetItemProfileHandler(String inputStream)
	{
		arrItemsDO =new ArrayList<ItemsDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					    JSONObject jsonObject=jObject.getJSONObject("data");
						itemDO = new ItemsDO();
						itemDO.ItemName=jsonObject.getString("name");
						itemDO.ItemId=jsonObject.getString("id");
						itemDO.CategoryId=jsonObject.getString("categoryId");
					    itemDO.OfferStatus=jsonObject.getBoolean("offerStatus");
						if(itemDO.OfferStatus)
						{
							itemDO.OfferIcon= R.mipmap.offers_icon;
						}
						else
						{
							itemDO.OfferIcon= R.mipmap.offers_white;
						}
						itemDO.ItemPrice=jsonObject.getString("price");
						itemDO.Size=jsonObject.getString("size");
						itemDO.ItemImageUrl_100=jsonObject.getString("imageURL_160");
						itemDO.ItemImageUrl_160=jsonObject.getString("imageURL_160");
						itemDO.ItemImageUrl_240=jsonObject.getString("imageURL_240");
						itemDO.ItemImageUrl_300=jsonObject.getString("imageURL_300");
						itemDO.ItemDesc=jsonObject.getString("description");
						itemDO.Avgrating=jsonObject.getInt("avgRating");

						JSONObject brandObject=jsonObject.getJSONObject("brand");
						itemDO.ItemBrandName=brandObject.getString("name");
						itemDO.ItemBrandId=brandObject.getString("id");
						itemDO.ItemBrandImgUrl_72=brandObject.getString("imageUrlM");
						itemDO.ItemBrandImgUrl_96=brandObject.getString("imageUrl");
						itemDO.ItemBrandDesc=brandObject.getString("description");

						JSONObject flavourObject=jsonObject.getJSONObject("flavour");
						itemDO.ItemFlavourName=flavourObject.getString("name");
						itemDO.ItemFlavourId=flavourObject.getString("id");

						JSONArray ReviewArray=jsonObject.getJSONArray("reviews");
						if(ReviewArray.length()>0)
						{
							for (int i = 0; i < ReviewArray.length(); i++)
							{
								reviewDO = new ReviewDO();
								JSONObject reviewObject = ReviewArray.getJSONObject(i);
								reviewDO.Rating = reviewObject.getString("rating");
								reviewDO.UserName = reviewObject.getString("userName");
								reviewDO.Comment = reviewObject.getString("feedback");
								itemDO.reviewlist.add(reviewDO);

							}
						}
						itemDO.Status=Status;
						errorMessage="null";
						arrItemsDO.add(itemDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrItemsDO!=null&&arrItemsDO.size()>0)
			return arrItemsDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
