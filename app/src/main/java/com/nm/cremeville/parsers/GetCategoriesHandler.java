package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.CategoryDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetCategoriesHandler extends BaseHandler {
	ArrayList<CategoryDO>  arrCategoryDO;
	CategoryDO	categoryDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetCategoriesHandler(String inputStream)
	{
		arrCategoryDO =new ArrayList<CategoryDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					for(int i=0; i<jsonArray.length();i++)
					{
						categoryDO = new CategoryDO();
						JSONObject jsonObject=jsonArray.getJSONObject(i);
						categoryDO.CatgName=jsonObject.getString("name");
						categoryDO.CatgId=jsonObject.getString("id");
						categoryDO.CatgImageUrl=jsonObject.getString("imageUrlM");
						categoryDO.Status=Status;
						errorMessage="null";
						arrCategoryDO.add(categoryDO);
					}
				}
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrCategoryDO!=null&&arrCategoryDO.size()>0)
			return arrCategoryDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
