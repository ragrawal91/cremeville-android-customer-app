package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.EditAddressDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class EditExistingAddressHandler extends BaseHandler {
	ArrayList<EditAddressDO>  arreditAddressDO;
	EditAddressDO	editAddressDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public EditExistingAddressHandler(String inputStream)
	{
		arreditAddressDO =new ArrayList<EditAddressDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				editAddressDO = new EditAddressDO();
				editAddressDO.Status=Status;
				errorMessage="null";
				arreditAddressDO.add(editAddressDO);
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arreditAddressDO!=null&&arreditAddressDO.size()>0)
			return arreditAddressDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
