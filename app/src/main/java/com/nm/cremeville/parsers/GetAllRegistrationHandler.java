package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.RegistrationDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class GetAllRegistrationHandler extends BaseHandler {
	ArrayList<RegistrationDO>  arrRegistrationDO;
	RegistrationDO	registrationDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetAllRegistrationHandler(String inputStream)
	{
		arrRegistrationDO =new ArrayList<RegistrationDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			int i;
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					registrationDO = new RegistrationDO();
					registrationDO.UserId=jsonObject.getString("id");
					registrationDO.UserName=jsonObject.getString("name");
					registrationDO.UserMobile=jsonObject.getString("phone");
					registrationDO.UserOTP=jsonObject.getString("otp");
					registrationDO.UserEmail=jsonObject.getString("email");
					errorMessage="null";
					registrationDO.Status=Status;
					arrRegistrationDO.add(registrationDO);
				}
			}
			else if (Status.equalsIgnoreCase("Already registered"))
			{
				registrationDO = new RegistrationDO();
				registrationDO.Status=Status;
				errorMessage="null";
				arrRegistrationDO.add(registrationDO);
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrRegistrationDO!=null&&arrRegistrationDO.size()>0)
			return arrRegistrationDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
