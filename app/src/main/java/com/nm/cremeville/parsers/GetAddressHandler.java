package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.AddressDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetAddressHandler extends BaseHandler {
	ArrayList<AddressDO>  arrAddressDO;
	AddressDO	addressDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetAddressHandler(String inputStream)
	{
		arrAddressDO =new ArrayList<AddressDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					if(jsonArray.length()>0)
					{

						for (int i = 0; i < jsonArray.length(); i++)
						{
							JSONObject addressObject = jsonArray.getJSONObject(i);
							addressDO = new AddressDO();
							addressDO.Addressid = addressObject.getString("id");
							addressDO.Address = addressObject.getString("streetAddress");
							addressDO.City = addressObject.getString("city");
							addressDO.AddressType = addressObject.getString("addressType");
							addressDO.HouseNumber = addressObject.getString("houseNumber");
							addressDO.Landmark = addressObject.getString("landMark");
							addressDO.Pincode = addressObject.getString("pincode");
							addressDO.Areaname=addressObject.getString("areaName");
							addressDO.StoreId=addressObject.getString("store_Id");

							errorMessage="null";
							addressDO.Status=Status;
							arrAddressDO.add(addressDO);
						}

					}
					else
					{
						addressDO = new AddressDO();
						errorMessage="null";
						addressDO.Status="No Address";
						arrAddressDO.add(addressDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrAddressDO!=null&&arrAddressDO.size()>0)
			return arrAddressDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
