package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.ProductDO;
import com.nm.cremeville.objects.StockCheckDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StockCheckHandler extends BaseHandler {
	ArrayList<StockCheckDO>  arrStockDO;
	StockCheckDO	stockCheckDO;
	String errorMessage="",Status="",Error="";
	ProductDO productDO;



	public StockCheckHandler(String inputStream)
	{
		arrStockDO =new ArrayList<StockCheckDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject OrderObject=jsonObject.getJSONObject("orderData");
					stockCheckDO = new StockCheckDO();
					stockCheckDO.Status=Status;
					JSONArray productArray=OrderObject.getJSONArray("cartItems");
					if(productArray.length()>0)
					{
						for (int j = 0; j < productArray.length(); j++)
						{
							productDO = new ProductDO();
							JSONObject productObject = productArray.getJSONObject(j);
							productDO.ItemId = productObject.getString("id");
							productDO.ItemName = productObject.getString("name");
							productDO.ItemPrice = productObject.getString("price");
							productDO.Quantity = productObject.getString("quantity");
							productDO.ItemImage = productObject.getString("image");
							productDO.ItemSize = productObject.getString("size");
							productDO.StockStatus = productObject.getBoolean("stockStatus");
							stockCheckDO.stockList.add(productDO);
						}
					}
					errorMessage="null";
					arrStockDO.add(stockCheckDO);
				}
			}
			else if(Status.equalsIgnoreCase("Invalid"))
			{

					stockCheckDO = new StockCheckDO();
					stockCheckDO.Status=Status;
					errorMessage="null";
					arrStockDO.add(stockCheckDO);
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrStockDO!=null&&arrStockDO.size()>0)
			return arrStockDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

