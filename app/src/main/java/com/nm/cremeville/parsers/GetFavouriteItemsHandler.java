package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.ItemsDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFavouriteItemsHandler extends BaseHandler {
	ArrayList<ItemsDO>  arrFavouriteItemsDO;
	ItemsDO	favouritesDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetFavouriteItemsHandler(String inputStream)
	{
		arrFavouriteItemsDO =new ArrayList<ItemsDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					if(jsonArray.length()>0)
					{
						for (int i = 0; i < jsonArray.length(); i++) {
							favouritesDO = new ItemsDO();
							JSONObject jsonObject = jsonArray.getJSONObject(i);
							JSONObject favouriteItemObject = jsonObject.getJSONObject("favouriteItem");

							favouritesDO.favouriteId = jsonObject.getString("id");

							favouritesDO.ItemId = favouriteItemObject.getString("id");
							favouritesDO.ItemName = favouriteItemObject.getString("name");
							favouritesDO.ItemPrice = favouriteItemObject.getString("price");
							favouritesDO.ItemImageUrl_100 = favouriteItemObject.getString("imageUrl");
							favouritesDO.Size = favouriteItemObject.getString("size");
							favouritesDO.Status = Status;
							errorMessage = "null";
							arrFavouriteItemsDO.add(favouritesDO);
						}
					}
					else
					{
						favouritesDO = new ItemsDO();
						favouritesDO.Status = "No Items";
						errorMessage = "null";
						arrFavouriteItemsDO.add(favouritesDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrFavouriteItemsDO!=null&&arrFavouriteItemsDO.size()>0)
			return arrFavouriteItemsDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
