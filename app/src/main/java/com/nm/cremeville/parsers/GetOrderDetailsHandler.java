package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.GetOrderDO;
import com.nm.cremeville.objects.ProductDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetOrderDetailsHandler extends BaseHandler {
	ArrayList<GetOrderDO>  arrgetOrderDO;
	GetOrderDO	getOrderDO;
	String errorMessage="",Status="",Error="";

	public GetOrderDetailsHandler(String inputStream)
	{
		arrgetOrderDO =new ArrayList<GetOrderDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					getOrderDO = new GetOrderDO();
					getOrderDO.OrderId=jsonObject.getString("id");
					getOrderDO.CustomerId=jsonObject.getString("customerId");
					getOrderDO.StoreId=jsonObject.getString("storeId");
					getOrderDO.AddressId=jsonObject.getString("addressId");
					getOrderDO.Address=jsonObject.getString("address");
					getOrderDO.Subtotal=jsonObject.getString("subTotal");
					getOrderDO.Tax=jsonObject.getString("tax");
					getOrderDO.DeliveryCharge=jsonObject.getString("deliveryCharges");
					getOrderDO.Grandtotal=jsonObject.getString("cartValue");
					getOrderDO.CouponCode=jsonObject.getString("couponCode");
					getOrderDO.CouponStatus=jsonObject.getString("couponStatus");
					getOrderDO.CouponAmount=jsonObject.getString("couponAmount");
					getOrderDO.LoyaltyStatus=jsonObject.getString("loyaltyStatus");
					getOrderDO.LoyaltyAmount=jsonObject.getString("loyaltyAmount");
					getOrderDO.LoyaltyPoint=jsonObject.getString("loyaltyPoint");
					getOrderDO.PaymentMode=jsonObject.getString("paymentMode");
					getOrderDO.OrderOrigin=jsonObject.getString("orderOriginId");
					getOrderDO.PaymentStatus=jsonObject.getString("paymentStatus");
					getOrderDO.TransactionId=jsonObject.getString("transactionId");

					if (jsonObject.has("cartItems"))
					{
						JSONArray cartItemArray=jsonObject.getJSONArray("cartItems");
						ProductDO productDO;
						if(cartItemArray.length()>0)
						{
							for (int i = 0; i < cartItemArray.length(); i++)
							{
								JSONObject cartItemObject=cartItemArray.getJSONObject(i);
								productDO=new ProductDO();
								productDO.ItemId=cartItemObject.getString("id");
								productDO.ItemName=cartItemObject.getString("name");
								productDO.ItemPrice=cartItemObject.getString("price");
								productDO.Quantity=cartItemObject.getString("quantity");
								productDO.Total=cartItemObject.getString("total");
								getOrderDO.productList.add(productDO);
							}
						}
					}
					getOrderDO.Status=Status;
					errorMessage="null";
					arrgetOrderDO.add(getOrderDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrgetOrderDO!=null&&arrgetOrderDO.size()>0)
			return arrgetOrderDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

