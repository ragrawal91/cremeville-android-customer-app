package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.PostOrderDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class PostOrderDetailsHandler extends BaseHandler {
	ArrayList<PostOrderDO>  arrpostOrderDO;
	PostOrderDO	postOrderDO;
	String errorMessage="",Status="",Error="";



	public PostOrderDetailsHandler(String inputStream)
	{
		arrpostOrderDO =new ArrayList<PostOrderDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			int i;
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					postOrderDO = new PostOrderDO();
					postOrderDO.OrderId=jsonObject.getString("id");
					postOrderDO.Status=Status;
					errorMessage="null";
					arrpostOrderDO.add(postOrderDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrpostOrderDO!=null&&arrpostOrderDO.size()>0)
			return arrpostOrderDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

