package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.PostReviewDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class PostReviewHandler extends BaseHandler {
	ArrayList<PostReviewDO>  arrPostReviewDO;
	PostReviewDO	postReviewDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public PostReviewHandler(String inputStream)
	{
		arrPostReviewDO =new ArrayList<PostReviewDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				postReviewDO = new PostReviewDO();
				postReviewDO.Status=Status;
				errorMessage="null";
				arrPostReviewDO.add(postReviewDO);
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrPostReviewDO!=null&&arrPostReviewDO.size()>0)
			return arrPostReviewDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
