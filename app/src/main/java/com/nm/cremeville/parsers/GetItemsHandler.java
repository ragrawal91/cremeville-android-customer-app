package com.nm.cremeville.parsers;


import android.view.View;

import com.nm.cremeville.R;
import com.nm.cremeville.objects.ItemsDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetItemsHandler extends BaseHandler {
	ArrayList<ItemsDO>  arrItemsDO;
	ItemsDO	itemDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetItemsHandler(String inputStream)
	{
		arrItemsDO =new ArrayList<ItemsDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					if(jsonArray.length()>0)
					{
						for(int i=0; i<jsonArray.length();i++)
						{
							itemDO = new ItemsDO();
							JSONObject jsonObject=jsonArray.getJSONObject(i);
							itemDO.ItemName=jsonObject.getString("name");
							itemDO.ItemId=jsonObject.getString("id");
							itemDO.OfferStatus=jsonObject.getBoolean("offerStatus");
							if(itemDO.OfferStatus)
							{
								itemDO.OfferIcon= R.mipmap.offers_icon;
								itemDO.offerDescStatus= View.VISIBLE;
							}
							else
							{
								itemDO.OfferIcon= R.mipmap.offers_white;
								itemDO.offerDescStatus= View.GONE;
							}
							itemDO.CategoryId=jsonObject.getString("categoryId");
							itemDO.ItemPrice=jsonObject.getString("price");
							itemDO.Size=jsonObject.getString("size");
							itemDO.ItemImageUrl_100=jsonObject.getString("imageURL_160");
							itemDO.ItemImageUrl_160=jsonObject.getString("imageURL_160");
							itemDO.ItemImageUrl_240=jsonObject.getString("imageURL_240");
							itemDO.ItemImageUrl_300=jsonObject.getString("imageURL_300");
							itemDO.offerDescription=jsonObject.getString("offerDescription");
							itemDO.ItemDesc=jsonObject.getString("description");
							itemDO.Avgrating=jsonObject.getInt("avgRating");
							itemDO.FavouriteStatus=jsonObject.getBoolean("favouriteStatus");
							itemDO.favouriteId=jsonObject.getString("favouriteId");


							JSONObject brandObject=jsonObject.getJSONObject("brand");
							itemDO.ItemBrandName=brandObject.getString("name");
							itemDO.ItemBrandId=brandObject.getString("id");
							itemDO.ItemBrandImgUrl_72=brandObject.getString("imageUrlM");
							itemDO.ItemBrandImgUrl_96=brandObject.getString("imageUrl");
							itemDO.ItemBrandDesc=brandObject.getString("description");


							JSONObject flavourObject=jsonObject.getJSONObject("flavour");
							itemDO.ItemFlavourName=flavourObject.getString("name");
							itemDO.ItemFlavourId=flavourObject.getString("id");

							itemDO.Status=Status;
							errorMessage="null";
							arrItemsDO.add(itemDO);
						}
					}
					else
					{
						itemDO = new ItemsDO();
						itemDO.Status="NOITEMS";
						errorMessage="null";
						arrItemsDO.add(itemDO);
					}
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object getData() {
		if(arrItemsDO!=null&&arrItemsDO.size()>0)
			return arrItemsDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}
}
