package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.UpdateNameDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class UpdateNameHandler extends BaseHandler {
	ArrayList<UpdateNameDO>  arrUpdateNameO;
	UpdateNameDO	updateNameDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public UpdateNameHandler(String inputStream)
	{
		arrUpdateNameO =new ArrayList<UpdateNameDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				updateNameDO = new UpdateNameDO();
				updateNameDO.Status=Status;
				errorMessage="null";
				arrUpdateNameO.add(updateNameDO);
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrUpdateNameO!=null&&arrUpdateNameO.size()>0)
			return arrUpdateNameO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
