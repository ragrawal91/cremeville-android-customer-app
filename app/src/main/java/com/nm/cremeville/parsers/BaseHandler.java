package com.nm.cremeville.parsers;


import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.StringUtils;
import com.nm.cremeville.webaccess.ServiceMethods;

import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public abstract class BaseHandler extends DefaultHandler
{
	public Boolean currentElement = false;
	public String currentValue = "";
	public abstract Object getData();
	public abstract String getErrorData();
	
	public static BaseHandler getParser(ServiceMethods wsMethod, String inputStream)
	{
		switch(wsMethod)
		{

			case WS_CATEGORIES:
				return new GetCategoriesHandler(inputStream);

			case WS_GET_ITEMS:
				return new GetItemsHandler(inputStream);

			case WS_GET_ITEM_PROFILE:
				return new GetItemProfileHandler(inputStream);

			case WS_GET_TAXES:
				return new GetTaxesHandler(inputStream);

			case WS_GET_LOGGED_IN:
				return new GetLoginHandler(inputStream);

			case WS_GET_FILTER_METADATA:
				return new GetFilterMetadataHandler(inputStream);

			case WS_CHECK_CUSTOMER_EXISTS:
				return new CustomerExistsHandler(inputStream);

			case WS_APPLY_FILTER:
				return new GetItemsHandler(inputStream);

			case WS_UPDATE_CUSTOMERNAME:
				return new UpdateNameHandler(inputStream);

			case WS_UPDATE_EMAIL:
				return new UpdateNameHandler(inputStream);

			case WS_ADD_FAVOURITE_ITEM:
				return new AddFavouritesHandler(inputStream);

			case WS_GET_FAVOURITE_ITEMS:
				return new GetFavouriteItemsHandler(inputStream);

			case WS_DELETE_FAVOURITE_ITEMS:
				return new DeleteFavouritesHandler(inputStream);

			case WS_DELETE_EXISTING_ADDRESS:
				return new DeleteExistingAddressHandler(inputStream);

			case WS_SAVE_ADDRESS_DETAILS:
				return new SaveAddressHandler(inputStream);

			case WS_EDIT_ADDRESS_DETAILS:
				return new EditExistingAddressHandler(inputStream);

			case WS_GET_ADDRESS_DETAILS:
				return new GetAddressHandler(inputStream);

			case WS_POST_ORDER_DETAILS:
				return new PostOrderDetailsHandler(inputStream);

			case WS_GET_CURRENT_ORDER:
				return new GetOrderDetailsHandler(inputStream);

			case WS_GET_EXISTING_ORDER:
				return new GetExistingOrderDetailsHandler(inputStream);

			case WS_APPLY_COUPON:
				return new ApplyCouponHandler(inputStream);

			case WS_GET_NEAREST_STORE:
				return new GetNearestStoreHandler(inputStream);

			case WS_GET_CURRENT_ADDRESS:
				return new GetCurrentAddressHandler(inputStream);

			case WS_POST_ITEM_REVIEW:
				return new PostReviewHandler(inputStream);

			case WS_SHOW_COUPON:
				return new ShowCouponsHandler(inputStream);

			case WS_SHOW_NOTIFICATIONS:
				return new ShowNotificationsHandler(inputStream);

			case WS_GET_CURRENT_LOYALTY_POINTS:
				return new GetLoyaltyPointHandler(inputStream);

			case WS_POST_STOCK_CHECK:
				return new StockCheckHandler(inputStream);
		}
		return null;
	}
	
	public String getStringFromInputStream(InputStream inputStream)
	{
	  if(inputStream != null)
	  {
	     BufferedReader br = null;
	     StringBuilder sb = new StringBuilder();
	     String line;
	    try 
	    {
	       br = new BufferedReader(new InputStreamReader(inputStream));
	       while ((line = br.readLine()) != null) 
	       {
	         sb.append(line);
	       }

	    }
	    catch (IOException e) 
	    {
	      e.printStackTrace();
	    } 
	    finally 
	    {
	      if (br != null) 
	      {
	        try 
	        {
	         br.close();
	        } 
	        catch (IOException e) 
	        {
	          e.printStackTrace();
	        }
	    }
	   }

	   return sb.toString();
	  }
	  else
	  {
	   return "";
	  }
	 }
	
	//Method to convert StringBuffer to String.
	public String sb2String(StringBuffer sb)
	{
		if(sb == null)
			return "";
		try
		{
			return sb.toString();
		}
		catch(Exception e)
		{
	   		LogUtils.error(this.getClass().getName(), "sb2String exception:" + e.getMessage());
		}
		return null;
	}
	
	//Method to convert StringBuffer to int.
	public int sb2Int(StringBuffer sb)
	{
		if (sb==null) 
			return 0;
		try 
		{
			return StringUtils.getInt(sb.toString());
		} 
		catch (Exception e) 
		{
			LogUtils.error(this.getClass().getName(), "sb2Int exception:"+e.getMessage() );
		}
		return 0;
	}
	
	//Method to convert StringBuffer to Long.
	public long sb2Long(StringBuffer sb)
	{
		if(sb == null)
			return 0;
		try
		{
			return Long.parseLong(sb.toString());
		}
		catch(Exception e)
		{
	   		LogUtils.error(this.getClass().getName(), "sb2Long exception:"+e.getMessage() );
		}
		return 0;
	}
	
	//Method to convert StringBuffer to Float.
	public float sb2Float(StringBuffer sb)
	{
		if(sb == null)
			return 0;
		try
		{
			return Float.parseFloat(sb.toString());
		}
		catch(Exception e)
		{
	   		LogUtils.error(this.getClass().getName(), "sb2Float exception:"+e.getMessage() );
		}
		return 0;
	}
		
	//Method to convert StringBuffer to double.
	public double sb2Double(StringBuffer sb)
	{
		if(sb == null)
			return 0;
		try
		{
			return Double.parseDouble(sb.toString());
		}
		catch(Exception e)
		{
	   		LogUtils.error(this.getClass().getName(), "sb2Long exception:"+e.getMessage() );
		}
		return 0;
	}

	//Method to convert StringBuffer to boolean.
	public boolean sb2Boolean(StringBuffer sb)
	{
		boolean result = false;
		
		if(sb == null)
			return result;
		
		if (sb.length() > 0)
		{
			try
			{
				result = sb.toString().equalsIgnoreCase("true");
			}
			catch(Exception e)
			{
		   		LogUtils.error(this.getClass().getName(), "sb2Boolean exception:"+e.getMessage() );
			}
			
		}
		return result;
	}
}
