package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.SaveAddressDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class SaveAddressHandler extends BaseHandler {
	ArrayList<SaveAddressDO>  arrSaveAddressDO;
	SaveAddressDO	saveAddressDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public SaveAddressHandler(String inputStream)
	{
		arrSaveAddressDO =new ArrayList<SaveAddressDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				saveAddressDO = new SaveAddressDO();
				saveAddressDO.Status=Status;
				errorMessage="null";
				arrSaveAddressDO.add(saveAddressDO);
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrSaveAddressDO!=null&&arrSaveAddressDO.size()>0)
			return arrSaveAddressDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
