package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.TaxesDO;
import com.nm.cremeville.utilities.CurrentDate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetTaxesHandler extends BaseHandler {
	ArrayList<TaxesDO>  arrTaxesDO;
	TaxesDO	taxesDO;
	int opstatus;
	String httpStatus="";
	CurrentDate currentDate;
	String errorMessage="",Status="",Error="";


	public GetTaxesHandler(String inputStream)
	{
		arrTaxesDO =new ArrayList<TaxesDO>();
		currentDate=new CurrentDate();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONArray jsonArray=jObject.getJSONArray("data");
					for(int i=0; i<jsonArray.length();i++)
					{
						taxesDO = new TaxesDO();
						JSONObject jsonObject=jsonArray.getJSONObject(i);
						taxesDO.vatTax=jsonObject.getDouble("vatTax");
						taxesDO.deliveryCharge=jsonObject.getDouble("deliveryCharge");
						taxesDO.minAmount=jsonObject.getDouble("minimumAmount");

						JSONObject timingObject=jsonObject.getJSONObject("timings");
						taxesDO.StartTime=currentDate.getOnlytime(timingObject.getString("startTime"));
						taxesDO.EndTime=currentDate.getOnlytime(timingObject.getString("endTime"));
						taxesDO.OrderingStatus=timingObject.getBoolean("orderTimingStatus");

						taxesDO.Status=Status;
						errorMessage="null";
						arrTaxesDO.add(taxesDO);
					}
				}
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrTaxesDO!=null&&arrTaxesDO.size()>0)
			return arrTaxesDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
