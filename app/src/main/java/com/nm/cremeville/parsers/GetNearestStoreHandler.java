package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.NearestStoreDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetNearestStoreHandler extends BaseHandler {
	ArrayList<NearestStoreDO>  arrNearestStoreDO;
	NearestStoreDO	nearestStoreDO;

	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public GetNearestStoreHandler(String inputStream)
	{
		arrNearestStoreDO =new ArrayList<NearestStoreDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{

				if (jObject.has("data"))
				{
					nearestStoreDO = new NearestStoreDO();

					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONArray storeArray=jsonObject.getJSONArray("stores");
					for(int i=0; i<storeArray.length();i++)
					{
						nearestStoreDO=new NearestStoreDO();
						JSONObject storeObject=storeArray.getJSONObject(i);
						nearestStoreDO.StoreId=storeObject.getString("id");
						nearestStoreDO.StoreName=storeObject.getString("name");
						arrNearestStoreDO.add(nearestStoreDO);
					}
					nearestStoreDO.Status=Status;
					errorMessage="null";
					arrNearestStoreDO.add(nearestStoreDO);
				}
			}
			else
			{
				nearestStoreDO = new NearestStoreDO();
				nearestStoreDO.Status=Status;
				errorMessage="null";
				arrNearestStoreDO.add(nearestStoreDO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrNearestStoreDO!=null&&arrNearestStoreDO.size()>0)
			return arrNearestStoreDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
