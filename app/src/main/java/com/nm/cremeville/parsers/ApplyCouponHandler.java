package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.CouponDO;
import com.nm.cremeville.objects.ProductDO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApplyCouponHandler extends BaseHandler {
	ArrayList<CouponDO>  arrCouponDO;
	CouponDO	couponDO;
	String errorMessage="",Status="",Error="";
	ProductDO productDO;



	public ApplyCouponHandler(String inputStream)
	{
		arrCouponDO =new ArrayList<CouponDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			int i;
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject OrderObject=jsonObject.getJSONObject("order");
					couponDO = new CouponDO();
					couponDO.Status=Status;
					couponDO.CustomerId=OrderObject.getString("customerId");
					couponDO.CouponCode=OrderObject.getString("couponCode");
					couponDO.DiscountAmount=OrderObject.getString("couponAmount");
					couponDO.FinalValue=OrderObject.getString("cartValue");
					couponDO.CouponDesc=OrderObject.getString("couponDescription");


					JSONArray productArray=OrderObject.getJSONArray("products");
					if(productArray.length()>0)
					{
						for (int j = 0; j < productArray.length(); j++)
						{
							productDO = new ProductDO();
							JSONObject productObject = productArray.getJSONObject(j);
							productDO.ItemId = productObject.getString("productId");
							productDO.ItemName = productObject.getString("itemName");
							productDO.ItemPrice = productObject.getString("value");
							productDO.Quantity = productObject.getString("itemQty");
							productDO.ItemImage = productObject.getString("imgUrl");
							productDO.ItemSize = productObject.getString("size");
							couponDO.productList.add(productDO);
						}
					}
					errorMessage="null";
					arrCouponDO.add(couponDO);
				}
			}
			else if(Status.equalsIgnoreCase("Invalid"))
			{

					couponDO = new CouponDO();
					couponDO.Status=Status;
					errorMessage="null";
					arrCouponDO.add(couponDO);
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrCouponDO!=null&&arrCouponDO.size()>0)
			return arrCouponDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

