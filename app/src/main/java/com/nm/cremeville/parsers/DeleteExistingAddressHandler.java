package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.AddFavouriteDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class DeleteExistingAddressHandler extends BaseHandler {
	ArrayList<AddFavouriteDO>  arrAddFavouriteDO;
	AddFavouriteDO	addFavouriteDO;
	int opstatus;
	String httpStatus="";
	String errorMessage="",Status="",Error="";


	public DeleteExistingAddressHandler(String inputStream)
	{
		arrAddFavouriteDO =new ArrayList<AddFavouriteDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("status");
			if (Status.equalsIgnoreCase("success"))
			{
				addFavouriteDO = new AddFavouriteDO();
				addFavouriteDO.Status=Status;
				errorMessage="null";
				arrAddFavouriteDO.add(addFavouriteDO);
			}

			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Object getData() {
		if(arrAddFavouriteDO!=null&&arrAddFavouriteDO.size()>0)
			return arrAddFavouriteDO;
		else 
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}
