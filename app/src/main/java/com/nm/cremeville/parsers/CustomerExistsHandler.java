package com.nm.cremeville.parsers;


import com.nm.cremeville.objects.CheckRegistrationDO;

import org.json.JSONObject;

import java.util.ArrayList;

public class CustomerExistsHandler extends BaseHandler {
	ArrayList<CheckRegistrationDO>  arrChkRegDO;
	CheckRegistrationDO	checkRegistrationDO;
	String errorMessage="",Status="",Error="";



	public CustomerExistsHandler(String inputStream)
	{
		arrChkRegDO =new ArrayList<CheckRegistrationDO>();
		getInputStream(inputStream);
	}

	private void getInputStream(String inputStream) {
		try {
			int i;
			JSONObject jObject = new JSONObject(inputStream);
			Status = jObject.getString("message");
			if (Status.equalsIgnoreCase("New User"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject CustomerObject=jsonObject.getJSONObject("customer");
					checkRegistrationDO = new CheckRegistrationDO();
					checkRegistrationDO.Message=jObject.getString("message");
					checkRegistrationDO.CustomerId=CustomerObject.getString("id");
					checkRegistrationDO.OTPMessage=CustomerObject.getString("otp");
					checkRegistrationDO.CustomerPhone=CustomerObject.getString("phone");
					checkRegistrationDO.OtpStatus=CustomerObject.getString("otpStatus");
					errorMessage="null";
					arrChkRegDO.add(checkRegistrationDO);
				}
			}
			else if (Status.equalsIgnoreCase("Existed User"))
			{
				if (jObject.has("data"))
				{
					JSONObject jsonObject=jObject.getJSONObject("data");
					JSONObject CustomerObject=jsonObject.getJSONObject("customer");
					checkRegistrationDO = new CheckRegistrationDO();
					checkRegistrationDO.Message=jObject.getString("message");
					checkRegistrationDO.CustomerId=CustomerObject.getString("id");
					checkRegistrationDO.OTPMessage=CustomerObject.getString("otp");
					checkRegistrationDO.CustomerPhone=CustomerObject.getString("phone");
					checkRegistrationDO.OtpStatus=CustomerObject.getString("otpStatus");
					checkRegistrationDO.CustomerName=CustomerObject.getString("name");
					errorMessage="null";
					arrChkRegDO.add(checkRegistrationDO);
				}
			}
			else
			{
				errorMessage="error";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Object getData() {
		if(arrChkRegDO!=null&&arrChkRegDO.size()>0)
			return arrChkRegDO;
		else
			return 0;
	}

	@Override
	public String getErrorData() {
		return errorMessage;
	}

}

