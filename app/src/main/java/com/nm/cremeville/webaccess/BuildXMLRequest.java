package com.nm.cremeville.webaccess;


import com.nm.cremeville.objects.ItemsCartsDO;
import com.nm.cremeville.objects.ItemsDO;
import com.nm.cremeville.utilities.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BuildXMLRequest {

	public static String saveAddressDetailsToServer(String customerID,String houseNumber,String address,String landmark,String city,
													String pincode,String AreaName,String storeId,String addressKey) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("addressType", addressKey);
			jsonObject.put("customerId", customerID);
			jsonObject.put("houseNumber", houseNumber);
			jsonObject.put("streetAddress", address);
			jsonObject.put("landMark", landmark);
			jsonObject.put("city", city);
			jsonObject.put("pincode", pincode);
			jsonObject.put("areaName", AreaName);
			jsonObject.put("store_Id", storeId);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogUtils.error(BuildXMLRequest.class.getSimpleName(), "Address Details :"+jsonObject.toString());
		return jsonObject.toString();
	}

	public static String SaveEditedAddressDetailsToServer(String houseNumber,String address,String landmark,
													String pincode,String AreaName) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("houseNumber", houseNumber);
			jsonObject.put("streetAddress", address);
			jsonObject.put("landMark", landmark);
			jsonObject.put("pincode", pincode);
			jsonObject.put("areaName", AreaName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}




	public static String postOrderDetails(String customerID, String storeId, String addressId, String deliveryAddress,
										  ArrayList<ItemsCartsDO> cartList, double subtotal, double vatTax, String deliveryValue,
										  double grandTotal, String couponCode, String couponStatus, String couponAmount,
										  String loyaltyStatus, String loyaltyAmount, String loyaltyPoints, String paymentMode,
										  String orderOriginId, String transactionId, String paymentStatus,String OrderStatus)
	{
		JSONObject jsonObject=new JSONObject();
		JSONArray cartArray=new JSONArray();
		try {
			jsonObject.put("customerId",customerID);
			jsonObject.put("storeId", storeId);
			jsonObject.put("addressId", addressId);
			jsonObject.put("address", deliveryAddress);
			jsonObject.put("subTotal", subtotal);
			jsonObject.put("tax", vatTax);
			jsonObject.put("deliveryCharges", deliveryValue);
			jsonObject.put("cartValue", grandTotal);
			jsonObject.put("couponCode", couponCode);
			jsonObject.put("couponStatus", couponStatus);
			jsonObject.put("couponAmount", couponAmount);
			jsonObject.put("loyaltyStatus", loyaltyStatus);
			jsonObject.put("loyaltyAmount", loyaltyAmount);
			jsonObject.put("loyaltyPoint", loyaltyPoints);
			jsonObject.put("paymentMode", paymentMode);
			jsonObject.put("orderOriginId", orderOriginId);
			jsonObject.put("paymentStatus", paymentStatus);
			jsonObject.put("transactionId", transactionId);
			jsonObject.put("orderStatus", OrderStatus);


			for(int i=0;i<cartList.size();i++)
			{
				JSONObject cartObject=new JSONObject();
				ItemsCartsDO model=cartList.get(i);
				cartObject.put("id",model.ItemId);
				cartObject.put("name",model.ItemName);
				cartObject.put("price",model.ItemPrice);
				cartObject.put("quantity",model.ItemQty);
				cartObject.put("image",model.ItemImageUrl);
				cartObject.put("size",model.ItemSize);
				int MultipliedPrice=model.ItemQty*Integer.parseInt(model.ItemPrice);
				cartObject.put("total",String.valueOf(MultipliedPrice));
				cartArray.put(cartObject);
				jsonObject.put("cartItems",cartArray);
			}


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogUtils.error(BuildXMLRequest.class.getSimpleName(), "Order Details :"+jsonObject.toString());
		return jsonObject.toString();
	}



	public static String getLogin(String userOTP,String userFirstMobile) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("phone", userFirstMobile);
			jsonObject.put("password", userOTP);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String updateEmailAtServer(String email,String EditedCustomerName) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("email", email);
			jsonObject.put("name", EditedCustomerName);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}



	public static String applyCoupon(String customerID, double grandTotal, String couponCode, ArrayList<ItemsCartsDO> cartList, String deliveryValue, String loyaltyAmount) {
		JSONObject jsonObject=new JSONObject();
		JSONArray cartArray=new JSONArray();

		try {
			jsonObject.put("couponCode", couponCode);
			jsonObject.put("cartValue", grandTotal);
			jsonObject.put("customerId", customerID);
			if(deliveryValue.equals("free"))
			{
				jsonObject.put("deliveryCharges", 0);
			}
			else
			{
				jsonObject.put("deliveryCharges", Double.valueOf(deliveryValue));
			}
			jsonObject.put("loyaltyAmount", Double.valueOf(loyaltyAmount));

			for(int i=0;i<cartList.size();i++)
			{
				JSONObject cartObject=new JSONObject();
				ItemsCartsDO model=cartList.get(i);
				cartObject.put("productId",model.ItemId);
				cartObject.put("itemName",model.ItemName);
				cartObject.put("itemQty",model.ItemQty);
				cartObject.put("value",Integer.valueOf(model.ItemPrice));
				cartObject.put("imgUrl",model.ItemImageUrl);
				cartObject.put("itemDesc",model.ItemDesc);
				cartObject.put("addStatus","true");
				cartObject.put("size",model.ItemSize);
				cartArray.put(cartObject);
				jsonObject.put("products",cartArray);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogUtils.error(BuildXMLRequest.class.getSimpleName(), "Coupon Details :"+jsonObject.toString());
		return jsonObject.toString();
	}


	public static String applystockCheck(ArrayList<ItemsCartsDO> cartList, String storeID) {
		JSONObject jsonObject=new JSONObject();
		JSONArray cartArray=new JSONArray();

		try {
			jsonObject.put("storeId", storeID);
			for(int i=0;i<cartList.size();i++)
			{
				JSONObject cartObject=new JSONObject();
				ItemsCartsDO model=cartList.get(i);
				cartObject.put("id",model.ItemId);
				cartObject.put("name",model.ItemName);
				cartObject.put("quantity",model.ItemQty);
				cartObject.put("price",Integer.valueOf(model.ItemPrice));
				cartObject.put("image",model.ItemImageUrl);
				int MultipliedPrice=model.ItemQty*Integer.parseInt(model.ItemPrice);
				cartObject.put("total",String.valueOf(MultipliedPrice));
				cartObject.put("size",model.ItemSize);
				cartArray.put(cartObject);
				jsonObject.put("cartItems",cartArray);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LogUtils.error(BuildXMLRequest.class.getSimpleName(), "stock Check Details :"+jsonObject.toString());
		return jsonObject.toString();
	}



	public static String updateCustomerName(String userName) {
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("name", userName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String addFavouritesToServer(String customerID, ItemsDO model)
	{
		JSONObject jsonObject=new JSONObject();
		JSONObject jObject=new JSONObject();
		try {
			jsonObject.put("createdBy", customerID);
			jsonObject.put("favouriteType", "Item");
			jObject.put("name",model.ItemName);
			jObject.put("id",model.ItemId);
			jObject.put("price",model.ItemPrice);
			jObject.put("imageUrl",model.ItemImageUrl_100);
			jObject.put("size",model.Size);
			jsonObject.put("favouriteItem",jObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String postReview(float rating, String itemId, String customerID, String comment, String item,String CustomerName) {
		JSONObject jsonObject=new JSONObject();

		try {
			jsonObject.put("rating", rating);
			jsonObject.put("ratingFor", itemId);
			jsonObject.put("createdBy",customerID);
			jsonObject.put("feedback",comment);
			jsonObject.put("ratingType",item);
			jsonObject.put("userName",CustomerName);


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String getCurrentOrder()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getCurrentLoyaltyPoints()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String deleteTheExistingAddress()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getCoupons()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getNotifications()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getCompleteAddress()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}



	public static String getNearestStoreInfo()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getExistingOrders()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String checkUserExist()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getAddressDetails()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getAllCategories()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}


	public static String getTaxes()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}



	public static String getItems()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getFavouriteItems()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String removeFavouriteItemFromServer()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getFilterMetadata()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getApplyFilter()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}

	public static String getItemsProfile()
	{
		StringBuilder stringBuilder = new StringBuilder();
		return stringBuilder.toString();
	}


}
