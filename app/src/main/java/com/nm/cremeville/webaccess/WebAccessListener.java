package com.nm.cremeville.webaccess;

public interface WebAccessListener
{
	void dataDownloaded(Response data);
}
