package com.nm.cremeville.webaccess;


import com.nm.cremeville.utilities.AppConstants;

public class ServiceURLs
{

	// Test URL
	//public static String HOST_URL_1 = "http://139.162.56.235:4848/api/";

    //Live URL
	public static String HOST_URL_1 = "http://139.162.61.196:4848/api/";

	public static String HOST_URL_2="http://maps.googleapis.com/maps/api/";
	//Method Names

	public static String GET_CATEGORIES 	= "Categories";
	public static String GET_ITEMS  	= "Products?filter=";
	public static String GET_ITEM_PROFILE  	= "Products/";
	public static String GET_TAXES  	= "Taxes";
	public static String GET_LOGIN   	= "Customers/login";
	public static String GET_FILTER_METADATA  	= "Categories/getFlavoursAndBrands?categoryId=";
	public static String APPLY_FILTER  	= "Products?filter=";
	public static String CUSTOMER_EXISTS="Customers/userExists?phone=";
	public static String UPDATE_CUSTOMER_NAME ="Customers/";
	public static String ADD_FAVOURITE_ITEMSTO_SERVER ="Favourites?access_token=";
	public static String GET_FAVOURITE_ITEMS="Favourites?filter=";
	public static String DELETE_FAVOURITE_ITEMS="Favourites/";
	public static String ADD_ADDRESS_TO_SERVER="Customers/";
	public static String GET_ADDRESS_FROM_SERVER="Addresses?filter=";
	public static String POST_ORDER_DETAILS="Orders?access_token=";
	public static String GET_CURRENT_ORDER_DETAILS="Orders/";
	public static String GET_EXISTING_ORDER_DETAILS="Orders?filter=";
	public static String APPLY_COUPON="Coupons/applyCoupon";
	public static String GET_NEAREST_STORE="Stores/getNearestStoreInformation?";
	public static String POST_ITEM_REVIEW="Reviews";
	public static String SHOW_COUPONS="Coupons";
	public static String SHOW_NOTIFICATIONS="Notifications";
	public static String DELETE_EXISTING_ADDRESS="Addresses/";
	public static String EDIT_EXISTING_ADDRESS="Addresses/";
	public static String GET_LOYALTY_POINTS="Customers/";
	public static String UPDATE_EMAIL="Customers/";
	public static String STOCK_CHECK="Items/checkStock";

	public static String GET_CURRENT_ADDRESS="geocode/json?latlng=";



	public static int getRequestType(ServiceMethods wsMethod)
	{
		switch (wsMethod) 
		{
			case WS_CATEGORIES:
				return AppConstants.GET;

			case WS_GET_ITEMS:
				return AppConstants.GET;

			case WS_GET_ITEM_PROFILE:
				return AppConstants.GET;

			case WS_GET_TAXES:
				return AppConstants.GET;

			case WS_GET_LOGGED_IN:
				return AppConstants.POST;

			case WS_GET_FILTER_METADATA:
				return AppConstants.GET;

			case WS_APPLY_FILTER:
				return AppConstants.GET;

			case WS_CHECK_CUSTOMER_EXISTS:
				return AppConstants.GET;

			case WS_UPDATE_CUSTOMERNAME:
				return AppConstants.PUT;

			case WS_UPDATE_EMAIL:
				return AppConstants.PUT;

			case WS_ADD_FAVOURITE_ITEM:
				return AppConstants.POST;

			case WS_GET_FAVOURITE_ITEMS:
				return AppConstants.GET;

			case WS_DELETE_FAVOURITE_ITEMS:
				return AppConstants.DELETE;

			case WS_DELETE_EXISTING_ADDRESS:
				return AppConstants.DELETE;

			case WS_EDIT_ADDRESS_DETAILS:
				return AppConstants.PUT;

			case WS_SAVE_ADDRESS_DETAILS:
				return AppConstants.POST;

			case WS_GET_ADDRESS_DETAILS:
				return AppConstants.GET;

			case WS_POST_ORDER_DETAILS:
				return AppConstants.POST;

			case WS_GET_CURRENT_ORDER:
				return AppConstants.GET;

			case WS_GET_EXISTING_ORDER:
				return AppConstants.GET;

			case WS_APPLY_COUPON:
				return AppConstants.POST;

			case WS_GET_NEAREST_STORE:
				return AppConstants.GET;

			case WS_GET_CURRENT_ADDRESS:
				return AppConstants.GET;

			case WS_POST_ITEM_REVIEW:
				return AppConstants.POST;

			case WS_SHOW_COUPON:
				return AppConstants.GET;

			case WS_SHOW_NOTIFICATIONS:
				return AppConstants.GET;

			case WS_GET_CURRENT_LOYALTY_POINTS:
				return AppConstants.GET;

			case WS_POST_STOCK_CHECK:
				return AppConstants.POST;

		}
		return 0;
	}

	public static String getContentType(ServiceMethods wsMethod)
	{
		switch (wsMethod) 
		{
			case WS_CATEGORIES:
				return AppConstants.ContentTypeJson;

			case WS_GET_ITEMS:
				return AppConstants.ContentTypeJson;

			case WS_GET_ITEM_PROFILE:
				return AppConstants.ContentTypeJson;

			case WS_GET_TAXES:
				return AppConstants.ContentTypeJson;

			case WS_GET_LOGGED_IN:
				return AppConstants.ContentTypeJson;

			case WS_GET_FILTER_METADATA:
				return AppConstants.ContentTypeJson;

			case WS_APPLY_FILTER:
				return AppConstants.ContentTypeJson;

			case WS_CHECK_CUSTOMER_EXISTS:
				return AppConstants.ContentTypeJson;

			case WS_UPDATE_CUSTOMERNAME:
				return AppConstants.ContentTypeJson;

			case WS_UPDATE_EMAIL:
				return AppConstants.ContentTypeJson;

			case WS_ADD_FAVOURITE_ITEM:
				return AppConstants.ContentTypeJson;

			case WS_GET_FAVOURITE_ITEMS:
				return AppConstants.ContentTypeJson;

			case WS_DELETE_FAVOURITE_ITEMS:
				return AppConstants.ContentTypeJson;

			case WS_DELETE_EXISTING_ADDRESS:
				return AppConstants.ContentTypeJson;

			case WS_SAVE_ADDRESS_DETAILS:
				return AppConstants.ContentTypeJson;

			case WS_EDIT_ADDRESS_DETAILS:
				return AppConstants.ContentTypeJson;

			case WS_GET_ADDRESS_DETAILS:
				return AppConstants.ContentTypeJson;

			case WS_POST_ORDER_DETAILS:
				return AppConstants.ContentTypeJson;

			case WS_GET_CURRENT_ORDER:
				return AppConstants.ContentTypeJson;

			case WS_GET_EXISTING_ORDER:
				return AppConstants.ContentTypeJson;

			case WS_APPLY_COUPON:
				return AppConstants.ContentTypeJson;

			case WS_GET_NEAREST_STORE:
				return AppConstants.ContentTypeJson;

			case WS_GET_CURRENT_ADDRESS:
				return AppConstants.ContentTypeJson;

			case WS_POST_ITEM_REVIEW:
				return AppConstants.ContentTypeJson;

			case WS_SHOW_COUPON:
				return AppConstants.ContentTypeJson;

			case WS_SHOW_NOTIFICATIONS:
				return AppConstants.ContentTypeJson;

			case WS_GET_CURRENT_LOYALTY_POINTS:
				return AppConstants.ContentTypeJson;

			case WS_POST_STOCK_CHECK:
				return AppConstants.ContentTypeJson;

		}
		return "NA";
	}
	
	public static String getRequestedURL(ServiceMethods wsMethod)
	{
		switch(wsMethod)
		{
			case WS_CATEGORIES:
				return HOST_URL_1+GET_CATEGORIES;

			case WS_GET_ITEMS:
				return HOST_URL_1+GET_ITEMS;

			case WS_GET_ITEM_PROFILE:
				return HOST_URL_1+GET_ITEM_PROFILE;

			case WS_GET_TAXES:
				return HOST_URL_1+GET_TAXES;

			case WS_GET_LOGGED_IN:
				return HOST_URL_1+GET_LOGIN;

			case WS_CHECK_CUSTOMER_EXISTS:
				return HOST_URL_1+CUSTOMER_EXISTS;

			case WS_GET_FILTER_METADATA:
				return HOST_URL_1+GET_FILTER_METADATA;

			case WS_APPLY_FILTER:
				return HOST_URL_1+APPLY_FILTER;

			case WS_UPDATE_CUSTOMERNAME:
				return HOST_URL_1+ UPDATE_CUSTOMER_NAME;

			case WS_UPDATE_EMAIL:
				return HOST_URL_1+ UPDATE_EMAIL;

			case WS_ADD_FAVOURITE_ITEM:
				return HOST_URL_1+ ADD_FAVOURITE_ITEMSTO_SERVER;

			case WS_GET_FAVOURITE_ITEMS:
				return HOST_URL_1+GET_FAVOURITE_ITEMS;

			case WS_DELETE_FAVOURITE_ITEMS:
				return HOST_URL_1+DELETE_FAVOURITE_ITEMS;

			case WS_DELETE_EXISTING_ADDRESS:
				return HOST_URL_1+DELETE_EXISTING_ADDRESS;

			case WS_SAVE_ADDRESS_DETAILS:
				return HOST_URL_1+ADD_ADDRESS_TO_SERVER;

			case WS_EDIT_ADDRESS_DETAILS:
				return HOST_URL_1+EDIT_EXISTING_ADDRESS;

			case WS_GET_ADDRESS_DETAILS:
				return HOST_URL_1+GET_ADDRESS_FROM_SERVER;

			case WS_POST_ORDER_DETAILS:
				return HOST_URL_1+POST_ORDER_DETAILS;

			case WS_GET_CURRENT_ORDER:
				return HOST_URL_1+GET_CURRENT_ORDER_DETAILS;

			case WS_GET_EXISTING_ORDER:
				return HOST_URL_1+GET_EXISTING_ORDER_DETAILS;

			case WS_APPLY_COUPON:
				return HOST_URL_1+APPLY_COUPON;

			case WS_GET_NEAREST_STORE:
				return HOST_URL_1+GET_NEAREST_STORE;

			case WS_GET_CURRENT_ADDRESS:
				return HOST_URL_2+GET_CURRENT_ADDRESS;

			case WS_POST_ITEM_REVIEW:
				return HOST_URL_1+POST_ITEM_REVIEW;

			case WS_SHOW_COUPON:
				return HOST_URL_1+SHOW_COUPONS;

			case WS_SHOW_NOTIFICATIONS:
				return HOST_URL_1+SHOW_NOTIFICATIONS;

			case WS_GET_CURRENT_LOYALTY_POINTS:
				return HOST_URL_1+GET_LOYALTY_POINTS;

			case WS_POST_STOCK_CHECK:
				return HOST_URL_1+STOCK_CHECK;
		}
		return getRequestedURL(wsMethod);
	}

	public static String getRequestedURL(ServiceMethods wsMethod, String parameters)
	{
		switch (wsMethod)
		{
			case WS_CATEGORIES:
				return String.format(HOST_URL_1 +GET_CATEGORIES,parameters);

			case WS_GET_ITEMS:
				return String.format(HOST_URL_1 +GET_ITEMS,parameters);

			case WS_GET_ITEM_PROFILE:
				return String.format(HOST_URL_1 +GET_ITEM_PROFILE,parameters);

			case WS_GET_TAXES:
				return String.format(HOST_URL_1 +GET_TAXES,parameters);

			case WS_GET_LOGGED_IN:
				return String.format(HOST_URL_1 +GET_LOGIN,parameters);

			case WS_CHECK_CUSTOMER_EXISTS:
				return String.format(HOST_URL_1 +CUSTOMER_EXISTS,parameters);

			case WS_GET_FILTER_METADATA:
				return String.format(HOST_URL_1 +GET_FILTER_METADATA,parameters);

			case WS_APPLY_FILTER:
				return String.format(HOST_URL_1 +APPLY_FILTER,parameters);

			case WS_UPDATE_CUSTOMERNAME:
				return String.format(HOST_URL_1 + UPDATE_CUSTOMER_NAME,parameters);

			case WS_UPDATE_EMAIL:
				return String.format(HOST_URL_1 + UPDATE_EMAIL,parameters);

			case WS_ADD_FAVOURITE_ITEM:
				return String.format(HOST_URL_1 + ADD_FAVOURITE_ITEMSTO_SERVER,parameters);

			case WS_GET_FAVOURITE_ITEMS:
				return String.format(HOST_URL_1 +GET_FAVOURITE_ITEMS,parameters);

			case WS_DELETE_FAVOURITE_ITEMS:
				return String.format(HOST_URL_1 +DELETE_FAVOURITE_ITEMS,parameters);

			case WS_DELETE_EXISTING_ADDRESS:
				return String.format(HOST_URL_1 +DELETE_EXISTING_ADDRESS,parameters);

			case WS_SAVE_ADDRESS_DETAILS:
				return String.format(HOST_URL_1 +ADD_ADDRESS_TO_SERVER,parameters);

			case WS_EDIT_ADDRESS_DETAILS:
				return String.format(HOST_URL_1 +EDIT_EXISTING_ADDRESS,parameters);

			case WS_GET_ADDRESS_DETAILS:
				return String.format(HOST_URL_1 +GET_ADDRESS_FROM_SERVER,parameters);

			case WS_POST_ORDER_DETAILS:
				return String.format(HOST_URL_1 +POST_ORDER_DETAILS,parameters);

			case WS_GET_CURRENT_ORDER:
				return String.format(HOST_URL_1 +GET_CURRENT_ORDER_DETAILS,parameters);

			case WS_GET_EXISTING_ORDER:
				return String.format(HOST_URL_1 +GET_EXISTING_ORDER_DETAILS,parameters);

			case WS_APPLY_COUPON:
				return String.format(HOST_URL_1 +APPLY_COUPON,parameters);

			case WS_GET_NEAREST_STORE:
				return String.format(HOST_URL_1 +GET_NEAREST_STORE,parameters);

			case WS_GET_CURRENT_ADDRESS:
				return String.format(HOST_URL_2 +GET_CURRENT_ADDRESS,parameters);

			case WS_POST_ITEM_REVIEW:
				return String.format(HOST_URL_1 +POST_ITEM_REVIEW,parameters);

			case WS_SHOW_COUPON:
				return String.format(HOST_URL_1 +SHOW_COUPONS,parameters);

			case WS_SHOW_NOTIFICATIONS:
				return String.format(HOST_URL_1 +SHOW_NOTIFICATIONS,parameters);

			case WS_GET_CURRENT_LOYALTY_POINTS:
				return String.format(HOST_URL_1 +GET_LOYALTY_POINTS,parameters);

			case WS_POST_STOCK_CHECK:
				return String.format(HOST_URL_1 +STOCK_CHECK,parameters);

			default:
				return getRequestedURL(wsMethod);
		}
	}
}
