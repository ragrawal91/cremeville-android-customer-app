package com.nm.cremeville.webaccess;

import android.content.Context;

import com.nm.cremeville.utilities.AppConstants;

import java.io.IOException;


public class RestClient
{

	public String sendRequest(Context mContext,ServiceMethods method, String parameters,String queryparams) throws IOException
	{
		int reqType = ServiceURLs.getRequestType(method);
		
		String contentType=ServiceURLs.getContentType(method);
		
		if(reqType == AppConstants.GET)
		{
			return new HttpHelper(mContext).sendGETRequest(ServiceURLs.getRequestedURL(method),queryparams,contentType);
		}
		else if(reqType == AppConstants.POST)
		{
			return new HttpHelper(mContext).sendPOSTRequest(ServiceURLs.getRequestedURL(method), queryparams, parameters, contentType);
		}
		else if(reqType == AppConstants.PUT)
		{
			return new HttpHelper(mContext).sendPUTRequest(ServiceURLs.getRequestedURL(method),queryparams,parameters,contentType);
		}
		else if(reqType == AppConstants.DELETE)
		{
			return new HttpHelper(mContext).sendDeleteRequest(ServiceURLs.getRequestedURL(method),queryparams,contentType);
		}

		return null;
	}
}
