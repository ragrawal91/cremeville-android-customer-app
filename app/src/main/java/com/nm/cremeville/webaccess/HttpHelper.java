package com.nm.cremeville.webaccess;

import android.content.Context;
import android.text.format.DateUtils;

import com.nm.cremeville.utilities.AppConstants;
import com.nm.cremeville.utilities.LogUtils;
import com.nm.cremeville.utilities.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class HttpHelper
{
    private long TIMEOUT_CONNECT_MILLIS = (60 * DateUtils.SECOND_IN_MILLIS);
    private long TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;
    InputStream responseStream;
    PreferenceUtils preferenceUtils;
    String cookieValue="null";



    public HttpHelper(Context mContext) {

        preferenceUtils=new PreferenceUtils(mContext);

    }

    public String sendPOSTRequest(String strPostURL,String queryparams,String strParamToPost,String contentType)
    {
        LogUtils.error(HttpHelper.class.getSimpleName(), "on HttpHelper sendPOSTRequest");

        DataOutputStream outputstream;

        int statuscode=0;

        String response = "NA";

        try {
        	 strPostURL = strPostURL.replaceAll(" ", "%20");

        	 if(queryparams!=null)
     		{
     		strPostURL+=queryparams;
                strPostURL = strPostURL.replaceAll(" ", "%20");
     		}
            LogUtils.error(HttpHelper.class.getSimpleName(), "strPostURL=" + strPostURL);
            URL Url=new URL(strPostURL);
            try {
                HttpURLConnection urlConnection= (HttpURLConnection) Url.openConnection();

                urlConnection.setRequestMethod("POST");

                urlConnection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);

                urlConnection.setDoInput(true);

                urlConnection.setDoOutput(true);

                urlConnection.setUseCaches(false);
                
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                outputstream=new DataOutputStream(urlConnection.getOutputStream());
                outputstream.writeBytes(strParamToPost);

                outputstream.flush();

                outputstream.close();

                urlConnection.connect();

                statuscode=urlConnection.getResponseCode();
                LogUtils.error(HttpHelper.class.getSimpleName(), "statuscode=" + statuscode);
                LogUtils.error(HttpHelper.class.getSimpleName(),"HTTP STATUS CODE is"+statuscode);

                if(statuscode== HttpURLConnection.HTTP_OK)
                {
                    responseStream = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response=stringBuilder.toString();
                }

                else if(statuscode== HttpURLConnection.HTTP_UNAUTHORIZED)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Login Failed");
                        jsonobject.put("message","login failed");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else if(statuscode== HttpURLConnection.HTTP_INTERNAL_ERROR)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Invalid");
                        jsonobject.put("message","Invalid");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else if(statuscode== 422)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Already registered");
                        jsonobject.put("message","Already registered");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }

    public String sendPUTRequest(String strPUTURL,String queryparams,String strParamToPost,String contentType) {
        LogUtils.error(HttpHelper.class.getSimpleName(), "on HttpHelper sendPUTRequest");

        DataOutputStream outputstream;

        int statuscode=0;

        String response = "NA";

        try {
            strPUTURL = strPUTURL.replaceAll(" ", "%20");

            if(queryparams!=null)
            {
                strPUTURL+=queryparams;
                strPUTURL = strPUTURL.replaceAll(" ", "%20");
            }
            LogUtils.error(HttpHelper.class.getSimpleName(), "strPUTURL=" + strPUTURL);
            LogUtils.error(HttpHelper.class.getSimpleName(), "strParamToPost=" + strParamToPost);
            URL Url=new URL(strPUTURL);
            try {
                HttpURLConnection urlConnection= (HttpURLConnection) Url.openConnection();

                urlConnection.setRequestMethod("PUT");

                urlConnection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);

                urlConnection.setDoInput(true);

                urlConnection.setDoOutput(true);

                urlConnection.setUseCaches(false);

                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                outputstream=new DataOutputStream(urlConnection.getOutputStream());
                outputstream.writeBytes(strParamToPost);
                outputstream.flush();
                outputstream.close();
                urlConnection.connect();

                statuscode=urlConnection.getResponseCode();

                LogUtils.error(HttpHelper.class.getSimpleName(),"HTTP STATUS CODE is"+statuscode);

                if(statuscode== HttpURLConnection.HTTP_OK)
                {
                    responseStream = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response=stringBuilder.toString();
                }

                else if(statuscode== HttpURLConnection.HTTP_UNAUTHORIZED)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Login Failed");
                        jsonobject.put("message","login failed");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else if(statuscode== 422)
                {

                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Already registered");
                        jsonobject.put("message","Already registered");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }

    public String sendGETRequest(String strGetURL,String queryparams,String contentType) {
        LogUtils.error(HttpHelper.class.getSimpleName(), "on HttpHelper sendGETRequest");

        DataOutputStream outputstream;
        int statuscode = 0;
        String response = "NA";
        try {
            LogUtils.error(HttpHelper.class.getName(), strGetURL);
            LogUtils.error(HttpHelper.class.getSimpleName(), "strGetURL=" + strGetURL);
            strGetURL = strGetURL.replace(" ", "%20");
    		if(queryparams!=null)
    		{
    		    strGetURL+=queryparams;
                strGetURL = strGetURL.replaceAll(" ", "%20");
    		}

            URL Url = new URL(strGetURL);
            LogUtils.error(HttpHelper.class.getName(),"after url");
            LogUtils.error(HttpHelper.class.getSimpleName(), "strGetURL=" + strGetURL);
            try {
                HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);
                connection.setDoOutput(false);
                
                connection.setRequestProperty("Accept", "application/json");

               // connection.setRequestProperty("Content-Type", contentType);
                
                connection.connect();

                //Get Response

                 statuscode=connection.getResponseCode();

                LogUtils.error(HttpHelper.class.getSimpleName(), "HTTP STATUS CODE is"+statuscode);

                if(statuscode==HttpURLConnection.HTTP_OK)
                {
                    responseStream = connection.getInputStream();

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response = stringBuilder.toString();
                }
                else if(statuscode==HttpURLConnection.HTTP_UNAUTHORIZED )
                {
                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Invalid Credentials");
                        jsonobject.put("message","Invalid Credentials");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }

                else if(statuscode==HttpURLConnection.HTTP_INTERNAL_ERROR )
                {
                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","failure");
                        jsonobject.put("message","New Registration");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }

    public String sendDeleteRequest(String strDeleteURL,String queryparams,String contentType) {
        LogUtils.error(HttpHelper.class.getSimpleName(), "on HttpHelper sendDELETERequest");

        DataOutputStream outputstream;
        int statuscode = 0;
        String response = "NA";
        try {
            LogUtils.error(HttpHelper.class.getName(),strDeleteURL);
            strDeleteURL = strDeleteURL.replace(" ", "%20");
            if(queryparams!=null)
            {
                strDeleteURL+=queryparams;
                strDeleteURL = strDeleteURL.replaceAll(" ", "%20");
            }
            URL Url = new URL(strDeleteURL);
            LogUtils.info(HttpHelper.class.getName(),"after url");
            try {
                HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
                connection.setRequestMethod("DELETE");
                connection.setConnectTimeout((int) TIMEOUT_CONNECT_MILLIS);
                //connection.setDoOutput(false);

                connection.setRequestProperty("Accept", "application/json");

                // connection.setRequestProperty("Content-Type", contentType);

                connection.connect();

                //Get Response

                statuscode=connection.getResponseCode();

                LogUtils.error(HttpHelper.class.getSimpleName(),"HTTP STATUS CODE is"+statuscode);

                if(statuscode==HttpURLConnection.HTTP_OK)
                {
                    responseStream = connection.getInputStream();

                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();

                    response = stringBuilder.toString();
                }
                else if(statuscode==HttpURLConnection.HTTP_UNAUTHORIZED )
                {
                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","Invalid Credentials");
                        jsonobject.put("message","Invalid Credentials");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }

                else if(statuscode==HttpURLConnection.HTTP_INTERNAL_ERROR )
                {
                    JSONObject jsonobject=new JSONObject();

                    try {
                        jsonobject.put("status","failure");
                        jsonobject.put("message","New Registration");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response=jsonobject.toString();
                }
                else
                {
                    response = AppConstants.NO_RESPONSE;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return response;
    }

}
