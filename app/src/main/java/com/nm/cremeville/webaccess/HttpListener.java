package com.nm.cremeville.webaccess;

public interface HttpListener 
{
	void onResponseReceived(Response response);
}
